# FINDING YOUR BLISS IN TOKYO - MOMENTUM
#### By [Jason L Gatewood](http://www.jlgatewood.com)([@starrwulfe](http://twitter.com/starrwulfe))  
*Originally commissioned by, written for and posted on [momentum.travel](http://momentum.travel/places/finding-bliss-tokyo/) on 2 Dec 2016* 


Tokyo is often called the world’s most densely populated megacity and with good reason. Roughly 30 million people—about 25 percent of Japan’s total population—live in the 60-kilometer radius around Greater Tokyo.

With so many people living in such close quarters, you’d think there would be little opportunity to find peace and quiet in the concrete jungle. Not so, according to model, actress and acclaimed yoga instructor Waka Nozawa.

‘There are surprisingly plenty of good places around Tokyo to feel the calm of nature and be able to meditate as well as practice yoga,’ Nozawa says. Here are her top spots:

### OMOTESANDO AND MEIJI SHRINE

![](http://momentum.travel/wp-content/uploads/2016/12/OasesTokyo_Body1.jpg)
Omotesando is famous for its high-fashion boutiques and ritzy stores; you’ll find Tokyo’s branches of Armani, Gucci, Louis Vuitton and more along a four-block stretch of the aforementioned boulevard.

At the end of the street is the gateway to one of Tokyo’s most historic Shinto sites, Mejii Jingu. Dedicated in 1920 to Emperor Meiji and Empress Shoken, the shrine grounds are situated in a wooded section of Yoyogi Park just opposite Harajuku Station.

Shinto practitioners believe divinity can be found throughout nature in plants, animals and humans, and are committed to the preservation of natural settings.
![](http://momentum.travel/wp-content/uploads/2016/12/OasesTokyo_Body4.jpg)

‘The transition from the crowds along Omotesando to the natural silence of Meiji Jingu is invigorating,’ Nozawa says. ‘Just crossing the gate into the shrine has a natural calming effect on me.’

**Getting there:** Nozawa recommends starting just outside Omotesando Station on the Ginza, Chiyoda and Hanzomon subway lines. Walk down the avenue toward Harajuku and Meiji-jingumae stations, and Yoyogi Park. Once inside the shrine grounds, there is a circuit that can be walked in about 30 minutes.

### YOYOGI PARK

You might as well wander over to the park next to Meiji Jingu, especially if you want to practice some outdoor yoga.

‘Remember, you’re not allowed to do exercises in the shrine or temple grounds!’ warns Nozawa.
![](http://momentum.travel/wp-content/uploads/2016/12/OasesTokyo_Body3.jpg)
Yoyogi Park is more appropriate because it has many clean and flat places to put a mat down, and sometimes you can find freelance yoga instructors wandering around the park who will help you learn a new pose for about ¥2,000 (around US$20).’

In addition to yoga, you’ll find groups practicing tai chi, kickboxing and even tumbling on any given day. And it’s not uncommon to see businessmen taking naps on the park benches during their lunch hour; Yoyogi Park is truly an oasis in the middle of Tokyo.

**Getting there:** Yoyogi Park can be accessed easily from Harajuku Station on the JR Yamanote Line, and Meiji-jingumae Station on the Tokyo Metro’s Chiyoda and Fukutoshin lines.

### OZAWA-YAMA RYUUN-JI

If you prefer a more traditional approach to relaxing, then try a meditation session at Ryuun-ji.

‘There are free zazen sessions every Sunday and also yoga classes that are held on the temple grounds,’ Nozawa says. Zazen is the art of meditation practiced by Zen Buddhists.

Its core tenant is to remain focused on the present without regard to the past or future, and it involves sitting on a cushion, counting breaths and occasionally being hit with a stick by the master.

[https://www.instagram.com/p/BL926faDwhO/?taken-by=wakanozawa](https://www.instagram.com/p/BL926faDwhO/?taken-by=wakanozawa)

Don’t worry—it doesn’t hurt and really does help you refocus your concentration. Instructions are available [online](http://ryuun-ji.or.jp/index.html) and in person.

**Getting there:** Ryuun-ji is located approximately 15 minutes on foot from both Gakugeidaigaku Station on the Tokyu Toyoko Line and Komazawa-daigaku Station on the Tokyu Den-en-toshi Line.

### SUN & MOON YOGA

Just because you’re in a different country doesn’t mean you should break your routine, right?

If doing yoga in a studio setting is your thing, then Nozawa recommends one of her local favorites, [Sun & Moon Yoga](http://sunandmoon.jp/) in Gotanda.

‘The setting is nice and multicultural, and there are English-speaking instructors on staff to help out,’ she says. ‘It also has training equipment such as cushioned pads and judo blocks to aid in getting into some of the harder poses.’

Sessions are around ¥3,000 (US$30), and many are conducted in both Japanese and English.

**Getting there:** Take the JR Yamanote Line, Toei Subway Asakusa Line or Tokyu Ikegami Line to Gotanda Station, and the studio is a five-minute walk from the east side of the station complex.

*Where in the great city of Tokyo would you go to find your inner peace?*
- - - -



*Images:* *Jason L Gatewood, Alamy*