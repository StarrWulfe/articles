# Articles
These are some of my written works that are currently in the editing phase or have been published (with links to the same article online if possible.) 

Most of these are news articles and web magazine features but some are also broadcast scripts and are rife with the formatting, typos and brevity that comes with the medium; in short, I ad-lib profusely whenever I'm on the air so usually my scripts are merely "talking points" I need to be aware of to stay on topic. Dont read these as regular print articles!

Be sure to check the tags and directory structure to navigate to the article you want to get to. 

I’ve tried my best to make sure the original Markdown/Org-Mode/plain-old-text is here so it can be easier to see and export elsewhere, but sometimes all I’ve been able to keep is the .docx or .pages export. Give me some time... Also Pandoc/Emacs/Vim have been wonderful tools for conversion so far I might add. Maybe it's the wanna-be programmer in me, but I far prefer working with so-called "cleartext" files then do the typesetting and formatting using Pandoc or LaTeX.

If you have a cooler way to show me how to do this in Vim/Emacs/Whatever, let me know!
