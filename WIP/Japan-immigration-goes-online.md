# Japan's immigration system to allow routine changes online

Starting sometime within the next 12 months, the Ministry of Justice which runs Japan's Immigration Service [has announced](https://asia.nikkei.com/Spotlight/Japan-immigration/Japan-to-allow-foreign-residents-to-change-or-renew-visa-online) a directed push to put online the routine services that most foreign residents under long-term visas must undergo. 

