# When to expect your COVID-19 vaccination in Japan

*due to the changing nature of this matter, we will be updating this article as frequently as new information becomes known to us. You are invited to help in the process by leaving a comment either here or across our social media networks.*

We are finally beginning to see the light at the end of the tunnel in regards to the Novel Coronavirus pandemic. Of particular note to those of us here in Japan, while we aren't seeing the levels of infection and ensuing deaths as our neighbors across the Pacific and elsewhere, we have had to contend with strained hospital networks, on-again-off-again States of Emergency from the national government, temporary and permanent closures of shops, restaruants and events as many here have opted to #stayhome instead of venturing out when told. The most noteable postponement being the 2020 Summer Olympics, which the nation as a whole has a lot riding on, has now been pushed to this summer (2021). Currently the outlook for the games going on even in that timeframe are murky, but the good news is that we now have vaccines available that can better the odds that we can at least enjoy a halfway return to normal life in the next six months or so.

### What We Know So Far...
While the rest of the world may have been focused on the UK and USA's ramp-up of vaccinations after the world-record paced successful attempt to actually create the medicines, we here in Japan are lagging behind due to how the country does its medical trialing. In particular, while many other nations will check out the trial data from another reputable agency like the CDC in the US and put a rubber-stamp on it, Japan usually chooses to add on its own small trial involving only ethinic Japanese as an addendum, further pushing out an approval date. This meant the first approved drugs that were ready to ship in January 2021 are only now becoming availible here in March. 


### What We're Still Waiting on...
The next point is logistical; there hasn't been a concrete plan developed yet that outlines a time schedule for the general public to get their doses. Officials from the national government have put a high-level framing around what would be a best-case scenario where the bulk of the country would have recieved there shots before the end of summer 2021, including any long-term resident foreigners. However the crucial details remain to be seen at this time.

  * Will there be mass injection sites as we've seen happen in our home countries?
  * How will the order of the distribution be handled, and by which agency? (will it be as simple as popping into our doctor's office and getting a jab when told, or will we need to wait for some card to come in the mail?)
  * Will we as foreigners need to register someplace for this to work, or are we already listed as per our Registration Cards?
  * Is there some way planned where this information can be readily accessible in English/Chinese/Korean/other languages?
  
  These points are being debated currently by officials at every level.
