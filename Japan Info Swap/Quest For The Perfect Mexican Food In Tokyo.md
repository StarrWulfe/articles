# Quest For The Perfect Mexican Food In Tokyo
Tokyo has been named ["World's Greatest Food City"](https://edition.cnn.com/travel/article/tokyo-world-greatest-food-city/index.html) numerous times and at last count is home to no less than 226 Michelin-starred restaurants. More than just ramen, udon, and tonkatsu places, you can find pretty much whatever a human can consume if you search hard enough in the Metropolis. But just because you can find it doesn't necessarily mean it's gonna be mind blowing. Take my addiction to burritos, tacos, salsa and guacamole for instance. I can't seem to get enough Mexican food in my life; you can thank my Southern California upbringing for that. It's an ethnic cuisine that seems simple enough to make, but difficult to get just right for many, but lately, there are a few shops scattered around Kanto that have made themselves worthy of my (not very authoritative) praise. These aren’t going to be chains like [Taco Bell](tacobell.co.jp), [Frijoles](https://frijoles-roppongi.business.site/), and [Guzman Y Gomez](gyg.jp)(although I am grateful for them too!); These are places that are one-offs where you’ll feel like Instagramming and Facebooking that enchilada when it comes to the table. 

### Cactus Burrito | Kawasaki, Musashi-Shinjo
In 2015, I wound up living in this blue-collar neighborhood just between Musashi-Kosugi and Mizonoguchi on the JR Nambu Line. There's nothing particularly spectacular about the area other than cheap housing close to major rail lines. But situated a 10 minute walk from the south exit of the train station is a hidden gem, [Cactus Burrito](https://goo.gl/7Gngs8). Their slogan even states itself as being "an oasis in the desert." One look at the menu  awash in  all manner of burritos, enchirios, carne asada fries and a fridge chocked full of craft beer, and you can easily see why I make the trek to this hole-in-the-wall joint long after I moved out of the area. 
**Cactus Burrito (A Cactus In The Desert)**
[http://cactus-burrito.com](http://cactus-burrito.com)
Map:  [1045 Chitose, Takatsu Ward, Kawasaki, Kanagawa 213-0022](https://goo.gl/maps/vBEUsMwcwn12)
Tel: 044-777-0555

### Baja
This is a straight up dive-bar “cantina” style outpost on the edge of the upscale Naka-Meguro neighborhood just south of the Meguro River. I became acquainted with the place about 8 years ago during the cherry-blossom viewing that happens around here, and became a frequent patron due to the affordable prices, tasty authentic tacos and heavy pours on cocktails. The place can only seat maybe five people, but feel free to take your tacos on the run; there’s a few outdoor benches and that river stroll is very picturesque if you’ve never been over this way before. 
**バハ BAJA 中目黒店**
Map: [1-16-12 Kami Meguro, Meguro Ward,  Tōkyō]([https://goo.gl/maps/vPmRYbPVeRK2)
Tel: 03-3715-2929

### Avocado Mexican Dining
Located in the trendy Shimo-Kitazawa district, Avocado bills itself as showcasing the best of modern Mexican cuisine. Most dishes here are lighter variations of your favorites with copious amounts of avocado, citrus juices and cilantro tossed in for a very flavorful experience.  One taste of their nachos with big chunks of avocado and fresh guac and you’ll be hooked! 
**メキシカンダイニング アボカド 下北沢店**
Web: [https://www.mexican-avocado.com/](https://www.mexican-avocado.com/)
Map:  [2-13-4  Kitazawa, Setagaya-ku, Tōkyō](https://goo.gl/maps/So6WPFcq8h22)
Tel: 03-3414-0916
*(There are also other locations in Shinjuku 3-chome and Kyoto as well)*


—
— By [Jason L. Gatewood](http://jlgatewood.com)
*Images: [WORKNAME}() by [PERSON](), [CC~~]()