# What to do in Tokyo during the New Year Holidays

People say Tokyo is the most restless city in the world, but maybe they’ve never been here around the traditional New Year holiday period when most residents aren’t punching the clock. Instead, they are spending time with their families, often well outside the Metropolis in their hometowns. Pretty much any and everything is shuttered for the first three days of the year except convenience stores and certain retail shops. If you are a tourist or a new resident to the area, you will find the streets devoid of traffic, trains, and buses running a slow schedule and eerily uncrowded. You might even become bored! But fear not, for there are still things to do, starting on New Year’s Eve.

**3-2-1, Happy New Year!!**
**Shibuya Crossing Countdown**
![](shibuya-crossing-tokyo-150x150.gif)If you feel that you can’t do without the parties and Auld Lang Syne, Shibuya crossing is probably the one place in Tokyo that will have throngs of people counting down to the New Year. Like other western celebrations such as Halloween and Christmas, the pubs, clubs, and nightclubs of the area have seen the western style New Year as another money-spinning occasion and is usually the epicenter of Japan’s spin on countdown parties. At midnight many people gravitate towards Shibuya Crossing to count down to the New Year not unlike what you may see in Times Square or Piccadilly Circus.
**Shibuya Station**
Shibuya Station, Hachiko exit, Shibuya-ku, Tokyo [Map](http://goo.gl/maps/ydvqG?shibuyastation)
**Joya No Kane: Ringing in the New Year**
![](Zozoji-temple-bell-150x150.jpg)For a more somber approach, you can choose to head over to a local Buddhist temple and watch the “joya no kane” temple bell-ringing ceremony. On the last day of the year, the bell in the temple is rung 108 times to symbolize the ridding of the 108 earthly temptations to make our minds and bodies fresh for the new year. Most local temples have some form of this ceremony starting around 8 pm on New Year’s Eve; one of the best places to check it out in central Tokyo is Zojoji Temple.

**Zojoji Temple**
[4-7-35 Shibakoen, Minato-Ku, Tokyo](https://maps.apple.com/?address=7-35,%20Shibakoen%204-Ch%C5%8Dme,%20Minato-Ku,%20Tokyo,%20Japan%20105-0011&auid=13022912957171287026&ll=35.657593,139.750002&lsp=9902&q=Zojoji%20Temple&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEHMKBAgKEAASJilw6gO6kdNBQDHySLqGxHdhQDnuvykWuNRBQEE64LwYH3hhQFAE)
03-3432-1431
[https://www.zojoji.or.jp](https://www.zojoji.or.jp/)
**Chichijima Island: Beach New Year Countdown**
[![](Ogasawara_Islands-150x150.jpg)](https://maps.google.co.jp/maps?q=Chichijima+Island&hl=ja&gbv=2&safe=active&um=1&ie=UTF-8&sa=X&ei=EA-BVJH_N8PUmAXdv4GADg&ved=0CAkQ_AU&output=classic&dg=brw)
As the land of the rising sun, what better place to celebrate the turn of the New Year than its earliest beach countdown ceremony on Chichijima island beach in the Ogasawara archipelago. There are Taiko drum displays, arm wrestling competitions, dance exhibitions, and the ceremony concluded with the first swim of the year and the release of baby turtles into the wild at sunrise. With a one-way boat trip taking up 25 and a half hours from Tokyo’s Takeshiba Passenger Ship Terminal, it’s certainly not the most accessible New Year party, but it is indeed one you will remember.

**Chichijima Island Beach**
[Ogamiyama-koen Park Festival Plaza, Chichijima, Ogasawara-mura](https://maps.google.co.jp/maps?q=Chichijima+Island&hl=ja&gbv=2&safe=active&um=1&ie=UTF-8&sa=X&ei=EA-BVJH_N8PUmAXdv4GADg&ved=0CAkQ_AU&output=classic&dg=brw)
04998-2-2587
[www.ogasawaramura.com](http://www.ogasawaramura.com/en/)

**2020 Day! **
Whether you partied all night at a countdown party or watched the sun come up somewhere lovely on the first sunrise of the year, or fell asleep watching one of the numerous Japanese TV specials that air on NYE, you’ve officially made it to 2020… Now what?! The slowest day of the year in Japan is January 1st, and while you could [stay home](https://morethanrelo.com/en/japanese-new-years-traditions/) and [do as the natives do](https://morethanrelo.com/en/osechi-japanese-new-years-food/), but you don’t need to be a homebody for the whole New Year’s break…
**Lucky Bag Shopping!**
![](lucky-bag-150x150.png)Forgetting the fact that Japan somehow now has a Black Friday manufactured shopping holiday in the middle of November, the traditional shopper’s marathon is the first three days of the year. It makes sense because most people are off work and school, workers are flush with their winter bonus pay, and kids will have received their New Year’s money envelope, called “Otoshidama (お年玉).” Stores put out their best “Fukubukuro (福袋),” literally “lucky bag” gifts for sale. These are bags filled with mystery merchandise that are often selling somewhere between 15~70% below retail– but the catch is no one knows what’s in the bags when they buy them! They are almost always a steal, and since the bags are limited in quantity, you will catch people lining up at the fashionable shops on New Year’s Eve just to wait for the doors to open the next day. If you are a shop-a-holic, then you can’t miss this tradition.
The best places? Of course, [Akihabara](https://goo.gl/maps/wBcUhDNyupU4z5Ln8), [Omotesando](https://goo.gl/maps/DebSzqizJy7FiK8o9), and [Ginza](https://morethanrelo.com/en/hanabi-fireworks-festivals-in-tokyo/), but pretty much every mall, plaza, and even online store will have a sale on.
**Emperor’s New Year Address**
![](imperial-palace-tokyo-150x150.gif)Usually, on January 2, the Emperor makes a public appearance at the Tokyo Imperial Palace. The palace is the primary residence of the Emperor of Japan, set in a large park-like area close to Tokyo Station.  There are only two occasions a year on which the inner grounds of the palace are open to the public, and this is one of them, so it is a perfect time to satisfy any curiosity you may have after seeing it from afar. Being that this year will be the first address from Emperor Naruhito since taking the throne, it’s due to be a sight attended heavily. And family members are scheduled to address a gleeful crowd at 10:10, 11:00, 11:50, 13:30, and 14:20.
**Tokyo Imperial Palace**
[1-1 Chiyada, Chiyoda, Tokyo](https://maps.google.co.jp/maps?hl=ja&gbv=2&safe=active&um=1&ie=UTF-8&fb=1&gl=jp&cid=15067460005103824311&q=Tokyo+Imperial+Palace&sa=X&ei=2A2BVIa5M8b4mAXUj4KgDg&ved=0CBUQtgMwAA&output=classic&dg=brw)
[Official guide for Emperor’s New Year Greeting to the General Public at the Palace](https://www.kunaicho.go.jp/e-event/sanga01.html)


``
Was published at https://morethanrelo.com/en/what-to-do-in-tokyo-over-the-new-years-holidays-2020/
``

