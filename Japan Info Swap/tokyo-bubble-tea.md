# Why is Bubble Tea suddenly popular in Japan?

The first time I tasted tapioca pearls in milk tea was when I was
a teenager in the mid 1990's while checking out a new trendy Asian fusion spot that was all the rage with us young folk in my hometown in the States. Fast-forward a couple of years to my first foray into Japan and I semi-noted that while milk tea can be bought at any vending machine and quickmart, the chewy "black pearls" and thick straws could not be found upon these shores. Side quests into Korea showed they had the stuff around the same time the States boomed, and of course so did Hong Kong and Singapore; two places frequented by Taiwanese, the originators of the Boba Tea concoction. So how did Japan get left out of this trend 20 years or so ago, and more astonishingly, just why is it suddenly popular here for seemingly no good reason?

### A Trend Once Missed

Japan missed the boat on the first world bubble tea phenomenon perhaps due to the lost years after the economic bubble of the 1980's collapsed and with it, many people's appetite for travel. Many foreign food, fashion and pop-culture trends are bought to Japan by enterprising trendsetters who emulate what they come across elsewhere. Also remember, there was no Twitter and Instagram either-- something that is definitely contributing to the boom right now. 

### So why is it a thing in 2019? 

Basically the same reason as the last point above; social media has hit Japan in a major way, especially YouTube and Instagram. In 2015, Taiwanese chain Gong Cha pushed into the Tokyo market with shops in Harajuku and Shibuya followed by a few others from the region. Then last fall a few SNS influencers started showing themselves drinking the concoction all over the place and a trend was born. Suddenly every shop, stall and stand that caters to the under 25 set started selling bubble tea along with their usual fare. It was even reported that  factions of Yakuza gangs, the Japanese mob, have opened up boba shops because it's "a good legitimate business with low overhead and easy cashflow."

### I'm thirsty! Where's a good place to get one? 

You know it's a trend if you can [go to the convenience store and pick one
up](https://www.family.co.jp/goods/drink/1753301.html), and even some [supermarkets have kits](https://mitok.info/?p=85788) that let you make bubble tea at
home. But if you gotta get your tapioca tea crave sorted, I recommend going to the source - the original chains from Taiwan where the stuff was invented that have made their way to Nippon: 

* **Gong Cha**

Originally from Kaohsiung, Taiwan, this franchise is found all around the
world from the United States to Myanmar. 

At least 9 locations in Kanto, from Shibuya and Shinjuku, to as far as
Tachikawa and Urayasu. [Map and details here.](https://duckduckgo.com/?q=gong+cha&atb=v98-1&ia=web&iax=about&iaxm=places)

* **The Alley Liu Jiao Xiang**

Another chain from Taiwan, with branches from Canada to Singapore. There
are 6 locations around Greater Tokyo from Sangenjaya and Jiyugaoka to
Akihabara and Ueno. [Map and details here.](https://duckduckgo.com/?q=the+alley+bubble+tea&atb=v98-1&ia=web&iaxm=places)

* **CoCo都可**

Not to be confused with my favorite curry chain, CoCo都可 (pronounced *ko-ko-to-ka* here) burst onto these shores from Taiwan with their first shop in Shibuya in 2017; there are currently 7 shops from Takadanobaba to as far as Machida. [Map and details here.](https://www.google.co.jp/maps/search/CoCo%E9%83%BD%E5%8F%AF/@35.6588533,139.4596584,10.94z)

