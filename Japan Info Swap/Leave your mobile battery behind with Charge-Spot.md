# Leave your mobile battery behind with Charge-Spot
Back in the days of flip phones, Japan what noted for having some seriously nice phones that could do almost anything our smartphones can do now. We were sending email, downloading music, watching TV and paying for train rides on them while the rest of the world was still oohing and ahhing over text messages. And while I’m not too sad about having to remember how to switch between Japanese and English text input when typing on a dial pad, I do miss the three days or more battery life I used to get out of my old Casio G-Shock “gala-kei”.  Firmly locked into the world of smartphones and life in Tokyo means I am used to toting around a mobile battery, USB charger and searching for power outlets in the corners of shopping malls and stores. One good thing about Tokyo is you can find a branch of one of the major cellphone carriers everywhere, and they’ll let you charge your phone if you’re a customer. But what happens if you’re out for a night and they’re closed plus you forgot your gear? Well now there’s the option of mobile battery rental.

Enter [Charge-Spot](http://charge-spot.net), a Hong Kong based company that has just begun placing their automated kiosks around Shibuya and Shinjuku to aid in the fight against smartphone battery drain. Their machines work in concert with a smartphone app to dole out portable power packs that you carry with you and return in any other kiosk when finished. It’s a system quite similar to bicycle and car share systems, and the automated DVD rental machines seen in my native USA. Add this one to the list of “why has this not been done before” because in a town like Tokyo, a dead smartphone will kill your professional and social life quicker than anything else. 

### How does it work?
* The first step is to download their app and install on your phone or tablet; iOS and Android are supported. 
* Next, simply register with their system with your phone number. No need to even have a username or password here. (I wish more systems were like this since I know I won’t get blasted with spam later)
* You’ll need to pay a ¥1,500 deposit via the Visa/MasterCard of your choice or use Apple Pay if you have an iPhone. The deposit is refundable at anytime.
* Then when your getting the “low battery” warning next time, just check the app to see the location of the nearest kiosk along with the status of available batteries. 
* When you get to the kiosk, scan its QR code with the app and a battery pack should pop out along with the timer starting on the app for your rental period.
* When you’re ready to return it, just check the app again to see where a kiosk with available empty slots is located near where you are, go there and drop the battery pack in the return slot. Done!

### What are the “gotchas?”
Mostly none actually, but there are a few things to consider:
You need a phone with working SMS to get the text message that registers you on their system. If you have regular Japanese voice/SMS/data contract here, you’re good. But if you are a short-termer using your phone from home, check to see if you can receive SMS messages in Japan. If you’re using a prepaid data-only SIM then you are outta luck; there’s no other way to verify without a text message.
Also the deposit is only using certain credit cards or Apple Pay. If you have AmEx or JCB or nothing at all, you’re benched. 

### How much does it cost?
Well they are giving a generous amount of credits to try their service right now. I got about ¥2000 in coupons upon registration in the app. Also the fees are pretty good— ¥100 for one day, ¥200 for two. Don’t let it go past that, or there goes your deposit, and you have a brand new battery pack that you can’t recharge since only the kiosks have the capability to do that. Also remember, as I said above you can get your deposit back anytime, so this is great if you find yourself stuck without options one day, but normally have your own charger and battery other times.

### How about the actual battery pack, what are the specs?
The battery packs themselves are 4800mA which can charge the average phone from dead to full twice at least. The pack has Micro USB, USB-C and Apple’s Lightning connectors on them so pretty much everything is covered. 

### Where can I find it?
Only Shibuya, Harajuku and Shinjuku have kiosks for now, but the plan is to expand across to major places inside the Yamanote Loop then around to other parts of Tokyo and Japan soon. Access http://charge-spot.net for instructions, download links and more

- - - -
*---By [Jason L Gatewood](http://www.jlgatewood.com)*