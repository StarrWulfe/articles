# Fuji rock 20th anniversary lineup
Fuji Rock is Japan’s biggest music festival and definitely one of the biggest and best in the world. Every year since it’s inception in 1998 has seen tens of thousands of concertgoers check out some of the world’s hottest music acts over the span of three days it takes place in the summer. Despite it’s name, Fuji Rock was only held at the base of Mount Fuji in its premiere; however an errant typhoon spoiled the 2nd day of that year’s fest, and the next year saw a move to Tokyo’s Toyosu area. However every year since 2000 has seen the festival take place on the grounds of Naeba Ski Resort in Yuzawa, Niigata Prefecture.  Having one of the world’s largest music festivals take place in one of the world’s best ski resort towns during the off season has the advantages of an abundance of lodging and food options, great transport to and from the venue, and a nice natural amphitheater effect with the mountains and slopes. 

### Tickets
To attend the concert series, you can purchase tickets in a variety of ways. The earlier you buy them, the cheaper they are.  If you are outside Japan, you can [purchase tickets using iFlyer.tv](https://admin.iflyer.tv/apex/eticket/?id=298971&l=cheapo). However you’re already in Japan, you’ll need to go through a ticket broker in-country; that means you can either head over to your local Seven-Eleven, Lawson or Mini Stop and use the kiosks (likely only in Japanese, but the store clerks usually do a great job of walking you through the process if needed). You may also purchase online through [Gan-Ban Ticket]([http://ganban-ticket-e.ocnk.net/product-list/1) in English; you must have a Japanese bank account for the furikomi bank transfer as they do not accept credit cards. If you’re in the Tokyo area and don’t mind traveling to Shibuya, Shinjuku or Kichijoji, the [HMV Record Shops in those places](http://www.fujirockfestival.com/ticket/playguide.html#playguide15) are selling them in-store as well. 

### Accommodations 
If you’re choosing to attend multiple days, you can take the Woodstock approach and camp out in designated areas, or choose to stay in anything from spartan cabins to full-service hotels that are usually in operation during ski season. 
- Camping: There are 3 campsites, each have a different purchase price and rule. Also you must bring your own camping equipment as there’s no way to buy tents or sleeping bags there (Yes, I have seen people try to do so when I attended years ago.)
- Hotels: There are lots of places to stay since the area is a ski resort town. Check at the [Tour Center website (Japanese)](http://www3.collaborationtours.com/fujirock/area/) to inquire and book. 

### Transportation
Of course being that it’s in Japan, means there is a train and/or a bus that will get you to the venue.  
- **Train** 
 [JR Echigo-Yuzawa](https://maps.google.com/maps?ll=36.936103,138.809896&q=36.936103,138.809896&hl=en&t=m&z=15) station on the Joetsu Shinkansen line is the easiest way to arrive, and a ¥500 shuttle bus will whisk you to and from Naeba and the concerts.     
- **Car**
You may also choose to drive there by way of the Kan-Etsu Expressway [E17] along with National Route 17, but make sure you’ve made parking arrangements first; there are numerous parking areas and each have their own rules and times of operation. 
- **Bus Tour Package**
There are direct buses leaving from throughout Japan that are all inclusive of fees for the concert, accommodations and the like. This is what I have done the past two times I went, and was very happy with the painless service, however it will [require you to use the Japanese tour site](http://www3.collaborationtours.com/fujirock/tour/busplan.html); use the translation program or Japanese-speaking friend of your choice.
### The Lineup
Here’s the list of performances scheduled as of this writing. Remember, acts can be subtracted or added without notice so be sure to check directly as showtime gets near.
```
* Bob Dylan
* Kendrick Lamar
* N.E.R.D
* Skrillex
* Vampire Weekend
* Sakanaction
* Brahman
* Chvrches
* Dirty Projectors
* Greensky Bluegrass
* Jack Johnson
* Maximum the Hormone
* MGMT
* Odesza
* Years & Years
* Anderson Paak
* Jon Hopkins
* James Bay
```
