Some apps to help you enjoy Tokyo 
---------
We're back again to give you a few more good apps to try out on your smartphone or tablet to make life a bit easier here in the Metropolis for you. 

####Trash Day Alarm
As we've said [many times](japaninfoswap.com/disposal-of-large-garbage-and-electrical-items-in-tokyo/) [before](http://japaninfoswap.com/separating-and-recycling-trash-nagoya/), garbage day in Japan can be a bit... Tedious. Worse yet, different items are collected at different times weekly, bi-weekly, and sometimes monthly and quarterly. You could set up reminders or calendar notifications, but what happens if your collection dates are interrupted by Golden Week or New Year's holidays? That's a lot of manual set up that needs to be done every year. Just download this free app and not only are those things taken care of, but you can also be reminded on your Apple Watch as well.
- Get the app [(iOS App Store only)](https://t.umblr.com/redirect?z=https%3A%2F%2Fitunes.apple.com%2Fapp%2Fid639394564%3Fmt%3D8&t=ZTY5NmFiNWFiYzMzNGI1NWMwNGE1ODU0OTM4YmNhZDhjZTE2OTVhYyxUbU4zdWpESg%3D%3D&p=&m=0)

####NHK World TV & Radio
Jokes about [dodging the NHK fee collection guys](https://en.rocketnews24.com/2017/02/16/w-t-f-japan-top-5-ways-to-get-rid-of-the-annoying-door-to-door-nhk-guy-【weird-top-five】/) aside, the network still remains one of the best media outlets to get info about Japan in English. Their app delivers streamed video content as well as news articles on cultural, world and domestic affairs in English. The latest update has also added real time emergency bulletin notificaitons about earthquakes and tsunami activity.
- Get the app [via their website](https://www3.nhk.or.jp/nhkworld/en/app/)

#### Japan Taxi
Most of the time in Tokyo, getting a taxi is usually a no-brainer, just go out to a busy street and look for a cab with the ["空車" sign](https://gqjapan.jp/uploads/media/2014/08/vacant-taxies.jpg

) and flag it down. But if you happen to find yourself in a "dead zone" taxi wise, just pop this app open and access 54,000 cabs hooked into the Japan Taxi Network, on-demand. Just as in Uber, you can use your device's GPS to pinpoint where you are and where you want to go and hail a car. Plus you get the option to pre-calculate the fare and pay through the app itself if you have Apple Pay or Android Pay all set up.
- Get the app: [Android](https://play.google.com/store/apps/details?id=jp.co.nikko_data.japantaxi) [iOS](https://itunes.apple.com/jp/app/id481647073?mt=8)

#### Check-A-Toilet
Everybody poops, right? Our lovely country seemingly has a large amount of resources tied up in making sure your time in the john is the best you can experience. Of course it should come as no surprise then to know there's a website that crowdsources information about the best places to do your business whilst out and about. This app is all about getting the lowdown about the nearest bathroom and ensures you don't encounter squalid condtions or the dreaded squatty potty, and can drop a deuce in peace. Or you can choose to explore restrooms unknown and add them to the database to help all your fellow Tokyoites find a proper loo. 
- Get the app [via their website](http://www.en.checkatoilet.com)