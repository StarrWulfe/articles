## Under the tracks in Akihabara

Of course you know what Tokyo's Akihabara district is known for: electronics, video games, anime and manga shops, maid cafes, and anything else that would make a nerd, geek, weeaboo and technojapanophile drool. Since I fit into lots of these categories, I tend to visit the area once a month or so just to check the scene out. But there's more to Akiba than all this. Theres something brewing out of the culture here that harkens back to the days of prewar Tokyo, and Akihabara wants to show everyone that side too.

###### the underside
Back in 2010, JR East started a project to add additional tracks between Ueno and Tokyo stations to make the Tokaido Line transfers easier. At this same time, JR also decided to look into removing their equipment stored there and leasing more spaces underneath their elevated line near Akihabara station. And from that newly reclaimed area, two new venues were created: "Chabara Aki-Oka Marche" was born.

###### Chabara Aka...whatzit?
Yeah that is a long name to be sure. "Chabara" is a coined portmanteau that means "fruit and veggie market of Akihabara." "Aki-oka" means "between Akihabara and Okachimachi stations", and "marche" is French for "market." Just shorten it to "Chabara" when asking for directions if you get lost, since no one really knows it by the long name. What's important is that it is a market where local food, candies, snacks, drinks and more that have been collected from all over Japan can be found. Think of it as the ultimate souvenir shop for the distinguished Japan connoisseur in your life. Personally, I've lived in three different regions of the country: Kansai, Chubu and now Kanto. Each area I've lived in has something famous I can't seem to find anywhere else, but places like Chabara specialize in making sure it's there. Like the special Steakhouse Curry made with Hida Beef (which is a step above Kobe Beef IMHO). Previous to Chabara I could only find it either in that region of Gifu Prefecture or order it online. The same for the slightly spicy red miso paste I like from Aichi Prefecture. Also let's not forget that other food-related pasttime many may have-- looking for good libations to evoke their trip to someplace new. Chabara has you covered in the adult beverage category as well with oodles of wines, spirits and local beers to peruse.

###### Can we grab a bite while we're here?
There are 2 shops where you can catch a breather. [Yanaka Coffee](http://www.yanaka-coffeeten.com) is a specialty coffeeshop that gets at least 30 varieties of beans direct from the growers. Grab a cup or take home a bag. [Komakishokudo](http://konnichiha.net/komakishokudo/) specializes in preparing food that is served in Buddhist temples here in Japan. That means absolutely no meat whatsoever is found in their cuisine. Whenever I have a vegan I'm helping navigate the city, this is one of the places I recommend. 

#####Getting there
Head over to JR Akihabara station. Then it's about one minute north out of the Electric Town exit.

**Website:** http://www.jrtk.jp.e.wz.hp.transer.com/chabara/ (machine translated so mind the grammatical errors!)
**Map:** https://goo.gl/maps/ZxVvEoCdZEL2
**Hours:** Everyday 11am-8pm
---
-- By [Jason L. Gatewood](http://jlgatewood.com)
*Images: Chabara Entrance, Chabara Interior. Jason L Gatewood, Own Work.*



