# Let’s Tour: Kawagoe Little Edo

Located about 60 minutes by train northwest of central Tokyo is the city of  Kawagoe in Saitama Prefecture. Because of its location in the Greater Tokyo area, it’s mainly considered just another “bedtown”; Japanese borrowed English slang for bedroom community.  But its current day suburban moniker can be easily peeled away to show Kawagoe’s long rich history that lies in its origins. You only have to wander over to the historical districts just north of Hon-Kawagoe station and find not only one of the last collection of historically preserved buildings in the Metropolis, but also many museums and shops dedicated to the task of showing off old Japanese traditions and life. 

### Get the low-down at the museum
In order to learn more about the area, head over to the [Kawagoe City Museum](http://museum.city.kawagoe.saitama.jp/) and gain an overview of how a samurai saddled with the burdensome sparsely inhabited lands was able to bootstrap the area into prosperity by turning it into a silk weaving boomtown.  The journey doesn’t begin there though as there is a large collection stemming back from the Yayoi and Kofun periods in early Japanese history. You’ll also learn how people were able to rebuild the town after an enormous blaze burnt down almost every building standing in the then small village.  There are English speaking volunteers available to help explain the finer points if needed. 
[Kawagoe City Museum, 2-30-1 Kuruwamachi, Kawagoe 350-0053, Saitama Prefecture](https://goo.gl/maps/7os3pFjijHB2)

### Seeking a higher power
Back in the feudal days, nothing could be done without the will of the gods above and rulers of the lands; Kawagoe was home to several strong temples, shrines, and lords. The oldest shrine, [Miyoshino Jinja](https://goo.gl/maps/FfERdF5zRfm) was established in the year 807! It is located on the grounds of what used to be Kawagoe Castle which doesn’t exist anymore but [Honmaru Palace](https://goo.gl/maps/15Kxf7CZqNM2) is still there and can be accessed. [Kita-in](https://en.m.wikipedia.org/wiki/Kita-in) also can be found nearby. This was the main Buddhist temple in the Kawagoe domain, and the building itself was once part of Edo Castle. Don’t miss the 540 statues of Rakan inside, all representing the original disciples of Buddha. 

### A stroll through the neighborhood 
Head over towards the “[Warehousing Street” historical area](http://www.city.kawagoe.saitama.jp/welcome/kankospot/kurazukurizone/kurazukuri.html) and you’ll find a whole district full of “kurazukuri” warehouses and old storefronts that are part of a designated historical site. These are no mere display units though; every one of them is still in use as a storehouse, small workshops or store, and you are very encouraged to visit and drop some hard yen on whatever may tickle your fancy.   If you hang out in the area long enough, you will hear the bell toll from [Toki no Kane](https://www.pref.saitama.lg.jp/chokotabi-saitama/eng/spot/sp64.html), the old bell tower that is also the symbol of the city of Kawagoe. It rings out 4 times a day (6am, 12pm, 3pm, and 6pm)

 
### For more information and access:
Visit the Kawagoe City tourism homepage where a detailed guide to all the area’s attractions can be found.
http://www.koedo.or.jp/foreign/english/](http://www.koedo.or.jp/foreign/english/) 

Three train companies have stations in the area:
- Kawagoe Station - JR Kawagoe line and Tobu Tojo Line [TJ21]
- Hon-Kawagoe Station - Seibu Shinjuku Line [SS29]

- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
Images courtesy [Kawagoe City Tourist Office](http://www.koedo.or.jp/foreign/english/), [Saitama Prefectural Tourism Office](https://www.pref.saitama.lg.jp/chokotabi-saitama/eng/spot/sp60.html)