# Best Breakfast Spots in Tokyo
You might not know this, but [Japanese cuisine was added to the  UNESCO ‘intangible heritage’ list](https://www.bbc.com/news/av/world-asia-25223105/japanese-cuisine-added-to-unesco-intangible-heritage-list) in 2013, meaning its something unique in the world that cannot be found anywhere else (we won't talk about that “Japanese" teppanyaki and all-you-can-eat buffet place you liked before you got here.) I could eat my fill of sushi, takoyaki, udon, soba any and everyday of the week, but one thing that I constantly pine for is a hearty breakfast. Scrambled eggs and bacon with toast, hash browns and pancakes... or heaven help me, WAFFLES and SAUSAGE! I grew up in a family where my first meal of the day sometimes bigger than the other two. And after decades of Japan living under my belt, a top ten question is always “where can I get a decent down-home breakfast around here" from many a gaijin. Allow me to share a few of my favorites.

### World Breakfast All Day
This appropriately named establishment does exactly what it says on the label; serve breakfast foods from around the world all day. Thai, Mexico, Poland, Germany, France, the UK and America are all represented here with decent portions and prices. 
[3-1-23 Jingumae, Shibuya-Ku, Tokyo](https://maps.apple.com/?address=1-23,%20Jingumae%203-Ch%C5%8Dme,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0001&auid=12456738344549137978&ll=35.670293,139.713816&lsp=9902&q=World%20Breakfast%20All%20Day&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBQKBAgKEAASJilc9cr3ONVBQDHhKPtJqnZhQDnayvBTX9ZBQEENDr7fBHdhQFAE&t=r)
https://www.world-breakfast-allday.com/
Open Everyday 7:30~20:00
Access: Gaienmae Station [G]

### Sarabeth's 
“The Breakfast Queen of New York" landed here in Tokyo about 3 years ago at Lumine II above Shinjuku Station. I couldn’t actually get in the place to have a meal until 2018 because it’s always crowded, but thankfully since then they’ve opened 2 more places around Tokyo and also operate in Osaka and Nagoya too. My favorite NYC export on the breakfast scene and probably the only place where I can get Eggs Benedict with lox here in the Metropolis. 
*Multiple locations and hours; check* [Sarabeth’s Japan locations web page](http://sarabethsrestaurants.jp/location/en/) *for more info.*

### Jade5 Breakfast & Brunch 
Have you ever been out in some part of town doing something and afterwards intent on getting home but then stumbled upon a diamond of a find and wound up spending more time and money than you should have? That describes my experience with Jade 5 perfectly. After a night out in the area, I fell into this cafe and was blessed with a menu that had BREAKFAST BURRITOS and BLUEBERRY PANCAKES! ‘Nuff said. You know that ain’t appearing any other menus in Tokyo, so I suggest you find your way to Hiroo for some comfort breakfast stat! (Lunch is also good with a BLT with for-real bacon, and actual apple pie like granny makes.)
[5-17-6 Hiroo, Shibuya, Tōkyō](https://maps.apple.com/?address=17-6,%20Hiroo%205-Ch%C5%8Dme,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0012&ll=35.649476,139.720489&q=17-6,%20Hiroo%205-Ch%C5%8Dme&_ext=EiYpuRgD3I7SQUAxqm5M9+B2YUA5N+4oOLXTQUBBTvkDhzt3YUBQBA%3D%3D&t=r)
Open Tuesday-Sunday, 7:30~18:00.
Access: Hiroo Station [H]
[Jade 5 on Facebook](https://www.facebook.com/BreakfastBrunch-Jade5-211662002351489/)

### Royal Host
This entry on the list is specifically aimed at those people like myself who were extraordinarily disappointed upon finding out that Denny’s in Japan is a Bizzaro twin to it’s American sibling. Not a single Grand Slam breakfast in sight. Royal Host on the other hand, has all-day breakfast , has a drink bar with unlimited coffee (my savior), and can be found in most major cities in Japan.
*Multiple locations and hours; check* [Royal Host’s locations page (in Japanese)](https://www.royalhost.jp/) *for more info.*


---
— By [Jason L. Gatewood](http://jlgatewood.com)