###Learn about Tokyo's yesteryear at Taito Shitamachi Museum

Sooner or later as you become acclimated to this place, you'll start learning about the different eras of Japan. For example as of March 2018, we are living in the Heisei Era, as defined by the sitting emperor. It's how the year is officially calculated, and loosely defines a generation's identity. The era preceding this one was the Showa Era, and was bookended by WWII and the Bubble Economy; roughly 1926~1989. Many Japanese look fondly on this era and if they lived in Tokyo during the middle of this period, they saw a massive transformation of living standards, the physical landscape and indeed the very meaning of what it means to be Japanese. Even if you only have a passing interest in this period of history, you should wander over to the Taito Shitamachi Museum to see first-hand what life was like during the period.

The Taito Ward Historical Society runs the museum in a scenic corner of Ueno Park. The ward is itself one of the oldest in Tokyo and is called the heart of the "shitamachi", or old downtown area of Tokyo. Inside, you will find dioramas of old shops and streets, that you can explore on your own or with a free English speaking volunteer guide. Even though I've been here a long time and know a thing or two about Japanese history, I still opted for the guided tour because you'll always learn something new, and they readily answer any and all questions. 

The first floor gives a look at how daily life was in the tenements of Tokyo before WWII, the wooden 2 room row houses that shared a common roof. These homes often had 4 or more people people living in them, commonly used the "front" room as a storefront or workshop leaving only one room as a common living/dining/sleeping area. From the common well, to the outhouse, keeping good terms with one's neighbors was key to life in the shitamachi, and is a trait that is still seen in modern Japan.

The second floor houses examples of toys, games and books of the postwar era. There's also a wonderful mockup of a living room and kitchen along with a reconstruction of the entrance to a public bath; taken from the actual building when it was torn down a few years ago. This along with many photos and movies of the time help transport you to a time before Tokyo was known for its Bladerunner-esque skyline and faster-than-lightspeed pace. 

#####Getting there
**[Taito City Shitamachi Museum](http://www.taitocity.net/zaidan/english/shitamachi/)**
**Address:** Ueno Koen 2-1 (Ueno Park), Taito-ku, Tokyo
**Phone:** (03)3823-7451
JR and Tokyo Metro Ueno Station, Tokyo Metro Yushima stations are closest to the museum.
**Map:** https://goo.gl/maps/LUntpF5LN1G2
**Hours** Tue-Sun, 9:30a-4:30p, Closed Mondays.
**Admission** Adults ¥300, Child/Seniors ¥100