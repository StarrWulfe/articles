## Hibiya Tokyo Midtown: Upscale shopping at the Emperor’s front door
Tokyo’s Hibiya district is known mostly for its namesake park right next to the Imperial Palace grounds, and being home to many Japanese government offices as well as corporations. If you want to shop, then you’d just need to wander a bit east into the Ginza and Yurakucho areas...but that’s the past. A few years ago, the Mori group built Toranomon Hills which sits a 15 minute walk to the southwest. You may know the Mori Group from their sprawling Roppongi Hills development. Not to be outdone, Mitsui Real Estate has extended their “Tokyo Midtown” concept from Roppongi to the northwest corner of Hibiya park with Hibiya Tokyo Midtown. Oh yeah, so that means the “original” Tokyo Midtown is now known as Roppongi Tokyo Midtown to lessen the confusion of anyone who just came to Greater Tokyo; us locals already kinda are.

Confusing naming schemes aside, Hibiya Midtown (yeah, I’m shortening it!) is a mixed-use development centered around a [Toho Cinemas](https://hlo.tohotheater.jp/net/schedule/081/TNPI2000J01.do) and an avalanche of premier eateries. Seriously, there are a LOT of places to eat a meal here. But that’s kind of expected in this area; this is the epicenter of “Japan Inc.” and those bureaucrats and corporate suits need a place to broker deals, smooze clients, and treat their underlings! 

Let’s talk about that new theater though; there’s 13 screens, 3D, ATMOS sound, luxury and box seating options and yes, there’s an IMAX on deck too. Dinner and a movie anyone?

If you find yourself needing to catch a breather from all the excitement or looking for a way to impress the one you’re with, just head over to the 6th floor Sky Park. It’s an outdoor rooftop terrace that will have even the most diehard Luddite reaching for their camera phone to snap a selfie or three of that Tokyo skyline in the background. Protip: wait till sunset and wander over here to see a stunning view since it mostly faces west. 

[Tokyo Midtown Hibiya](https://www.hibiya.tokyo-midtown.com.e.adj.hp.transer.com/en/sp/)
1 Chome-1-2 Yūrakuchō, Chiyoda-ku, Tōkyō-to 100-0006
Access is dead simple since the basement floor connects directly to Hibiya station at both ends. Exits A5 and A11 will get you there.
03-5157-1251
https://goo.gl/maps/kSukZexeTmB2

