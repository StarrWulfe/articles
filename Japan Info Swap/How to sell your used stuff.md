# How to sell your used stuff
Japanese accommodations being notoriously small for my American hoarding habits, I occasionally draw the the ire of my wife and am made to “consider paring down” some of my possessions. This used to mean bugging friends and co-workers if they need whatever it was I needed getting rid of, or heading over to the local [Second Street](https://www.2ndstreet.jp/), [Hard Off](https://www.hardoff.co.jp) and other “recycle shop” secondhand storesonly to wind up getting back little more than train fare for my item, even though I burned gas and nerves getting it there. Online options are a little better; I’ve used [Craig’s List](http://www.craigslist.com/tokyo) and [Facebook “For Sale” groups](http://www.facebook.com/tokyogaragesale) for over a decade with decent results but there’s still the hassle of dealing with haggling with buyers wanting pictures from every imaginable angle, a detailed usage history, and hand delivery to the most remote part of Japan possible that leaves a bad taste in my mouth. The end result is that I only use these options whenever I’m shifting flats or my used item is an obvious high-value like a smartphone, PC or appliance. Everything else these days just gets the [“sodai-gomi”](https://morethanrelo.com/en/disposal-of-large-garbage-and-electrical-items-in-tokyo/) treatment and carted off to the trash heap. But that was before I discovered Mercari.

[Mercari](http://mercari.co.jp) is an online marketplace that allows regular folks to sell to other regular folks “flea market” or “swap meet” style, in the vein of a flashier [Ebay](www.ebay.com) if you've ever used that site before. Unlike that aforementioned site though, you can create an account, take pictures, write a quick description and be up and selling in about 5 minutes flat. Plus the only fee will be a 10% commission on the item’s selling price. Selling something for ¥10,000? Mercari gets ¥1,000 with no hidden listing charges, or extra fees whatsoever. Really the only catch is that the app and entire marketplace is in Japanese... Hey, don’t go heading for the hills just yet; you just need some rudimentary Japanese terms along with the power of [Google Translate](http://translate.google.com) to help you in a pinch. 

### Having a way with words
Let’s tackle the most intimidating part first: the product title and description. You just need to make sure to write well enough so that the algorithm program on the site classifies and places your ad into searches that match the item the customers are looking for. 

1. **Title**  This should be consicse and include brand name, model and top specs. Only the first 13 characters will count so be brief! Here's an example title straight from the site: ”【保証書付】iPad mini5 64GB wifiモデル　スペースグレー” which translates as “(Insurance Card Included) iPad mini5 64GB Wi-Fi model Space Gray”. It also works for clothing: “ユニクロ　メンズ　ダッフルコート　XS コート　アウター　小さいサイズ” which is “Uniqlo Men's duffle coat XS coat outerwear small-sized”. Pack as much info in there as possible and don’t be afraid to check other similar items in the app first to see how other sellers have worded it.
2. **Description** When selling something used in Japan, I find it easier to just go into detail on whatever imperfections due to regular use of the item, no matter how small it might be. Japanese customers don’t like to be caught off-guard even though it could be something as obvious as a small scratch on the housing of a 3 year old PC or a small stain on the back of a sofa due to regular use. Not disclosing these seemingly trivial issues will net you a negative review and possibly a returned item. In my short experience, simply stating the obvious has never hurt me. Some ways to do this are shown in the following examples: 

	持ち運びに便利な○○です
	This is an XX that is easy to carry

	〇〇のために使用していました
	I used this item for XX

	一回しか使ったことないので、新品に近いです。
	I only used it once, so it is almost as good as new

	2-3回程度使いまして、目立った傷や汚れはありませんが、中古なので神経質な方はご遠慮願います。
	I used the product 2-3 times. It doesn't have any noticeable damages or stains, but it is still second-hand so people who are nervous about such are kindly asked to refrain from buying.

	おそらく気にならない程度と思いますが、傷汚れありにしました。中古ですのでご理解ください。
	It is probably not anything significant, but I've set the condition of the product to "has damages and stains". Please be understanding as this is a second-hand product.

	中古のため、多少傷が付いていますが、ご了承ください。
	Please note that this product is old and has a few damages.

	Using phrases like these will help clear the air and set the expectations for your particular item.

### A picture is worth...well you know...
Of course you know you’re going to need to take some decent pics of your item to post it for sale. The app makes good use of whatever smartphone or tablet you’re using, so kudos if you have a brand-spanking-new iPhone or Galaxy that can take pictures in the pitch darkness of space and make them all studio perfect. For the rest of us though, you can also use your “good camera” to get the shots and save them to your phone's memory and post them that way too. Make sure to take pictures of those imperfections we talked about above, and if possible, get angles of the item while in use to show it still works and in relation to other objects; people trying to buy your old sofa or bookshelf will appreciate that!

### The price is right!
Setting the sale price is a dicey affair for sure. You need to be fair for the item being used yet competitive with similarly categorized items. Also Japanese buyers are finicky with cost—- pricing your item in the basement will make viewers think it’s too cheap and something must be wrong with it, thereby passing it over. You need to do some research on the app and set your price in the same ballpark as others, even if you are trying to hurry up and just get rid of it. Your transaction history is public record and once you become known for frequently changing the prices after your initial posting, it’ll be hard to ditch that reputation. Stick to your guns! Remember if your item hasn’t sold in about 5 days, just delete it, wait a day and repost it with a lower asking price. 

### SOLD!
Once you have a willing buyer, you'll get a notification in the app, an email to your registered account and so forth. The buyer will get automated notifications from the system saying you're in the process of getting their order together to send out but because Japan is Japan, you should also send a personalized message like “この度はご購入いただき、ありがとうございます。
お支払いのご確認が取り次第ご送付させていただきます。引き続き、どうぞよろしくお願いいたします。(Mr/Ms xx, thank you for your purchase. I will send the product as soon as the payment has been confirmed. Thank you!)” 
Also make sure you’ve posted your item with shipping included in the pricing (something you set in the listing process) or else be prepared for endless haggling! Sending the item is very easy these days since there are partnerships with FamilyMart and Lawson’s convenience stores for obtaining the packaging materials and then sending the item out from the convenience store itself instead of making an extra trip to the post office or delivery service. A barcode will be generated in the app that the conbini worker scans and just follow their instructions from there. Easy as pie.

### Profit!
As long as you’ve set up your 振り込み申請 (Furi-Komi shinsei bank transfer application) properly, your proceeds will be transferred into your bank account on a certain day minus the standard bank transfer fee of ¥220. Another alternative is to use their MerPay service which gives you a virtual account to spend from on either the Mercari site itself or by using barcodes at participating shops and even a virtual MasterCard debit card in ApplePay and the iD eMoney network (my preferred route) 

### For more information...
Here are some other useful tidbits about Mercari in English on the web:
- [Reddit posting about selling on Mercari](https://reddit.com/r/japanlife/comments/8vf1ic/selling_on_mercari/)
- [Annette in Japan blog: selling on Mercari](adventuresofanette.blogspot.com/2018/02/how-to-sell-things-on-mercari-japan.html) 