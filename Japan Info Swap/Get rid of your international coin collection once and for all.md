## Get rid of your international coin collection once and for all

Like any other expat living here in Japan, I’ve taken to using the proximity to the rest of Asia along with cheap airfare combined to go on some sweet vacations to places like Korea and Hong Kong. I even worked for a company based in Taiwan meaning there was a heap of back-and-forth travel there too. All of this added up to a large pile of coins stashed in a desk drawer. Almost US$75 worth actually... and because of many bank’s rules, there is no way to exchange coins... Bills yes, of course, but not the round pieces of metal jingling around in my pockets. And just like Japan, some of these places have coins that are worth a lot. 

Then I heard of a company called [Pocket Change](https://www.pocket-change.jp/en/) and it totally changed my habit of trying as much as possible to spend every last coin somehow before getting on the plane home, to just bringing it back with me and dealing with it on my own time in Tokyo. 

Pocket Change machines are found in most every major international airport in Japan, and in the case of Tokyo, near places like Shinjuku, Shibuya and Tokyo station. The idea is simple: just dump your bounty of coins into the machine, and get the market exchange rate value returned to you in the form of e-money from [Rakuten Edy](https://edy.rakuten.co.jp), [Aeon WAON](https://www.waon.net), Apple App Store or iTunes, or an [Amazon](https://amazon.co.jp) credit. Also this works if your needs are elsewhere too; for example you can get a [Target](http://target.com) gift card for use in the US or donate to UNICEF and other charities.

I had no problem converting my leftover American Dollars along with some Taiwanese New Dollars into Edy and Amazon credits; the whole process from start to finish was about 5 minutes. 

For more information about updates to their services and new locations, check out [https://www.pocket-change.jp/en/](https://www.pocket-change.jp/en/).

---
-- By [Jason L. Gatewood](http://jlgatewood.com)
*All Images: Pocket-Change machine in HIS Shinjuku. Jason L Gatewood, Own Work.*