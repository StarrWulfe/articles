# Let’s Tour: Sendai, The City of Trees
## What to see

For a city the size that it is (just over one million people, but that still behind places like Hiroshima and Kawasaki), Sendai has a storied past, steeped in nature, and has a vibrant lifestyle for its citizens. This all adds up to a really good place to check out if you’re looking for a change of pace from the hustle and bustle of the Tokyo Metropolis and seek to avoid the endless tourist throngs around places like Kyoto and Nara to the west. 

### Date Masamune was here
Sendai would still just be a fishing village if it wasn’t for the feudal lord Date Masamune (pronounced “DAH-TAY”, not like the word meaning a romantic eouting!) Known as the “One-Eyed Dragon of Ōshu” because of losing his right eye to smallpox (which may have been part of a poison his mother gave him according to one legend) his ambitious plans to grow his domain were only rivaled by his cunning tactics in both battle and alliances. His likeness can be seen all over the city with his trademark black and gold-trimmed battle armor and large golden moon crescent atop his helmet. 

#### Aoba-jo (Sendai Castle)
The best place to learn about the man behind the place is to head atop the Mount Aoba Plateau in the middle of town and check out his old castle… ruins. Unfortunately the structure burnt down repeatedly over the years, most recently during bombing runs in WWII. The walls and foundations are still shown off and there is a large museum dedicated to the Date clan and their exploits. Also don’t miss the large statue of Date Masamune atop his steed that eternally looks east over the Hirose River through the city and into the sea beyond just as he likely did atop Mount Aoba over 400 years ago. It’s a prime photo spot and one of the best skyline views I’ve ever seen. 
~**Sendai Castle**~
*Address:* [Sendai Aoba Castle Site, Aoba, Sendai, Miyagi](https://maps.apple.com/?ll=38.252234,140.856609&spn=0.006131,0.013138&t=m)
*Web:* [http://honmarukaikan.com](honmarukaikan.com)
*Access:* Stop #6, [Loople Shuttle Bus](http://loople-sendai.jp/en/)

**~Zuihōden Mausoleum~**
You can also check out where Date Masamune is entombed along with three of his sons by visiting this temple. There are volunteer guides that speak English here along with a really good smartphone powered guide app as well. Just be prepared for the 70 steps that will await you when you arrive. Hey, gotta get that cardio workout in!
[23-2 Otamayashita, Aoba, Sendai, Miyagi](https://goo.gl/maps/1xn82mCLCLL2)
[ZUIHODEN  The mausoleum Masamune Date of the first feudal load  of Sendai-Han](https://www.zuihoden.com/en/)
*Access:* Stop #4, [Loople Shuttle Bus](http://loople-sendai.jp/en/)

**~Osaki Hachiman Shrine~**
Sendai’s main Shinto shrine is located just west of the center of town and founded by Date Masamune himself, dedicated to Hachiman, the god of war. Its most famous festival, Dontosai is held over the New Year holidays during the time while people make their first shrine visits of the year, they drop off their old *omamori* good luck charms and get new ones. The old ones are burned in a huge bonfire to ward off the bad luck that was collected over the previous year. Of course you are welcome to check out the temple grounds any other time too and marvel at it’s black lacquer and gold leaf construction and like me, wonder why there are wild chickens just running round the place. 
[4-6-1 Hachiman, Aoba, Sendai, Miyagi](https://www.google.com/maps/place/Osaki+Hachimangu+Shrine/@38.2725243,140.8428373,17z/data=!3m1!4b1!4m5!3m4!1s0x5f8a29c853ee5bdd:0x4e765606fdf81f9b!8m2!3d38.2725243!4d140.845026?hl=en-US)
[http://www.oosaki-hachiman.or.jp/](http://www.oosaki-hachiman.or.jp/)
*Access:* Stop #12, [Loople Shuttle Bus](http://loople-sendai.jp/en/)

### Wild and Wonderful
One fascination I have with Sendai is it’s likeness to the city I hail from, Atlanta in the United States. Even though it’s a metro area of just under 5 million, there are tall pines, winding rivers, rolling hills and stone mountains all over. Sendai looks exactly the same with its spreading oaks, rocky bluffs and the Hirose River cutting straight through the middle of town. There are plenty of parks and historical sites that offer a nice respite to allow you to reset your mind and spirit while bathing in nature’s glow, from mountain to sea and everything in between.

#### Akiu Great Falls Area
**~Rairai Canyon~**
This area in the southwestern part of town is home to many hot springs and resort hotels centered around this natural formation the Natori river makes as it carves its way through the area. 
[ 62 Akiumachi-yumoto,  Taihaku-Ku, Sendai, Miyagi](https://maps.apple.com/?address=Akiumachiyumoto%2062,%20Taihaku-Ku,%20Sendai,%20Miyagi,%20Japan%20982-0241&auid=16733460082831148154&ll=38.224549,140.728942&lsp=9902&q=Rairai%20Valley&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEHMKBAgKEAASJCkP/o9OCRxDQDGjNOSxKJdhQDmN07WqLx1DQEEPLz9ehpdhQA%3D%3D&t=m)

**~Akiu Great Falls~**
Further up river lies one of Japan’s steepest waterfalls. Surrounded by the natural pine and oak forest, take a walk down to the bottom of the falls and enjoy the natural temperature difference (Much cooler down there in summer!) When you're done, come back up and head over to the nearby botanical gardens and shopping street… Or how about some soba noodles at [Michelin ranked Jōan](https://tabelog.com/en/miyagi/A0401/A040105/4000132/dtlrvwlst/)?
[Akiumachi-baba Taihaku-Ku, Sendai, Miyagi](https://maps.apple.com/?ll=38.274683,140.603852&spn=0.003524,0.006763&t=m)

#### Sakunami Area
**~Nikka Whisky Sendai Factory~**
The Sendai area is known for its great *sake* and *nihon-shu* rice spirits, but did you also know the clear mountain streams are also great for making whiskey? The founder, [Masataka Taketsuru](https://en.wikipedia.org/wiki/Masataka_Taketsuru) did in 1967 when he saw the water in the Nikkawa River was crystal clear. Having already established his first brewery in Hokkaido, he wanted to expand the business and create a contrasting taste to what was being done at the Yoichi Brewery. For that reason the Miyagikyo Brewery has a different set of equipment and process that helps round out the Nikka brand. The factory grounds are also very picturesque with red brick buildings and wrought iron gates competing with the ponds, trees and grassy knolls for attention against the mountainous backdrop. I asked one of the factory workers candidly what they liked about working there, and he replied simply “all this nature”. I agree… Along with the added benefit of being able to partake in some of Japan’s finest libations that are the product of the factory! OF COURSE there’s a tasting after the tour, and you will not be disappointed. 
For more information on tours, [Check out Nikka’s Miyakyo Distillery website.](https://www.nikka.com/eng/distilleries/miyagikyo/)
[Nikka 1, Aoba, Sendai City, Miyagi](https://maps.google.com/maps?ll=38.308059,140.65044&z=16&t=m&hl=en-US&gl=JP&mapclient=embed&cid=514090486589361486)

**~Okura Dam and Recreation Area~**
The far western part of Sendai is very mountainous and the headwaters of all the area’s streams can be found here. Of course this makes for a great place to plop a dam down and generate clean electricity while using the resulting reservoir to get drinking water from. Somewhere in the process, someone noticed how beautiful everything looked and made many of these locations into parks. The Okura Dam and surrounding prefectural park is a great area to visit and take in the surrounding scenery especially in spring cherry blossom and fall foliage seasons. Also the park at the bottom of the dam is apparently renowned for its picnicking area.
[1 Okura-Fuda, Aoba, Sendai, Miyagi](https://goo.gl/maps/MuY5UoNK1uJ2)

**~Johgi Nyorai Saihoji Temple~**
Follow the Okura River up through the gorge another 15 minutes and you’ll arrive at Johgi Nyorai Temple tucked into the valley. Famous for being the place to pray for matchmaking, safe childbirth and having a happy home in general, the temple grounds itself is surrounded by nature and you perfectly welcome to simply stroll the grounds admiring the koi ponds, trees that burst into golds and reds during fall and the sounds of birds all around. The temple itself has a fabled history as it houses a relic painting of the Kannon Buddha that’s kept in the main temple. There’s also a five-storied pagoda and a nice market street that is home to a very famous [fried tofu joint](Insert URL for that part here!) that deserves its own write up! 
Web: [https://jogi.jp/](https://jogi.jp/)
Phone: 022-393-2011
[1 Ōkura-Jōge, Aoba, Sendai, Miyagi](https://goo.gl/maps/QTceHWgbz6G2)

### Ganbatte Sendai, Ganbatte Tohoku!
The phrase *ganbatte* loosely translates into “do your best” or “don’t give up” and following the Great East Japan Earthquake and Tsunami was something said, heard and seen almost everywhere in Japan. The eastern portion of Sendai, made up primarily of flat farmland, industrial zones, scattered residential areas and beaches was very hard hit on March 11, 2011 when a magnitude 9.0 earthquake struck and generated a 10 meter wave washing ashore about an hour later. If you visit the area, it would be a great thing to visit some of the areas that took the brunt of the disaster and have risen from the rubble anew.

**~Arahama Elementary School Memorial ~**
The full gravity of the disaster can be felt here, as this school saved the 320 people that evacuated from the nearby neighborhood on the coast that day. Water crept up to the middle of the 2nd floor bringing in all manner of debris including cars, furniture and other possessions. The town the children lived in that attended this school was totally destroyed as was the school’s newer gymnasium next door. It was decided the school should stand to show the power of nature, and the bottom 2 floors have been left as they were once the water receded; the top floor exhibits photos and movies of the disaster and includes the chalkboards left as the children saw them that day. 
[2-1 Arahama Shinborihata, Wakabayashi, Sendai, Miyagi](https://goo.gl/maps/CkTK1JKLHS72)
[Sendai City Disaster Memorial](http://www.city.sendai.jp/kankyo/shisetsu/shinsaimemorial2.html)

**~Sendai Coast Equestrian Center~**
Humans weren’t the only thing rescued that day; just down the street is an Olympic class horse riding training facility, and  over 20 of its horses needed to be evacuated and sent to other facilities in the aftermath. Three of them have returned since the facility reopened this year and you can ride, pet and feed them. 
[1 Idoji-Aza-Numako, Wakabayashi, Sendai, Miyagi](https://goo.gl/maps/VSWpjvby7AJ2)
[www.kaigankoen-bajyutsu.jp](www.kaigankoen-bajyutsu.jp)

**~Seaside Adventure City Park~**
Sitting on the other side of the horse stables is a park situated on top of a hill and because of this was mostly spared from being inundated totally by tsunami waters. Even so, the park known for its hands-on recreation and learning programs for local kids had to go on hiatus for the last 7 years and has only now reopened this past July. Home to a “day camp site” where you can barbecue in the warm months, a large playground and sandbox for the kids, it’s a nice place to relax and catch a breath of fresh air with the Sendai skyline on one side and Pacific Ocean on the other.
[1-139 Idoji-Aza-Kaihatsu, Wakabayashi, Sendai, Miyagi](https://goo.gl/maps/usXq6xzjmJA2)
[Seaside Adventure Park Website](http://honyaku.j-server.com/LUCSENDAI/ns/tl.cgi/http%3a//www.city.sendai.jp/shisetsukanri/kaigankoen/saikai.html?SLANG=ja&TLANG=en&XMODE=0&XCHARSET=utf-8&XJSID=0)


- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
*Images: [WORKNAME]() by [PERSON](), [CC~~]()*