# Let's tour: Odaiba

Author: Jason L Gatewood
Last Edited: Jan 22, 2020 3:32 PM
Status: Published

You may not know the Odaiba district by name before coming to Japan, but you have certainly seen its various landmarks turn up in almost every form of media that showcases modern Tokyo. The triangular inverted pyramid that forms one part of [Tokyo Big Sight](https://en.wikipedia.org/wiki/Tokyo_Big_Sight)— our sprawling convention center, the ginormous Ferris wheel that looms over the Pallete Town leisure area, and the even more imposing Rainbow Bridge, connecting the central Tokyo mainland to the island, you’ve no doubt seen it before. Right down to the in-real-life sized Gundam robot that stands guard over the place. But why are these things located on an island sitting on the edge of Tokyo Bay?

### The Batteries that Protected Edo

Somewhere around 1853, it was decided that there needed to be some sort of fortifications were needed on the coast to protect the city (then known as Edo) from those pesky Americans lead by Commodore Perry who were trying to force open the doors of trade with the reclusive shogunate. Small islands were dredged out of the seabed and fitted with cannons batteries, which in Japanese, is called *”Daiba” (台場)*. Although 11 wer planned, only 6 were ever built. In fact, the third one is now a [park that you can visit](https://maps.apple.com/?address=Daiba%20Park,%2010,%20Daiba%201-Ch%C5%8Dme,%20Minato-Ku,%20Tokyo,%20Japan%20135-0091&auid=5307717383980868306&ll=35.633442,139.772036&lsp=9902&q=Daiba%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJCn6epGKicVBQDEe/QsRKHVhQDmHJ0hhnNxBQEHgAvT4QHxhQA%3D%3D&t=r) and see exactly how it was over 160 years ago. Later on during WWII, it was decided the area surrounding the batteries would become a port, so the shallows were dredged and all that dirt needed to go somewhere, thus forming the foundations to the artificial islands system. After the war, these became a landfill used for getting rid of the rubble of the air raids and construction waste in the ensuing boom. Finally it was around 1992 when a major construction project was launched to turn the area into mostly what we see today. 

### Tokyo’s Leisure World

Because it has so much flat open space, yet close to the center of town, Odaiba is known as an oasis of leisure for many a Tokyoite. There are many parks, plazas and recreation areas to enjoy here. But the biggest sport that is most represented is probably shopping; there are 8 major shopping malls, each with its own set of exclusives and themes. 

[Decks Tokyo Beach](https://maps.apple.com/?address=6-1,%20Daiba%201-Ch%C5%8Dme,%20Minato,%20Tokyo,%20Japan%20135-0091&auid=12969453630424634412&ll=35.629090,139.775783&lsp=9902&q=DECKS%20Tokyo%20Beach&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEIcCCgQIChAAEiYp/VjSjPHPQUAxZYTo96Z4YUA5ey746BfRQUBB60izgQF5YUBQBA%3D%3D&t=r) is the home of [Legoland Tokyo](https://tokyo.legolanddiscoverycenter.jp/en/), [Sega Joyopolis](https://tokyo-joypolis.com/language/english/), and the Odaiba Ichibangai floor which includes a video arcade with games from the latter 20th century and before. 

![Let%20s%20tour%20Odaiba/5CD893B7-C23B-4C42-B2B9-22A9BC009581.jpeg](Let%20s%20tour%20Odaiba/5CD893B7-C23B-4C42-B2B9-22A9BC009581.jpeg)

![Let%20s%20tour%20Odaiba/5621528A-5CDD-469A-9F50-2369DE200DAF.jpeg](Let%20s%20tour%20Odaiba/5621528A-5CDD-469A-9F50-2369DE200DAF.jpeg)

Next door is [Aqua City,](https://www.aquacity.jp.e.yp.hp.transer.com/lng/top/) another mall with everything from a [United Cinema movie theater](https://www.aquacity.jp.e.yp.hp.transer.com/cinema/) with a sound system, motion chairs and a wrap-around screen making it more of an active than a passive way to take in the cinema, and an expansive list of restaurants that include the [Ramen Kokugikan](https://www.aquacity.jp/tokyo_ramen_kokugikan/original.html), where many famous one-off noodle shops can be sampled in one place.

![Let%20s%20tour%20Odaiba/aqua_city_jlg.jpg](Let%20s%20tour%20Odaiba/aqua_city_jlg.jpg)

Just on the other side of Fuji Television's headquarters building (which looks like Minecraft and Lego had a baby), is [Diver City](https://maps.apple.com/?address=1-10,%20Aomi%201-Ch%C5%8Dme,%20Koto-Ku,%20Tokyo,%20Japan%20135-0064&auid=11422391837857591620&ll=35.625192,139.775501&lsp=9902&q=Diver%20City%20Tokyo%20Plaza&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEIcCCgQIChAAEiYpQTLTnXjPQUAxJVIaoqd4YUA5vwf5+Z7QQUBB1zHTKgJ5YUBQBA%3D%3D&t=m), which has a very large [Round One Spocha](https://morethanrelo.com/en/round-one-spocha-is-the-mother-of-all-game-centers/) game center on the top level as well as being the home of the life-sized [Unicorn Gundam statue](https://www.unicorn-gundam-statue.jp) I mentioned earlier. Hey, it's a bit more authentic [that other statue](https://www.notion.so/Let-s-tour-Odaiba-df9a043a81fa44ab9a383716503ea6b6) over by Aqua City though! Because of this, there are various Gundam related shops in the mall for those of you that want to satiate your giant robot anime urges. 

![Let%20s%20tour%20Odaiba/odaiba_unicorn_gundam_jlg.jpg](Let%20s%20tour%20Odaiba/odaiba_unicorn_gundam_jlg.jpg)

Just south of there is [Odaiba Oedo Onsen Monogatari](https://morethanrelo.com/en/tokyo-area-super-sento/), should you find yourself in need of a soak in the hot springs. Also don't miss the Venus complex just east of the Gundam statue; it's home to the [Toyota City Showcase and Megaweb History Garage](https://morethanrelo.com/en/toyota-megaweb-showcase/). If you're into cars, then this is the place for you. 

### Odaiba Outdoors

The other thing you'll notice about this part of town is that there are an abundance of parks, plazas and wide pedestrian pathways connecting everything together. This is by design of course. The original masterplan for Odaiba was for it to be a series of high-rise communities in its interior, surrounded by coastal parks. This has mostly remained true for the westernmost section of the island. Just north of Decks and Aqua City is [Odaiba Seaside Park](https://maps.apple.com/?address=%E3%80%92135-0091,%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%AF%E5%8C%BA,%20%E5%8F%B0%E5%A0%B41%E4%B8%81%E7%9B%AE&auid=17052584374012580352&ll=35.631419,139.777679&lsp=9902&q=Odaiba%20Kaihin%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJCmgeq8/R8VBQDEk3NpRVnVhQDlfqOcWWtxBQEHcIyUub3xhQA%3D%3D&t=m) and to the west of Aqua City and Diver City is Shiokaze Park. Both of these coastal parks have space for all kinds of recreation, whether you want to get in a jog or simply lay out in the sun. Swimming isn't allowed, but you are welcome to wade and splash about in the shallows just off the beaches as well.  

### Getting there

The easiest way to get to Odaiba is via either the [Tokyo Waterfront Rinkai Line](https://www.twr.co.jp/en/tabid/233/Default.aspx) or the [Yurikamome automated transit line](https://www.yurikamome.co.jp/en/). The Rinkai line is connected to JR's Saikyo line and makes stops at Ikebukuro, Shinjuku, Shibuya and Osaki stations, making it very convenient for making connections to other train lines. The Yurikamome connects at JR's Shimbashi station along with the Asakusa and Ginza subway lines. It's a little more expensive but since it crosses over the Rainbow Bridge, the views from the train are spectacular. Speaking of which, you can also walk or bike your way across the Rainbow Bridge itself; there's pedestrian access just east of Tamachi Station, and takes about 45 minutes to cross into or out of Odaiba Seaside Park.