# Tokyo Area Flea Markets
If you're searching for a truly unique curio or gift for someone here around Tokyo or are looking to get a one-of-a-kind item at a really low price, then you should check out one of the many flea markets happening around the metropolis. All kinds of treasures (and a little junk) abound at these bargain-hunting street markets, from artwork and clothing to toys, games and cookware (and sometimes the food itself!) There's a little everything out there.

While the chance is very high that there's a "street market" going on somewhere around Greater Tokyo any given weekend, for some unknown reason the month of February seems to be chock full of some pretty famous ones. So if you needed a reason to escape the comfort of your futon or kotatsu on a wintery weekend in February, here's your chance!

#### Tokyo International Forum Oedo Antique Market
We will start with the biggest one in town, held in the area between Tokyo and Yurakucho stations every first and third Sunday of the month. Upwards of around 250 separate sellers are out on the street ready to hawk their wares. Since it's near the Ginza, Marunouchi and Shinbashi areas, there are plenty of ways to get out of the elements and take a break too. 

**Dates & Times:** February 3・17, 9:00～16:00
**Address:** [3-5-1 Marunouchi, Chiyoda, Tokyo](https://maps.apple.com/?address=5-1,%20Marunouchi%203-Ch%C5%8Dme,%20Chiyoda-Ku,%20Tokyo,%20Japan%20100-0005&ll=35.676970,139.763633&q=5-1,%20Marunouchi%203-Ch%C5%8Dme&_ext=EiYpLcSPxxPWQUAx5JfIYUJ4YUA5q5m1IzrXQUBBUDJ8+Zx4YUBQBA%3D%3D&t=r)
**Access:** Tokyo Station, Yurakucho Station, Hibiya Station
**Web:** [https://www.antique-market.jp/english/](https://www.antique-market.jp/english/)

#### Yoyogi Park Keyaki-Namiki Oedo Antique Market
Usually held around the outdoor stage and pathway between NHK Broadcast Center and Yoyogi National Gymnasium, it qualifies as the second largest running flea market in Tokyo with over 150 sellers. An easy stroll to take if you're headed between Harajuku and Shibuya as well. There's usually food stands open here too if you're looking for a simple snack while you bargain hunt. 

**Dates & Times:** February 24, 8:00～16:
**Address:** [2-2-1 Jinnan, Shibuya-Ku, Tokyo](https://maps.apple.com/?address=2,%20Jinnan%202-Ch%C5%8Dme,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0041&auid=15028645876250748645&ll=35.666595,139.697809&lsp=9902&q=Yoyogikoenkeyakinamiki&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJilgJGpJmNRBQDFF9hbOG3ZhQDne+Y+lvtVBQEEFNm1idnZhQFAE&t=r)
**Access:** Harajuku Station, Meiji Jingu Mae Station [JY] [C] [F]

#### Aoyama Weekly Antique Market

Held on the grounds of the United Nations University, this market is a weekly event every Saturday, but depending on the season and week is bigger and sports a theme. Sometimes concurrent events such as the World Coffee Market take place at the same time too, so there's always something to explore in this smaller venue. Plus its around the corner from Omotesando in Aoyama, so the 30 or so sellers are a bit more upscale here too. 

**Dates & Times:** February 2・9・16・23 (every Saturday)
**Address:** [5-53-70, Jingumae Shibuya, Tokyo](https://maps.apple.com/?address=53-70,%20Jingumae%205-Ch%C5%8Dme,%20Shibuya,%20Tokyo,%20Japan%20150-0001&auid=8341885731051049074&ll=35.662357,139.708262&lsp=9902&q=United%20Nations%20University&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBkKBAgKEAASJikW+VDuNNRBQDHZ5mPMfHZhQDmUznZKW9VBQEETuNhf13ZhQFAE&t=r)
**Access:** Omotesando Station [C][G][Z]
**Web:** [http://www.thejmp.com/](http://www.thejmp.com/)

#### Hanazono Shrine Weekly Market

The shrine nicknamed "The Guardian of Shinjuku" holds a pretty popular weekly antiques flea market as well every Sunday with about 30 booths hawking everything from woodcraft to stamp and coin collections. It's pretty easy to get to as well, located just east of Kabukicho and Shinjuku station. It gets started quite early, so don't be suprised to see plenty of bleary-eyed all-nighters passing you on the way in!  

**Dates & Times:** February 3・10・17・24 (every Sunday), 6:30~15:30
**Address:** [Shinjuku 5-17-3, Shinjuku-ku, Tokyo](https://maps.apple.com/?address=17-3,%20Shinjuku%205-Ch%C5%8Dme,%20Shinjuku-Ku,%20Tokyo,%20Japan%20160-0022&ll=35.693508,139.705211&q=17-3,%20Shinjuku%205-Ch%C5%8Dme&_ext=EiYpWZgqrjHYQUAxi0A+yWN2YUA5121QCljZQUBBgbDAZb52YUBQBA%3D%3D&t=r)
**Access:** Shinjuku 3-Chome Station [C][F][M]
**Web:** [http://www.thejmp.com/](http://www.thejmp.com/)

#### Tokyo City Flea Market

The mother of all flea markets happens most weekends down at Oi Racetrack in Shinagawa Ward when the ponies are in their off-season, which means pretty much all winter. I've been able to find vintage videogames, older PCs, a decent coffee maker, a few jimbei and more for chump change here. You know it's big when the shipping companies have booths so you can ship your found bargains back to your home without worry!


**Dates & Times:** February 2,3・9,10・16,17・23,24 (every weekend), 6:30~15:30
**Address:** [2-1-2 Katsushima, Shinagawa, Tokyo](https://maps.apple.com/?address=1-2,%20Katsushima%202-Ch%C5%8Dme,%20Shinagawa-Ku,%20Tokyo,%20Japan%20140-0012&ll=35.593640,139.741414&q=1-2,%20Katsushima%202-Ch%C5%8Dme&_ext=EiYpT6EEOGnLQUAxgvm2aox3YUA5zXYqlI/MQUBB2Ng/6uZ3YUBQBA%3D%3D&t=r)
**Access:** Oi Kebajo Mae [MO03], Tachiaigawa Station [KK06]
**Web:** [http://www.thejmp.com/](http://www.trx.jp/)

---
— By [Jason L. Gatewood](http://jlgatewood.com😩)
*Images: [WORKNAME]() by [PERSON](), [CC~~]()*
