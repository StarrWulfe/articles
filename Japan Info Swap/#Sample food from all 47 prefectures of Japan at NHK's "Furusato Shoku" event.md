# Sample food from all 47 prefectures of Japan at NHK's "Furusato Shoku" event

It's impossible. Even in my almost 2 decades of traveling all over Japan, I have yet to hit the magic number 47, the number of prefectures that make up Japan. This means there's forty-seven different regions that proclaim to be home to the best beauty, culture, and nature representing Japan. I don't know about that; I tend to enjoy all of Japan equally. But there's the most important category out there that always tests my regional loyalty: food. Whenever I venture to a new partmof Japan, I make sure to consult every online guide, app, and friend I have to see what *meibutsu no tabemono (名物の食物)* or famous food I should partake in the area. But since my travels have never taken me to certain places, I feel I've missed out. Luckily for me (and you if you go) NHK will bring their delicacies to us Tokyoites on the weekend of March 11 and 12th with their *[Furusato no Shoku Nippon no Shoku](https://www.nhk-p.co.jp/event/detail.php?id=854)* event.

*Furusato Shoku (ふるさと食)* can be translated to mean "food from one's hometown" and *Nippon no Shoku (にっぽんの食)* would be "Japanese food", and that's what NHK aims to showcase here. Whether you looking for octopus balls from Osaka, Fukuoka's famous ramen, or roasted cow tongue from Sendai, you're sure to find it in one of the stalls here. 

One goal of the exhibition is to help promote and support the ongoing rebuilding efforts after the Great Eastern Japan Earthquake and Tsunami that will have occurred seven ago, March 11, 2011. Also since we're in the ramp up to the 2020 Tokyo Olympics mode, there will also be an area exhibiting Japanese cooking as a "cultural export" of sorts as well as giving us something to look forward to in two years.  

There will be cooking demonstrations as well as guest appearances by NHK talents and characters. (Yay [Domokun](https://g.co/kgs/RaZ2U5)!) All and all, it should make for an enjoyable time out with family, friends, or just by yourself.

Website: https://www.nhk-p.co.jp/event/detail.php?id=854
When: March 10 & 11 (Saturday and Sunday) 10am~4pm.
Where: [NHK Studio Park
2 Chome-2-1 Jinnan, 渋谷区 Shibuya-ku, Tōkyō-to 150-8001, Japan](https://maps.google.com?q=NHK%20Studio%20Park,%202%20Chome-2-1%20Jinnan,%20%E6%B8%8B%E8%B0%B7%E5%8C%BA%20Shibuya-ku,%20T%C5%8Dky%C5%8D-to%20150-8001,%20Japan&ftid=0x60188cade7c66435:0xf033342ea6dfafe0&hl=en-US&gl=us)
Access: Shibuya Station, 15min walk; Harajuku/Jingu-Mae stations, 10min walk

---
*By [Jason L. Gatewood](http://linkedin.com/in/jlgatewood)*

Images: <a href='https://www.flickr.com/photos/skrb/2427310052/' target='_blank'>ふるさとの食 日本の食 全国フェスティバル</a>&quot;&nbsp;(<a rel='license' href='https://creativecommons.org/licenses/by-nc/2.0/' target='_blank'>CC BY-NC 2.0</a>)&nbsp;by&nbsp;<a xmlns:cc='http://creativecommons.org/ns#' rel='cc:attributionURL' property='cc:attributionName' href='https://www.flickr.com/people/skrb/' target='_blank'>yuichi.sakuraba</a>
