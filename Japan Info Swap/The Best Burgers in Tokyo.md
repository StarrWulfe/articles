# The Best Burgers in Tokyo
This list is not intended to be exhaustive; the sheer size and amount of places opening, closing and existing in Greater Tokyo demands an army of greasy, cheesy, burger loving writers to file addendums on a weekly basis in order to keep the list accurate. Since I am only one of this type, alone in the Metropolis for this publication, I am humbly forced to curtail the article to my opinion of good burger haunts.  As such, this means I can’t profess to tell how good chains like Shake Shack, Fatburger, Wendy’s, and the others are (they are pretty darn good though!), and will be limiting myself to the one-offs, “only in Tokyo” burger joints I’ve personally experienced first hand. *Yoroshiku onegaishimasu*.

### Grill Burger Club SASA
I’m not completely sure why this happened, but the Daikanyama/Naka Meguro area has become “burger central” in the last 2-3 years somehow.  The first of these shops I visited was the Grill Burger Club SASA located right next to Daikanyama station.  I had no idea what to expect until entering the shop. That’s when I saw the rotund manager/grill master doing his thing while wearing a wide smile and a copious amount of grease on his apron— this is always a sign that the food is gonna be epic! You can get anything from a standard cheese double to a gourmet mozzarella portobello with artisanal bread. And the aforementioned chef is very accommodating if you want to mix and match toppings and ideas from the menu. Go crazy all you want, just remember you will pay a bit more for the creativity. 

Address: 2-21-15 Ebisunishi, Shibuya, Tokyo
Weekdays: 11AM–9:30PM
Weekends: 11AM–8:30PM
Phone: 03-3770-1951
Web: [http://hijiriya.co.jp](http://www.hijiriya.co.jp/)


### BLACOWS
Japan’s most famous cuisine is sushi and ramen, but there’s also “wagyu 和牛”, the rich, marbled cuts of beef that comes from places like Kobe, Matsuzaka and Hida. Ever wondered what these tasty cuts would taste like in a burger? Wonder no more because you can head over to Ebisu and try it out at BLACOWS. The name is synonymous with the fact that they hand grind 100% “kuroge 黒毛” beef, literally black cows. All burgers here are custom; you select the toppings deli-style, right down to whether you want lettuce, tomato or onion. 

Address: 2-11-9 Ebisu, Shibuya, Tokyo
Weekdays:
Weekends:
Web:  [http://www.kuroge-wagyu.com/bc/](http://www.kuroge-wagyu.com/bc/)

### Homework’s
My first time eating an honest-to-goodness-for-real-American-style-in-Japan burger was way back in 2001 when I was brought to this shop in Hiroo by my coworkers. I ordered a double bacon cheeseburger that had my cheeks glistening with oil and mustard by the time I was halfway through it. To this day, if my travels carry me close to the area, It’s a given that I’m making a run over to Homework’s. The place has been around since 1985, and they haven’t done anything but continue to make juicy burgers out of 100% Aussie beef. Well, that and open a branch up in Azabu, so you now have 2 places to enjoy doing your hamburger homework!

Address: 5-1-20 Hiroo, Shibuya, Tokyo
Weekdays:
Weekends:
Web: http://www.homeworks-1.com/
