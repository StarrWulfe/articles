# JR Railway Museum
A good many visitors to Japan have been known to utter out loud, “there sure are a lot of trains in Japan!” …And they wouldn’t be wrong! We happen to have the [world’s highest per capita train ridership](https://en.wikipedia.org/wiki/List_of_countries_by_rail_usage#Passengers_carried_in_rail_transport_per_year) and are home to [The world’s busiest station](https://en.wikipedia.org/wiki/Train_station#Worldwide) as well. It is not an overstatement to say that modern-day Japan was built on rails and finding out just how this came to be is pretty interesting, not to mention makes for a good outing by yourself or with the family.

### So what can we find inside?
According to the Wikipedia page, [The Railway Museum](http://www.railway-museum.jp/en/) (鉄道博物館 Tetsudō Hakubutsukan) opened October 14th, 2007. It was built and is operated by the East Japan Railway Culture Foundation, a non-profit affiliate of the [East Japan Railway Company (JR East)](https://en.wikipedia.org/wiki/East_Japan_Railway_Company) as a successor to the original museum that used to be located just to the east of Ochanomizu station under the JR tracks near Akihabara.

Inside you’ll find restored and saved 30 railway cars covering  everything from steam engines all the way up to and including the Shinkansen “Bullet Train.”  Not to be missed are the train cab simulators which are every bit what you’d face if you were to pilot a JR train, down to the last switch and dial. There are an extensive array of modes railway dioramas so finely detailed, you’ll wind up looking for your particular station to see if they’ve captured it right..  This is a proper museum outside of the interactive bits, so there’s also some brain food for those who want to know all about Japan Rail from it’s inception in the late 1800’s through artifacts and books, video booths, and a research room. Rounding out the amenities, there’s a multi-purpose hall, a gallery balcony overlooking the Joetsu and Hokuriku Shinkansen lines, a cafeteria and children’s play area, and of course the museum shop,

### Getting There
**Access:** The Railway Museum is located next to [Tetsudo Hakubutsukan Station](https://goo.gl/maps/Yh7oPCtNv5N2) on the New Shuttle Line, one stop north of Omiya station. 

**Website:** [THE RAILWAY MUSEUM](http://www.railway-museum.jp/en/)
**Hours:** Everyday except Tuesdays, 10am-5:30pm; Closed December 29~January 1 for New Year’s holidays. 
- - - -
*---By [Jason L Gatewood](http://www.jlgatewood.com)*
Images:
* [Saitama Railway Park Zone](https://commons.wikimedia.org/w/index.php?curid=2912547) By Lover of Romance - Own work, [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/), 
* “[2012-06-02_JRRailwayMuseum002](https://www.flickr.com/photos/kevin_butt/7319598726/)" ([CC BY-NC 2.0](https://creativecommons.org/licenses/by-nc/2.0/)) by [theKNB](https://www.flickr.com/people/kevin_butt/)

