# Apps for Learning and Using Japanese
Part of living in Japan also means having to assimilate into one of the most complex language systems mankind has devised (or at least that’s how it feels at times.) The spoken language is a mixture of original Japanese words mixed with a heap of ancient and modern Chinese, English, Spanish, and Portuguese loanwords that are used everyday, sometimes in the same sentences. The writing system is a mix of Chinese ideograms, two collections of *kana*, characters used to represent the basic phonetical alphabet, and  thanks to a long history of international trade, roman letters and arabic numerals. It takes the average Japanese child 8 years of schooling to get to the point where a newspaper can be totally read easily. Are you ready for a challenge? We won’t focus on trying to get to NHK newscaster levels of fluency fast here; it’s more important to be able to communicate things like “where is the nearest grocery store” and “I’ve fallen and I can’t get up!” 

### Prep Time
Oh how I wish there were smartphones and apps when I started down the road to learning Nihongo. The amount of books and dictionaries I amassed back in the day is probably why I have no problem lugging heavy luggage on the Yamanote Line these days. Thankfully all you’ll need is your trusty pocket communication device (smartphone) and in most (but not all) cases, an internet connection. Before we get to the learning apps, we’d better make sure we have our device set up with the proper base apps for not only learning but daily life in Japan.
1. **Google Translate** You’re likely already using this app anyway, but perhaps not aware of the various functions it can do aside from typed translations. There’s the handwriting function, very useful if you need to scrawl in some Japanese kanji and don’t know how to type it in kana. You can also use the camera function to take a picture of some text and it will change it on the fly into the language of you choice. 
	* [‎Google Translate on the App Store](https://itunes.apple.com/us/app/google-translate/id414706506?mt=8)
	* [Google Translate on Google Play](https://play.google.com/store/apps/details?id=com.google.android.apps.translate&hl=en)
2. **Imiwa/JED** You’re gonna need a good English-Japanese dictionary to refer to whenever you get stuck trying to find the meaning to a word and all its uses, synonyms, and written forms. Both of these come at the low low price of free, and are based on the open-source JDICT project so as words get added to the lexicon, their databases are updated accordingly. 
	* [‎imiwa? on the App Store](https://itunes.apple.com/us/app/imiwa/id288499125?mt=8)
	* [JED Japanese Dictionary on Google Play](https://play.google.com/store/apps/details?id=com.umibouzu.jed)
	* [Yomiwa on Google Play](https://play.google.com/store/apps/details?id=com.yomiwa.yomiwa&hl=en)
	* [‎Yomiwa Japanese Dictionary on the App Store](https://itunes.apple.com/us/app/yomiwa-offline-translator/id670931120?mt=8)
3. **Google Keyboard** You’ll need to type in Japanese on your device too. Sure you could use the default keyboard app and perhaps switch it to Japanese, but it won’t have Google’s updated dictionaries but built-in search ability. 
	* [Google Keyboard on the App Store](https://itunes.apple.com/us/app/gboard-the-google-keyboard/id1091700242?mt=8)
	* [Google Keyboard on Google Play](https://play.google.com/store/apps/details?id=com.google.android.inputmethod.latin)

###  App-ified Learning
Now with that out the way, we can get down to the business of learning and understanding. The easiest way to learn anything is to break it up into small chunks, then make a daily routine out of ingesting and using the new vocabulary in real situations. Here are a few apps I’ve used on both platforms to both learn and keep my skills sharpened.
1. **Memrise** Context is always key when it comes to languages. Using the power of prerecorded video of common interactions so you can see Japanese in action, this app will have you remembering those “daily life phrases” in no time. I also liked using the drills in the app due to the gamification add-ons. 
	* [‎Memrise on the App Store](https://itunes.apple.com/us/app/memrise-language-learning/id635966718?mt=8)
	* [Memrise on Google Play](https://play.google.com/store/apps/details?id=com.memrise.android.memrisecompanion&hl=en)
2. **White Rabbit Press** This company has been making materials for Japanese study for a long time. If you’ve studied Japanese in a formal class, chances are you have touched a flashcard pack, reader, or audio CD from them. Their app combines all their expertise into an all-in-one method to help your reading and listening skills improve.
	* [Japanese Graded Readers for iOS and Android by White Rabbit Press](https://www.whiterabbitpress.com/japanesegradedreaders)
3. **Learning Japanese with Tae Kim** Sometimes you just need to know exactly when a certain grammar rule is used or what verb ending should be used to denote a certain context. This app will explain that not from an English but from a Japanese point of view. I found that by just treating the language as a blank slate with a completely different set of rules and throwing out any relation to English grammar, was I able to simply start using Japanese practically right away.
	* [Tae Kim's Guide to Learning Japanese in the App Store](https://itunes.apple.com/app/id1366698942)
	* [Tae Kim's Guide to Learning Japanese on Google Play](https://play.google.com/store/apps/details?id=com.alexisblaze.japanese_grammar)

### Let the games begin
Nothing beats rote practice. Back in the ancient times of the late 1990’s I could usually be found scanning index cards and small flash cards on a keyring to help reinforce my kanji and bunpo understanding. But that’s all dinosaur fodder now; we can practice and save the galaxy at the same time on our smartphones because they’ve gameified practice and quiz sessions!
1. [Japanese Dungeon: Learn J-Word on Google Play](https://play.google.com/store/apps/details?id=com.terryyoung.japanesedungeon)
2. [‎Learn Japanese by MindSnacks on the App Store](https://itunes.apple.com/us/app/learn-japanese-by-mindsnacks/id633683543?mt=8&app=itunes&ign-mpt=uo%3D4)
3. [Kanji Quest - study for JLPT in a fun Kanji Game ! - Apps on Google Play](https://play.google.com/store/apps/details?id=ism.game.guessthekanji)
4. [‎Kanji Mnemonics: Learn Japanese fast with Dr. Moku on the App Store](https://itunes.apple.com/us/app/kanji-mnemonics-learn-japanese-fast-with-dr-moku/id1209564595?mt=8&app=itunes&ign-mpt=uo%3D4)

### More to come…
There’s more out there in terms of apps and websites to help you get started on your journey into the world of Nihongo. Please feel free to add to this list by letting us know in the comments section! 
- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
*Images:*
[“Japanese JHS Japanese Class”](https://gogonihon.com), [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0)
[I英辞郎 2009](https://commons.wikimedia.org/wiki/File:I%E8%8B%B1%E8%BE%9E%E9%83%8E_2009_(3745606223).jpg) by [takako tominaga](https://www.flickr.com/people/69862132@N00), [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0)
![](Apps%20for%20Learning%20and%20Using%20Japanese/I%E8%8B%B1%E8%BE%9E%E9%83%8E_2009_(3745606223).jpg)
