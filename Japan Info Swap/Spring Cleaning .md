## Spring Cleaning 

You may have noticed not needing to crank the heater up as much, or seeing a bit more light in the skies past 5pm because  the days are slowly getting longer. Soon springtime will be here, along with a sudden urge to clean, organize and sort your entire abode. And for those of us from the West, this is a normal feeling, the Spring Cleaning ritual...but here in Japan, you're about 2~3 months late.

#####Mega-Cleaning
The yearly deep-cleaning ritual that Japanese do called *Osouji (お掃除)*, usually happens during the waning days of Decemeber as many seek to get their living spaces and workplaces sorted for the upcoming new year. Even schoolkids must dedicate a half-day or more of school to scrub their schools (there's no janitorial staff at most public schools). It's OK, we'll let you off the hook this time; Instead we'll tell you some tips to get your place spic 'n span with little fuss, so you can have your abode ready to greet the spring flowers in a few weeks.

#####¥100 shop is your friend
Of course you're going to need cleaning supplies and tools. While he temptation may be to just get whatever from the local grocery store, do yourself a favor and head to a ¥100 shop like [Daiso](https://en.wikipedia.org/wiki/Daiso), [Can-do ](https://www.cando-web.co.jp/e/index.html)or [Seria](http://www.seria-group.com/shop/). No need to go overboard on the price of your supplies! Things like dish detergent, glass and floor cleaner and the like can add up if you buy the brand name stuff after all.

#####Check your local garbage regulations
Throwing stuff out is hard work in Japan. It's not that difficult to get rid of old magazines and books you may have had stacking up; just consult your neighborhood or building [trash chart](https://sociorocketnewsen.files.wordpress.com/2014/05/nagoya068.jpg) so you can avoid the ["sticker of shame"](https://sociorocketnewsen.files.wordpress.com/2014/05/cimg15691.jpg) on your trash. Also [check out our article](https://japaninfoswap.com/separating-and-recycling-trash-nagoya/) on how to do this more in detail. 

#####Out with the old, in with the new
Ditching your old electronics like PCs, TVs, and such is really tough; in most of Tokyo, the regular garbage collection won't touch it and you'll need to pay for a third party to come and take it. But, if you're buying something new to replace the item in question, ask the shop clerk at the time of purchase if *they* can take away the old item for you. Another way is to contact your nearest "recycle shop" second-hand store and see if they can take if off your hands. In many cases, there's a noisy truck that will cruise your neighborhood with a speaker loudly proclaiming they'll take your used stuff if you flag them down. (Just don't get that truck confused for the [other loud trucks in Japan](https://en.wikipedia.org/wiki/Sound_trucks_in_Japan) unless you're the not-easily-embarrassed type.)

#####The art of the online deal
A good way of getting rid of *still useable* stuff is to sell or give it away online. [Craig's List](www.craigslist.com) is the grandaddy of them all when it comes to posting your stuff, and it's pretty safe bet over here in Nippon; I've had zero issues personally in the many years I've used it to buy and sell, but obviously common sense applies here. Because social media is a thing now, there are many Facebook groups dedicated to "Sayonara Sales" (like a garage sale to get rid of items before leaving the country) and even a few [bartering and giveaway groups](http://telegra.ph/Facebook-Groups-for-sellingbuyingtrading-used-stuff-02-17-2). 

##### Leave it to the professionals
If you "don't do windows..." floors, walls or ceilings for that matter, you can always call the professionals. Services like [Merry Maids](https://www.duskin.jp/merrymaids/) and [Casy](https://casy.co.jp/) can help when you feel like you're short on time and patience for cleaning.

---
*By [Jason L. Gatewood](http://linkedin.com/in/jlgatewood)*

Images: <a href='https://www.flickr.com/photos/ahaseg/5435277654/' target='_blank'>おそうじ</a>&quot;&nbsp;(<a rel='license' href='https://creativecommons.org/licenses/by-nc-nd/2.0/' target='_blank'>CC BY-NC-ND 2.0</a>)&nbsp;by&nbsp;<a xmlns:cc='http://creativecommons.org/ns#' rel='cc:attributionURL' property='cc:attributionName' href='https://www.flickr.com/people/ahaseg/' target='_blank'>ahaseg</a>