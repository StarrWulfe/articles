# Japan National Correctional Facilities Exhibition

Spending a weekend in jail is probably not at the top of most people's list, but if you've had a passing thought about what a Japanese prison is like, then just like everything else in JapanLand, there's a festival for it. Called "Correctional Exhibitions (矯正展)", they give a chance for the public to understand about life in a prison and also sell the goods the prisoners themselves make. Indeed, the life of one interred in one of Japan's jails is highly regimented and monitored, and there's very little idle time. The correctional facilities themselves don't exactly scream "Super Max" like they would elsewere; For example, the grounds of the Tokyo Detention House are more pristine than Ueno and Yoyogi Parks.

This year's theme, "Moving towards a brighter society" is intended to focus on how the National Correctional System focuses on finding methods to rehabilitate and improveme the lives of imprisoned inmates. The focus will be how the prison system has created a large trade school within every detention center that trains the interred to learn new skills and create new things to help give them something to be proud of and ease them back into the social fabric upon their release.

The products the prisoners make like totebags, shoes, tables and even barbeque pits, and soap. It will all be on exhibition and sale as well. The products are all of high quality, and some can be seen on [Capic's Instagram page]( 
https://www.instagram.com/capic01official/). Capic is the brand name for the National Correctional System's industry and sales division and stands for *Correctional Association Prison Industry Cooperation*. There's even a [shopping website](https://www.e-capic.com) in case Amazon isn't quite doing it for you.

Also note, you won't have access to actual prison facilities even if you're on the fringe areas of the grounds themselves... But if you insist on seeing inside an actual working jailhouse in Japan, I'm sure the police would be willing to show you in detail *over several years* if you try hard enough. Trust us, that's a tourist destination best taken out your bucket list.

### Access and Dates
**Date:** Saturday April 27 to Monday May 6 (Golden Week), 10am~4pm
**Place:** [Fuchu Prison Craft Goods Showroom](https://maps.google.com/maps?ll=35.681941,139.472041&z=19&t=m&hl=en-US&gl=JP&mapclient=embed&cid=5081203830515753994)
**Transit:** JR Kita Fuchu Station (JM34)

**Date:** Friday May 31, 10am~4:30pm & Saturday June 1, 9:30am~4pm
**Place:** [Kitanomaru Park, Science and Technology Museum (北の丸公園科学技術館)](https://maps.apple.com/?address=Science%20Museum,%20Chiyoda-Ku,%20Tokyo,%20Japan%20102-0091&auid=10730551143364463641&ll=35.691291,139.753072&lsp=6489&_ext=EiYpzNnxBunXQUAxtNIl3et3YUA5Sq8XYw/ZQUBBzi4DeUZ4YUBQBA%3D%3D&t=r)
**Transit:** Tokyo Metro Takebashi Station (T08), Metro/Toei Kudanshita Station (S05)(T07)(Z06) 
**Website:** [http://www.moj.go.jp/kyousei1/kyousei05_00026.html](http://www.moj.go.jp/kyousei1/kyousei05_00026.html)

http://www.moj.go.jp/kyousei1/kyousei05_00026.html
