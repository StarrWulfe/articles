# Sumo Restaurants

Of course you know about Japan's national sport, sumo. If not, then [here are a few ways to familiarize yourself](https://morethanrelo.com/en/tag/sumo/) with this original form of wrestling. You might ask yourself, "how do those guys gain all that weight, but still maintain their stamina and flexibility?" Despite what you might think, sumo wrestlers are known for their strict adherence to traditions in their lifestyle and training, so of course when dieting, they also include a mix of new and old recipes to maintain their strength and health. This is evident in their food of choice, *chankonabe (ちゃんこ鍋),* a stew filled with meat, tofu and veggies designed to help pack on the weight when eaten in large quantities. Usually prepared by the younger members of a sumo training stable, it's literally everything but the kitchen sink in terms of it's ingredients. But it's also a darn good way to stave off winter's coldness by us regular folk as well as yet another way to put another tick on your "Authentic Japanese Food I've Eaten" checklist. 

### Kappo Yoshiba

![Sumo%20Restaurants/p_02.jpg](Sumo%20Restaurants/p_02.jpg)

If you want the whole experience and feel of being at the sumo matches along with your dining experience, then look no further than Kappo Yoshiba. The building itself is steeped in history, for it is the home of the Miyagino Sumo Stable, boasting a historic *dohyo (土俵)* sumo ring in the middle of the dining space. Their menu includes fresh sushi from Toyosu market, and their special "Yoshiba Chanko Nabe" is full of seafood, fresh meat and vegetables. 

**Address:** [2-14-5 Yokoami, Sumida-ku, Tokyo](https://maps.apple.com/?address=14-5,%20Yokoami%202-Ch%C5%8Dme,%20Sumida-Ku,%20Tokyo,%20Japan%20130-0015&auid=16600319414690906568&ll=35.701813,139.795662&lsp=9902&q=Yoshiba&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBQKBAgKEAASJikESenUQdlBQDFjGrHASHlhQDmCHg8xaNpBQEF9EJ5fo3lhQFAE)

**Web:** [https://www.kapou-yoshiba.jp/english](https://www.kapou-yoshiba.jp/english)
**Phone:** 03-3623-4480, FAX 03-3623-4561
**Hours:** Mon-Sat 11:30 am - 1:30 pm , 5:00 pm - 9:10 pm. Closed Sundays and public holidays, Golden Week, O-Bon holidays, New Year holidays. Open for dinner on Sundays & public holidays during sumo tournaments

### Chanko Kirishima

![Sumo%20Restaurants/chanko kirishima.jpg](Sumo%20Restaurants/chanko kirishima.jpg)

["chanko, chanko kirishima, ryogoku, tokyo"](https://www.flickr.com/photos/enakamura/3495108185/) [(CC BY-NC-SA 2.0)](https://creativecommons.org/licenses/by-nc-sa/2.0/) by [etsuko.nakamura](https://www.flickr.com/people/enakamura/)

Like many chanko nabe joints, this one was started by a former pro sumo wrestler. One of the top Ozeki grapplers during the early 1990's, Kirishima Kazuhiro went on to coach his own stable, but has since retired from that life and now runs his restaurant full time. 

**Address:** [2-13-7 Ryogoku, Sumida, Tokyo](https://maps.apple.com/?address=13-7,%20Ryogoku%202-Ch%C5%8Dme,%20Sumida-Ku,%20Tokyo,%20Japan%20130-0026&auid=4893184662655886869&ll=35.695642,139.791564&lsp=9902&q=Chanko%20Kirishima%20Ryogoku%20Honten&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBQKBAgKEAASJikGNMeZd9hBQDFMXIIwJ3lhQDmECe31ndlBQEHgrqPNgXlhQFAE) 
**Web**: [http://www.j-kirishima.com/](http://www.j-kirishima.com/c/index.html#&slider1=2)
**Phone:** 03-3634-0075
**Hours:** Mon -  Sat: 11:30am – 11:00pm Sun & holidays: 11:30am – 10:30pm

###Chanko Tomoegata

![Sumo%20Restaurants/tomoegata_chanko_via_tripadvisor.jpg](Sumo%20Restaurants/tomoegata_chanko_via_tripadvisor.jpg)

via [TripAdvisor](https://www.tripadvisor.com/Restaurant_Review-g1066459-d1678317-Reviews-Chanko_Tomoegata-Sumida_Tokyo_Tokyo_Prefecture_Kanto.html#photos;aggregationId=&albumid=101&filter=7&ff=438050274))

Consistently rated the best chanko nabe restaurant in Ryogoku (which as you can tell by now is chanko and sumo heaven), Tomoegata has crafted their recipe over 40 years of continuous operation. 

**Address:** [2-17-6 Ryogoku, Sumida , Tokyo](https://maps.apple.com/?address=17-6,%20Ryogoku%202-Ch%C5%8Dme,%20Sumida-Ku,%20Tokyo,%20Japan%20130-0026&auid=6349796246091516176&ll=35.694679,139.792546&lsp=9902&q=Chanko%20Tomoegata&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBQKBAgKEAASJilp8jkNWNhBQDF0wdc7L3lhQDnnx19pftlBQEGMYrHYiXlhQFAE) 
**Phone:** 03-3632-5600
**Website:** [https://www.tomoegata.com](https://www.tomoegata.com/)