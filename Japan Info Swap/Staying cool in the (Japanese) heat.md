# Staying cool in the (Japanese) heat
Our latest weather woes have made [international headlines](https://edition.cnn.com/2018/07/23/asia/japan-heatwave-deadly-intl/index.html) recently, with an unprecedented heatwave that has set records across Japan. Many of us come from countries where this is quite normal (my last summer trip back home had highs around 40ºC/101ºF), a large number may not have ever experienced this brand of “hot n’ humid.” Even for those of us non-neophytes are wondering what the Japanese equivalent for all the hot weather gear is  and where to find them. Wonder no more because thanks to relentless investigating (mostly by harassing my friends and the local drug store staff in my neighborhood) I’ve put together a little list of things you should have on hand to survive the Deathball-In-The-Sky.

1. **Aquarius + Thermos bottle = happiness**
If you check out the local school kid population as they schlep off to and fro, you’ll hear a clanging and clattering noise thanks to the multiple bags they usually have filled with schoolbooks and athletic equipment. Usually they’ll also have a [“Thermos” style water canteen](https://goo.gl/images/VdbCFw) slung about themselves as well, likely filled with green tea or simply cold water. But we can go one better by getting [Aquarius branded drink powder](https://www.cocacola.co.jp/brands/aquarius/aquarius02) to the water that add necessary electrolytes and salts that we end up sweating out just walking around in the blazing heat. You can find the mix packs at any supermarket or drugstore. Bonus: you save money by taking your drink around rather than using a vending machine or convenience store every time as well.

2. **Insta-cool Ice Packs**
In the winter months, we already talked about the ingenious “kairo” heat pads that generate heat through a chemical reaction and keep your hands and feet toasty. These are the summer equivalent, instant ice packs that chill instantly when you punch the center of them. For maximum effectiveness, place one in a towel, then rap the towel about your neck in a way that keeps the pack affixed to the back of the neck. This cools the blood heading to your brain and keeps you from getting dizzy from the heat. These can be found almost anywhere you’d find the kairo packs in the winter.  Just ask for [“reikyakuzai 冷却材”](https://www.monotaro.com/p/6233/6417/?utm_medium=cpc&utm_source=Adwords&gclid=CjwKCAjw-dXaBRAEEiwAbwCi5pw1X-LPdTqr9U4pnB9BgRZxwi0II5PB5eLO0vA9_JwYRamzBC4fPRoC2fMQAvD_BwE).

3. **Salt Tablets**
Called “enbun hokyu ame 塩分補給飴” in Japanese, [these](https://www.monotaro.com/g/01253330/) also help keep your salt levels up which is critical to keeping water in your body instead of rapidly evaporating out, causing heatstroke. Again, easily found at any drugstore, supermarket and many convenience stores. 

4. **Third Spaces**
These are the places between home and work; coffee shops, malls, libraries, train stations and other public spaces. Since I spend way more time on public transport or walking in Tokyo than my old car bound life in the States, I have grown accustomed to using third spaces to get out of inclement weather, including the scorching heat. Now is a good time to check out that local coffee shop, new shopping area or even the public library in your part of Nihon!

- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
*Images: [WORKNAME}() by [PERSON](), [CC~~]()*


 