# 2019 Heisei Abdication & Reiwa Coronation

Unless you've made a concerted effort to avoid Japanese news headlines for the last two years, you already know that the current patriarch of the world's oldest royal family, Emperor Akihito, will step down from the Chrysanthemum Throne and make way for his son Prince Naruhito to be crowned. A new emperor calls for a new era name, and we know now that on April 30th 2019, the Heisei period will end and the Reiwa era starts on May 1st with the coronation of the new prince. 

This is all happening right in the middle of one of the Golden Week holiday period at the beginning of May, where Children's Day, Showa Day and Greenery Day usually form an almost week long period of time off. Now with the addition of the abdication and coronation ceremonies we will have an unprecedented ten day long stretch of time off... Well, that is if your workplace lets you take all of it off.

### The End of an Era
As the 125th emperor of the world's oldest continuous reigning monarchial line, Emperor Akihito has definitely seen a lot. Born December 23, 1933, he was child during the height of WWII and is surely the product of his father the Showa Emperor's teachings of pacifism and peace in the post-war period when he came of age. Upon his father's death on January 7, 1989, he assumed the title of Emperor and named his era Heisei (平成) which is translated usually as "Spreading Peace". Even before his ascension, he has been known for his unconventional approach to fulfilling his royal duties, starting with his marriage to Empress Michiko being the first between a royal and commoner in Japan. Together they strived to be closer to the people and have visited all 47 prefectures in Japan along with 18 countries abroad, including unprecedented trips to former WWII battlefields to give condolences and apologies on behalf of the Japanese people; the pair also traveled to the Tohoku region to visit those affected by the Great Eastern Japan Earthquake and Tsunami in 2011. Nevertheless, with his recent ailments and hospitalizations in recent years, the Emperor expressed his last bold move -- abdication.

### One Simply Doesn't Abdicate
Or at least not in the last 202 years since Emperor Kōkaku in 1817. Because of this, there was no provision for doing so in Japan's current constitution and a one-time law had to be drafted to allow for it. Usually the custom is to hurriedly usher in a new era as soon as an emperor passes on, but since so many legal proceedings, official documentation and even one's birthdate is tied to the reigning era, the proceeding months are usually quite hectic; remember also that there was no internet, smartphones, online records and bookkeeping and other tech marvels that could potentially be thrown into chaos as well. Needless to say while at first the idea was shunned, Once Japan Inc. weighed in, the idea of being able to plan the succession is being welcomed by everyone. 

### All Hail the Reiwa Era
Because of this preplanning, we even know Crown Prince Naruhito will herald the Reiwa (令和) era into existence on May 1, 2019. But what kind of changes will we see here in Japan during our long Golden Week coinciding with the ceremonies?

Will you be able to see it on TV? 

 - Expect wall-to-wall coverage of any festivities and ceremonies (if they can be televised at all) on NHK, Japan's public broadcast network. 

Will there be a countdown, ticker-tape confetti, or a parade? 

- As of this writing, nothing of the like will take place until October 22 which will be another national holiday and the actual Enthronement Ceremony. During these proceedings there will be a “shukuga onretsu no gi (祝賀御列の儀),” where the new Emperor and Empress will ride in a convertible car in a parade around the palace grounds and people can express their well-wishes.

What happens to the current Emperor?

- He will receive the title of Jōkō (上皇, Emperor Emeritus), an abbreviation of Daijō Tennō (太上天皇), upon abdicating. 