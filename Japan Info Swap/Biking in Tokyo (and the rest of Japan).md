# Biking in Tokyo (and the rest of Japan)

Just as sure as you'll find vending machines, convenience stores and a small shrine in the average Japanese neighborhood, you'll surely find people traveling around on bicycle. Seen more as a form of everyday transport than pleasure, most Japanese use their bikes to run errands around their immediate neighborhoods, ferry children to day care, and get to and from their local train stations in the suburban areas where pubic transport links are more spread out. 

Because they form a necessary part of the transportation infrastructure, cycling laws are enforced just as strictly as automobile laws; drunk cycling is punished with the same amount of fines and jail time, you may only park your cycle in designated areas, and in most areas, the bicycle must be registered with local police and carry a sticker with its registration number.  

### Getting Started

The easiest way to take care of everything at once is to buy a bike at a shop, and as you might guess, they are located everywhere from inside department stores all the way down to local mom & pop outlets. When you purchase a bike at the shop, the maintenance, registration and optional insurance can be taken care of straight away. If you decide to purchase a used bike (and there are lots of deals to be had this way) just remember to have the previous owner cancel their cycle registration with the local police so you may be able to reregister under your own name. Many an expat has had to spend time explaining to the cops why they are the rightful owner of a 10 year old mama-chari because they didn’t do this part.   

### Is insurance needed?
While not usually necessary, certain conditions may make it required. For example, some apartment buildings and parking areas where you’ll park long term may require a copy of the certificate. If you’ll be cycling to work, your employer may also make it a necessity. In any case, it is advisable to purchase cycling insurance if you will be a regular rider to protect against injury and property damage. Purchasing insurance is simple enough; pop into almost any convenience store and as the staff at the register for bicycle insurance. [PRICES?]

### Keeping Safe
The rules to cycling are pretty much the same as anywhere else; when riding in the street, keep left with slower traffic. On sidewalks, and wherever pedestrians are walking, no excessive speeding. By law you must have a bell and headlamp for night riding. Also no two-up riding is allowed (though you’ll see this sporadically.] You’ll likely notice the lack of helmeted riders in Japan because there is no law requiring their use. It’s still a good idea though if you will be riding longer distances in mixed traffic of course. Parking your bike can vary from place to place; in small neighborhoods, you can park on the side of the road or sidewalk, but in busier ones, you should seek out designated zones, parking areas, and even automated bike lockers. Definitely do not leave your bike parked in front of any train station... unless you like solving the mystery of what company took your bike and how to recover it! Speaking of parking, you’ll note that most Japanese do not chain their bikes to trees, light poles or other street fixtures; locking it down in place is good enough.

While Tokyo and other major cities are in the process of adding bike lanes and pathways to the streetscapes, in many places you’ll likely be riding in mixed traffic or on sidewalks. In any case, I’ve found using Google Maps to give me bicycle specific routes has been the best way to ensure I’m not on roads that are difficult to share with both cars or pedestrians. The routes given also tend to be more level and cut out the steep inclines making sure I arrive at my destination without looking like I biked the Tour de France. 

Towards a bikeable Tokyo
From the hardcore sport cyclist, down to the average Joe making a grocery run, Tokyo can be navigated on two human-powered wheels. Here are some resources and guides that may aid you in the quest to become totally savvy in this realm:


- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
*Images: [WORKNAME]() by [PERSON](), [CC~~]()*

![](Biking%20in%20Tokyo%20(and%20the%20rest%20of%20Japan)/53D6EC72-5DE8-40B7-856D-C20FDA798CCB.png)
![](Biking%20in%20Tokyo%20(and%20the%20rest%20of%20Japan)/BCE6DFCB-FE69-4AF1-84CB-F1037604B975.png)



![](Biking%20in%20Tokyo%20(and%20the%20rest%20of%20Japan)/840EA708-2695-40C1-B02F-710C864C07AC.png)

