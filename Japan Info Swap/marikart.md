# How to go-kart through the streets of Tokyo

If you've been in Tokyo's tourist areas for just a few hours, you might have caught a glimpse of a fleet of go-karts with costume-clad drivers speeding through the streets. Even if you haven't seen them up close, you may have heard of them or even caught them all over [YouTube and on TV](https://www.youtube.com/watch?v=hzn-85DCPSY). Marikar gives visitors and even residents a new way to speed through town behind the wheel, take in the sights, and have a very fun and unique experience at the same time. 

Countless reviews have been done about the them since their inception, and the vast majority of them are overwhelmingly positive. There was some press a few years ago talking about minor traffic accidents that involved the company's vehicles, a few natives calling the tourist's romp around town "disturbing and maybe dangerous," But really it's no more dangerous than riding an elecric bike or moped around town which is in fact [easier to rent!](https://morethanrelo.com/en/tackling-tokyo-on-two-wheels/) 

###Karting on city streets? Is this legal?
Yes it is completely legal. Go-karts under 50cc with turn signals, head and taillamps, mirrors and other safety equipment are classified as low-powered on-road vehicles by Japanese Motor Vehicle Code and sport a small blue license plate. You'll see the same one on some small delivery motortrikes and micro cars. It's entirely legal to drive a similarly equipped ATV on the street too.
### First, some requirements
Actually one big requirement: you need driving documents. An International Driver's Permit, or valid Japanese Driver's License will do. There are [some other types of documents/driving licenses that are OK](https://maricar.com/en/booking_shinagawa.html) but bottom line is there are _no exeptions to the rules._ **No documents, no karting.**

###So how do I do it?
- First, you need to pick what area you want to tour. Akihabara, Asakusa, Shibuya, Odaiba, etc... Each region is served by a different kart garage.
- Then you'll need to visit the website for that particular garage. They don't make it easy at all here as all the different garages are owned by different companies under a franchise license. I've gone ahead and done some of the hard work for you by listing the Tokyo area ones below.
 - [Shinagawa](https://maricar.com/en/shinagawa.html)
 - [Akihabara](https://maricar.com/en/akihabara.html)
 - [Shibuya](https://maricar.com/en/shibuya.html)
 - [Odaiba](https://kart.st/en/tokyobay.html)
 - [Yokohama](https://kart.st/en/southtokyobay.html)
-  Each mini-site will show the types of courses, options and other details about the areas they operate in. Make sure you have an idea about the dates, times, course and number of people in your own party.
-  To Check availability, book or just ask questions f sending them a message on either ther LINE or Facebook Messenger address on the website. It's much faster than their web form. You can also call them as well if that's easier for you. Remember, each location has a different SNS address so be careful!

And that's basically it. When you arrive at the garage, an attendant will check documents and then you'll pay; there'll be a short safety talk and then a walk-around the karts. Then you'll be off with a guide in a lead car, and another in a chase car to keep everyone together. I'm not sure this type of experience exists anywhere else in the world, so it's truly an "Only In Japan" story you can tell your friends if you decide to take the plunge!
