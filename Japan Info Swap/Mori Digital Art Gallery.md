# Mori Digital Art Gallery

When most people think of art and Japan in the same vein, usually images on the order of [Hokusai’s Great Wave of Kanagawa](https://en.wikipedia.org/wiki/The_Great_Wave_off_Kanagawa) spring to mind. You could tour the traditional art scene in Nippon seemingly forever since there’s a [multitude of museums throughout the metropolis](https://www.japanvisitor.com/guides/japan-museums). However, exactly one of them has a section catering to digital art, and that’s the Mori Building Digital Art Museum.

The new concept created by  digital art collective teamLab, opened next to the Pallette Town building in Odaiba this past January and features 470 projectors shining shimmering CGI graphics streaming from an entire data center’s worth of computers, onto reflective surfaces that patrons can interact with in many different ways. The Mori Building group has utilized teamLab’s services before if you’re feeling a bit of deja vu; a stroll around Roppongi Hills or Toranomon Hills should help jog your memory a bit.

The exhibits themselves are an interactive and ever-changing suite of light and sound; no two people will experience them exactly the same way and that’s a good thing. Whether its a virtual bouldering course that alters the light shower as you “climb” or an incline that allows you (and the kids... *especially* the kids) to power slide through a virtual pool of fruit and colors, this is a very hands-on affair.  As you move throughout the complex, each exhibit melds into the next and at times the interactions seemed to even be following me into the next area. This is not  by accident, as all the works are designed to integrate with one another and the patrons moving about the space. The main goal of the exhibit is to attempt to unify the digital and biological world and explore those new connections.
While us grownups contemplate the deep seated philosophical ideas behind every piece, children will literally be bouncing, running and crawling the walls whilst exploring every nook and cranny of the halls. There are even spaces where we are encouraged to sit down and drink a cup of tea where virtual flowers will bloom on top of it, all the while creating the perfect atmosphere to connect and share an experience with one another.
 
The proximity of the new museum to the majority of the 2020 Tokyo Olympic venues on Odaiba island isn’t mere coincidence either, however officials say this is a permanent exhibition and plans for new installations in the space after the Games are long gone are already underway.

### Getting there
MORI Building Digital Art Museum: EPSON teamLab Borderless [1-3-8 Aomi, Kōtō-ku, Tōkyō-to 135-0064](https://goo.gl/maps/qWA9oXFRUyu) 03-6406-3949 Tokyo Waterfront Railway Rinkai Line to Tokyo Teleport Station [R04] or Yurikamome Line to Aomi Station [U10] 
[http://borderless.teamlab.art](Http://borderless.teamlab.art)   

### Admission fees
Adult (aged 15 and over) ¥3,200 Child (ages between 4-14) ¥1,000 (Children below age 3 and under are free.) Purchase tickets and check availability by visiting [http://ticket.teamlab.art](http://ticket.teamlab.art).

![](Mori%20Digital%20Art%20Gallery/qrJHAPaDtUT_j4GxuxrI68ZbbreRlYbPAOV10sQlUIKwCjYkK0om1cTYV1XwTPROvCoELNcy=w608-no.jpeg)

![](Mori%20Digital%20Art%20Gallery/p.jpeg)![](Mori%20Digital%20Art%20Gallery/p%202.jpeg)![](Mori%20Digital%20Art%20Gallery/p%203.jpeg)
- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
Images: courtesy teamLab
Exhibition view of MORI Building DIGITAL ART MUSEUM: teamLab Borderless 2018, Odaiba, Tokyo
©️ teamLab 