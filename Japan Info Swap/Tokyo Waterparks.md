# Tokyo Waterparks
When the air outside is thick with humidity, cicadas  begin their endless chirping, the stores start stocking endless varieties of flavored bottled water and teas, and all the kids start sporting [“playground tans”](), Tokyo Summertime has finally come. We are the enviable environment to enjoy a trip to the beach, but sometimes we’d like to cool off a bit closer to town. Good thing there are several great waterparks around the metropolis we can take advantage of! 

Here’s our guide to some of the best waterparks to take a beat the heat and catch a few thrills at the same time:

**Tokyo Summerland**
![](Screen-Shot-2016-06-28-at-2.51.33-PM.png)
Located less than 2 hours away from Shinjuku in the city of Akiruno, this sprawling amusement park has rides like roller coasters and a Ferris wheel but is well known for its water park.
Both outdoor and indoor sections, outfitted with various water slides, fountains, and pools that can keep visitors entertained for the whole day. And when that isn’t enough, a hotel is also available.
**Water park schedule:**
Tokyo Summerland is open year round, except during the winter season where it is closed from December through February.
**Ticket Prices:**

**Normal Period**
**(March – June / October 2 – November)**
**Summer period**
**( July 1 – October 1 )**
|...|**Entrance Fee**|**Free Pass**|**Entrance Fee**|**Free Pass**|
|----|----|----|----|----|
|Adults (Ages 13-60)|¥2,000|¥3,000|¥3,500|¥4,500|
|Children (Ages 7-12)|¥1,000|¥2,000|¥2,500|¥3,000|
|Infants/Senior Citizens(Ages 2‐6 / 61 and over)|¥1,000|¥1,500|¥1,800|¥2,000|
※ *Regular Period: March to June | October to November* | *Summer Period: July 1 – September 30*
*※ Admission Ticket: This ticket includes Admission + use of Adventure Dome (Indoor Pool) +Adventure Lagoon (Outside Pool, opens during summer season only) + Water slides (excluding DEKASLA).*
*※ Free Pass Ticket: This ticket includes use of facility available with the Admission ticket + use of DEKASLA (opens during summer season only) + unlimited ride on rides (excluding some part of rides, coin-operated rides).*
**Access:**
Take the JR Chuo Line [JC] or Keio Line [KO] to Hachioji Station. Transfer to city bus services that offer direct access to the amusement park from there. 
[Map Link](https://www.summerland.co.jp/access/map.html)
600 Kamiyotsugi, Akiruno-shi, Tōkyō-to 197-0832
042-558-6511
[www.summerland.co.jp](http://www.summerland.co.jp/) 

**Yomiuri Land Water Amusement Island****
![](Screen-Shot-2016-06-28-at-6.18.33-PM.png)
Located about an hour away from central Tokyo is one of the larger and well-known amusement parks, Yomiuriland. The park offers over 25 types of attractions including modern thrill rides and water fumes as well as kid-friendly rides.
The park also opens its water park in the summer time. The water amusement island which is also know to locals as WAI offers several pools including an Anpanman-themed kids’ pool and various sliders featuring the Giant Sky River which stands at 24.5 meters and has a 386 meters downhill runway, navigable only with the use of a four-seater inflatable ring.
Special Events are also held throughout the summer including various shows, synchronize swimming performances, and summer water races.
**Water park schedule:**
July 01 to September 10, 2017 | 9:00 – 17:00.
**Ticket Prices:**

Pool admission
( Amusement park + Pool admission)
1-Day pass with pool
(Amusement park + Pool admission + unlimited rides)
Adults (ages 18 – 64)
¥3,200
¥6,000
Junior and senior high school students
¥2,500
¥4,800
Children (ages 3 – elementary school)
¥2,100
¥4,400
Silver (ages 65 & above)
¥2,000
¥5,300
**Access:**
■ Keio Line
 Keio-Yomiuri-Land Station. From there, cross the street and board the “Gondola Sky Shuttle” to get to the the park gate.
■ Odakyu Line
Yomiuri Land-Mae Station
Transfer to buses bound for Yomiuri Land.
Address: 4015-1 Yanokuchi, Inagi-shi, Tokyo 206-8725
Phone：044-966-1111
Website:  [www.yomiuriland.com](http://www.yomiuriland.com/english/) 

**Toshimaen Water Park****
![](Screen-Shot-2016-06-28-at-5.07.56-PM.png)
This park located in Nerima ward a includes  a vast assortment of carnival rides in addition to that water park having 26 water slides and 6 different pools.  
Open July 15 to September 4, 9:00 – 17:00

Admission
Admission +
Rides
Admission +
Kids Rides
Water Park
Adult (more than junior high school students)
¥1,000
¥4,200
¥2,900
¥4,000
Children A (height more than 110cm)
¥500
¥3,200

¥3,000
Children B (height less than 110cm)


¥2,400
¥2,000
**Access:**
■ Seibu Ikebukuro Line [SI]
From Ikebukuro Station, about 14 minutes to Toshimaen Station
About 1-minute walk from the station.
■ Toei Oedo Line [E]
From Shinjuku Station, about 20 minutes to Toshimaen Station.
About 1-minute walk from the station.
[Map link](https://maps.apple.com/?address=%E3%80%92176-0022,%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E7%B7%B4%E9%A6%AC%E5%8C%BA,%20%E5%90%91%E5%B1%B13%E4%B8%81%E7%9B%AE25%E7%95%AA1%E5%8F%B7&auid=13900339751083957811&ll=35.744893,139.645221&lsp=9902&q=Toshimaen&t=r)
Address: 3−25−1 Koyama, Nerima-ku, Tōkyō-to 176-8531
Phone：03-3990-8800 | 03-3990-4126 (Toshima Hotspring)
Website:  [www.toshimaen.co.jp](http://www.toshimaen.co.jp/index.html) 

**Tobu Super Pool**
![](Screen-Shot-2016-06-28-at-9.04.41-PM.png)
Located north of Tokyo in  Saitama prefecture, Tobu Dobutsuen-Koen consists of an amusement park, a zoo area, and leisure pool complex which is open during the summer season. The pool area has many slides, a lazy river, and a wave pool with giant fountains.

**Water park schedule:**
July 22 to September 9 | 9:30 – 17:00

**Ticket Prices:**

Admission Fee
Pool Admission
(Admission + Pool)
1-Day Pass
(Admission + Unlimited Rides + Pool)
Adults (Junior high & above)
¥1,700
¥2,400
¥4,800
Children (ages 3 & up)
¥700
¥1,100
¥3,700
Seniors (ages 60 & above)
¥1,000
¥1,700
¥3,700
Ride Ticket (1 ride)
¥300 – ¥1,000


**Access:**
■ From central Tokyo
Direct service is available via Tokyo Metro’s Hanzomon line [Z] and Hibiya line [H] that continue onto the Tobu Skytree line [TS]. Get off at Tobu-Dobutsukoen Station. From Tobu-Dobutsukoen Station, walk about 24 minutes (2 kilometers).
[Map link](https://maps.apple.com/?address=425,%20Tsumetagaya,%20Shiraoka,%20Saitama,%20Japan%20349-0222&ll=36.025563,139.704176&q=%E6%9D%B1%E6%AD%A6%E5%8B%95%E7%89%A9%E5%85%AC%E5%9C%92%E3%83%BB%E3%82%B9%E3%83%BC%E3%83%91%E3%83%BC%E3%83%97%E3%83%BC%E3%83%AB)
Address: 110 Suka, Miyashiro-machi, Minami Saitama-gun, Saitama
Phone：048-093-1200
Website:  [www.tobuzoo.com](http://www.tobuzoo.com/global/english/) 

**Showa Kinen Park: Rainbow Pool and Water Playland**![](Screen-Shot-2016-06-29-at-5.22.45-PM.png)
Once home to a prewar airfield, Kokuei Shōwa Kinen Kōen (国営昭和記念公園 )  is a massive park located in the western Tokyo suburb of Tachikawa and is considered as a paradise for families as it offers a wide range of activities to choose from – cycling, boating, playing sports, picnicking, flower and plant viewing, or just simply relaxing.
During the summer season, the park opens its Rainbow Pool and Water Playland. Nine different pools, water slides and wave machines are on offer here.
**Water park schedule:**
July 17 to September 4 | 9:30 – 18:00.
**Ticket Prices:**

Pool Rates
Group Rates
Sunset (entry after 14:00)
Pool Passport
Adult (ages 15 or older)
¥2,500
¥2,100
¥1,250
¥6,700
Children (elementary & junior high school)
¥1,400
¥1,100
¥700
¥3,400
Young children (ages 4 & older preschool)
¥500
¥400
¥250
¥900
Maternity (Pregnant Women)
¥500
*※ Maternity – please present Mother & Child Handbook*
*※ Silver – please present ID confirming age*
Silver (ages 65 or older)
¥700
*※ Rates includes park entrance.*
**Access:**
From Tokyo/Shinjuku Station, take the JR Chuo Rapid Line [JC] bound for Ome and alight at Nishi-tachikawa Station. 
[Google マップ](https://maps.apple.com/?address=3173,%20Midoricho,%20Tachikawa,%20Tokyo,%20Japan%20190-0014&auid=6983686569156599148&ll=35.706883,139.392140&lsp=9902&q=%E5%9B%BD%E5%96%B6%E6%98%AD%E5%92%8C%E8%A8%98%E5%BF%B5%E5%85%AC%E5%9C%92%20%E6%B0%B4%E9%81%8A%E3%81%B3%E5%BA%83%E5%A0%B4&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEKcBCgQIChAAEiQpeE7EXWnZQUAxiwr41vRsYUA59iPquY/aQUBB7d8+dk9tYUA%3D&t=r)
Address: 3173 Midori-cho Tachikawa City, Tokyo 〒190-0014
Phone：042-528-1751
Website:  [www.showakinen-koen.jp](http://www.showakinen-koen.jp/guide-english/schedule-english/) 

