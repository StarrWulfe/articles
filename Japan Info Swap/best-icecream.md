#Best Ice Cream shops in Tokyo

Summertime in the Metropoilis gets really hot and humid, bordering what you'd find in many subtropical locations. While it's easy to find relief in the form of sitting underneath the aircon at home, having a one-hour go on the Yamanote Line's new trains (whilst avoiding the "weak air conditioned car!), or any of the other escapes we've written about in years past, one simple and very tasty way to beat the heat is to have an ice cream. Of course you could pop into the nearest convenience store or Baskin-Robbins and grab an icy treat for cheap, but where's the fun in that? There's plenty of one-off ice cream shops that don’t involve Thirty-Something, the Dutch, or people from Vermont to explore.

### Mr. Friendly
Located on a backstreet in Daikanyama, this themed shop based on the children's character that has been teaching how to be a good friend to kids since the 1960s specializes in many sweet treats like tea cakes, dessert hot cakes, crepes and the like, but many also visit for their take on ice cream parfait. Their take on the layered sweet can be had in either chocolate and banana or strawberry and caramel custard forms. There's also a smaller version with just soft serve ice cream, the topping of your choice, all layered over a bed of corn flakes. 

[Ebisu-nishi 2−18-6, Shibuya, Tokyo](https://maps.apple.com/?address=%E3%80%92150-0021,%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%8B%E8%B0%B7%E5%8C%BA,%20%E6%81%B5%E6%AF%94%E5%AF%BF%E8%A5%BF2%E4%B8%81%E7%9B%AE18%E7%95%AA6%E5%8F%B7,%20SP%E3%83%93%E3%83%AB1%EF%BC%A6&auid=17403626523716370974&ll=35.649678,139.705197&lsp=9902&q=Mr.%20Friendly&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEAgKBAgKEAASJikz9PSnlNJBQDGtIpHLYnZhQDmxyRoEu9NBQEHp0VVbvXZhQFAE&t=r)
[Daikanyama Station [TY02]](https://maps.apple.com/?address=Daikan-yamacho,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0034&auid=6202459392780877717&ll=35.648315,139.703516&lsp=9902&q=Daikan-yama%20Station&_ext=CiUKBAgFEAMKBQgGEOEBCgQIChAPCgQIJRBkCgQIKhABCgQIOhABEiQpEh/xi97SQUAxVVBwJXp2YUA5XuMraxnTQUBBf10dQox2YUA%3D&t=r)
(03)3780-0986
Open Everyday, 11am~8pm
[www.mrfriendly.jp](http://www.mrfriendly.jp/index.html)

### Gomaya Kuki
Sesame or "goma" in Japanese, is a seasoning that comes up frequently in Japanese cooking, from the oils used to fry tempura, to salad dressings and toppings on various dishes. However one place I've never considered until coming to Japan back in the day was in desserts, especially ice cream. In Gomaya Kuki, they take the idea seriously enough to only serve this concoction in only two flavors: White Goma with hints of vanilla and Black Goma with hints of chocolate. In addition there are levels of goma flavorings and you can get sesame sprinkles on your cup or cone to enhance the taste. 

[Jingu-mae 4-26-22](https://maps.apple.com/?address=26-22,%20Jingumae%204-Ch%C5%8Dme,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0001&auid=13114460717614429283&ll=35.668497,139.706983&lsp=9902&q=GOMAYA%20KUKI&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEJ0CCgQIChAAEiYpnVPaHf7UQUAxlfDFT3J2YUA5GykAeiTWQUBB00QD5cx2YUBQBA%3D%3D&t=r)
[Meiji Jingu-mae Station [F13][C03]](https://maps.apple.com/?address=Jingumae,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0001&auid=1200541228264157053&ll=35.668367,139.705298&lsp=9902&q=Meiji-jingumae%20'Harajuku'%20Station&_ext=CiYKBAgFEAMKBQgGEOEBCgQIChACCgUIJRCQAwoECCoQAwoECDoQARIkKXo5qp9v1UFAMRpbPb+IdmFAOcb95H6q1UFAQaR/FN2admFA&t=r), [Harajuku Station [JY19]](https://maps.apple.com/?address=Jingumae,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0001&auid=8311635058217769247&ll=35.671365,139.702727&lsp=9902&q=Harajuku%20Station&_ext=CiUKBAgFEAMKBQgGEOEBCgQIChANCgQIJRBkCgQIKhABCgQIOhABEiQpGPOM3dHVQUAxiXKyrXN2YUA5ZLfHvAzWQUBBIS+2y4V2YUA%3D&t=r)
(080)7961-8516
Open Everyday, 11am~7pm
[gomayakuki.jp](http://gomayakuki.jp)

### Daily Chiko
I still don't know what the shop name means after 13 or so years of coming here almost every summer and then some, but whenever I find myself in the Nakano Broadway vicinity and craving ice cream, I end up in the basement ordering whatever the #7 flavor is according to the lineup in front of the stand. Sometimes it's matcha, sometimes its orange sherbert, and once it was plane-jane vanilla. Usually I get 2 or 3 layers and you can mix flavors, but they also have a very tall 8 layer cone for ¥600 that you can try to get through as well. This is also a greay way to start or end your exploration of Nakano as well since there's literally days worth of treasures to be found around here.

[Nakano 5-52-15, Nakano Broadway Bldg B1](https://maps.apple.com/?address=52-15,%20Nakano%205-Ch%C5%8Dme,%20Nakano-Ku,%20Tokyo,%20Japan%20164-0001&auid=13319791453868047654&ll=35.709224,139.665657&lsp=9902&q=Daily%20Chiko&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEA4KBAgKEAASJiko2gusNNpBQDHlEL2+H3VhQDmmrzEIW9tBQEFbJtJfenVhQFAE&t=r)
[Nakano Station [JB07][JC06][T01]](https://maps.apple.com/?address=Nakano,%20Nakano-Ku,%20Tokyo,%20Japan%20164-0001&auid=16637557223840411235&ll=35.705783,139.665741&lsp=9902&q=Nakano%20Station&_ext=CiUKBAgFEAMKBQgGEOEBCgQIChANCgQIJRBkCgQIKhABCgQIOhABEiQpVKb7qjnaQUAx6SB7sER1YUA5oGo2inTaQUBBe0V/0FZ1YUA%3D&t=r)
(03)3386-4461
Open Everyday, 10am~8pm
[Facebook.com/DailyChiko](https://www.facebook.com/Dailychiko)

Obviously this list is not exhaustive, and just represents my favorite spots for a cool treat around town. If you've stumbled across a place that should be included here, drop us a shout in the comments!