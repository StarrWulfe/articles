# Where to see English stand-up comedy around Tokyo

Turn on the TV at any given moment during “Golden Hour” (what we Yanks call Primetime) and you’ll no doubt see comedy duos interspersed in the endless waves of food tasting, international shenanigans, and celebrity anecdotes that seem to permeate the average Japanese media diet. Those comedy teams, called *manzai* form the backbone of Japanese comedic performance and goes back hundreds of years. If you can understand Japanese language and culture to the point of “getting” the average manzai routine, I can’t recommend seeing a live performance enough. But remember, comedy always depends not only on the performers but the audience to quickly sort out the wordplay, nuanced situations, cultural references and so-forth. Even with almost 20 years in, I have a fair amount of trouble understanding Japanese comedy and almost none of that has to do with the language barrier— I simply wasn’t raised in Japan and don’t get some of the context.

### Everyone needs a laugh
Luckily there’s a critical mass of other foreigners and Japanese doing stand-up in English around Greater Tokyo (with more talent than myself) that don’t mind taking to the stage with nothing more than a microphone, some funny stories, and some time to tell them. 

#### Good Heavens
Located in the bohemian-trendy Shimo-Kitazawa district, this British pub has been hosting possibly the only weekly English language stand-up sets for ages now. So long in fact that you might even see some famous comics from your home country dropping in to do a set while they’re touring Japan just for the hell of it. 
##### Details:
**_Date & Time:_** Most Wednesdays starting around 7pm or so. 
**_Place:_** [32-5 Daizawa, Setagaya, Tokyo](https://maps.apple.com/?address=32-5,%20Daizawa%205-Ch%C5%8Dme,%20Setagaya-Ku,%20Tokyo,%20Japan%20155-0032&auid=10144906870755013178&ll=35.658914,139.667643&lsp=9902&q=Good%20Heaven&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEKECCgQIChAAEiYpMsSxH8TTQUAxrDgLDDB1YUA5sJnXe+rUQUBBuCCAnop1YUBQBA%3D%3D&t=r "32-5 Daizawa, Setagaya, Tokyo")
**_Cost:_** ¥1500, includes drink.
#### Antenna America
Probably the best place in all of Kanto to enjoy some craft beer and authentic  Buffalo wings, this rooftop cafe in a nondescript building in the middle of downtown Yokohama brings stand-up to the Bayside City twice a month in the form of both English and Japanese language showcases. 
#### Details:
**_Date & Time:_** Usually twice a month Thursdays; one week is English, the next is Japanese. 
**_Place:_** [5-4 Yoshidamachi, Naka, Yokohama, Kanagawa](https://maps.apple.com/?address=5-4,%20Yoshidamachi,%20Naka-Ku,%20Yokohama-Shi,%20Kanagawa,%20Japan%20231-0041&auid=3285329717193071069&ll=35.446017,139.632216&lsp=9902&q=Antenna%20America%20Kannai&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEGgKBAgKEAASJin60AXmg7hBQDE2BzLzDXRhQDl4pitCqrlBQEGsGClIaHRhQFAE&t=r)
**_Cost:_** ¥1500, includes drink.

#### 