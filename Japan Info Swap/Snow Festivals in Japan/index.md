# Snow Festivals in Japan
Because Japan is over 70% mountainous and located in the temperate latitudes of the western Pacific where the moist ocean air constantly does battle with dry, cold Siberian and Mongolian air coming in from China, the Japan Alps enjoys a healthy dusting of snow every winter. No part of Japan (even Okinawa) is less than a few hours car, plane or train ride from enjoying a winter wonderland. For some that means skiing or boarding the slopes, but for others it means turning those frozen crystals into works of art in the form of snow and ice sculptures. Taking place in more places than you may have thought, you can usually find one in many places in the snowy parts of Japan.

### Sapporo Snow Festival
![via Sapporo Snow Festival Official Website - https://www.snowfes.com/](Sapporo%20snow%20fest.jpg)
Let’s start with the 900 pound gorilla that perhaps is the most famous snow festival in the world if not just Japan. The first week in February has the whole downtown section of Sapporo in the northernmost island of Hokkaido turned into a frosty fantasyland with ice sculptures and even small replicas of famous buildings standing 15 meters tall or more. Most sculptures will be centered on Odori Park in the middle of town and open 24 hours for the whole week, but check out the Tsudome area where there are mini snow raft rides and Susukino to see the ice sculpture contest. 
**Web:** [https://www.snowfes.com/english/](https://www.snowfes.com/english/)
**Dates:** February 4-11, 2020
**Place:** [Odori Park, Sapporo, Hokkaido](https://maps.apple.com/?address=Odorinishi%20%E5%A4%A7%E9%80%9A,%20Chuo-Ku,%20Sapporo,%20Hokkaido,%20Japan%20060-0042&auid=16443075569851517039&ll=43.059819,141.347228&lsp=9902&q=Odori%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJClAYQNMIHxFQDFa4hg2K6dhQDk5uVaoK5NFQEGmHWfEDa9hQA%3D%3D)

### Aomori Yukiakari Matsuri
![via Aomori City Tourism Office - https://www.city.aomori.aomori.jp/](yukiakari%20aomori.jpg)  
This festival is held in the northern prefecture of Aomori where the residents of the city create displays in the snow made with candle light. You can also participate by decorating your own candle holder and add to the wishes of the townspeople.
**Web:** [Aomori City Events Site](https://www.city.aomori.aomori.jp/kanko/bunka-sports-kanko/kankou/shiki-event/fuyu/01/01.html)
**Dates:** January 31 - February 2, 2020
**Place:** [Nebuta House Museum](https://maps.apple.com/?address=1-1,%20Yasukata%201-Ch%C5%8Dme,%20Aomori-Shi,%20Aomori,%20Japan%20030-0803&auid=1975185257317027369&ll=40.829707,140.736157&lsp=9902&q=Nebuta%20Museum%20Wa%20Rasse&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEN8BCgQIChAAEiYpPnfdp6BpREAx3NA/+F2XYUA5vEwDBMdqREBBUB0AOr+XYUBQBA%3D%3D)

### Urabandai Snow Festival “Night Fantasy”
![via Urabandai Tourism Office - https://www.urabandai-inf.com](Urabandai%20snow%20fest.jpg)
Coming back a little bit closer in Tohoku is the town of Urabandai in Fukushima where they like to build igloos, light candles in the snow and have a fireworks festival under the clear cold starlit night! 
**Web:** [Urabandai Tourism Official Website](https://www.urabandai-inf.com/en/?event=the-2020-snow-festival-night-fantasy-will-be-held-on-february-15th-we-look-forward-to-your-visit)
**Dates:**  February 15, 2020, 10:00 - 20:00
**Place:**  [Urabandai Site Station](https://maps.apple.com/?address=1092-65,%20Hibara%20Onogawahara,%20Kitashiobara-Mura,%20Yama-Gun,%20Fukushima,%20Japan%20969-2701&auid=8318564259085671036&ll=37.677728,140.071362&lsp=9902&q=Urabandai%20Site%20Station&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEH4KBAgKEAASJinHYMWaLNZCQDF0aYgbGoJhQDlFNuv2UtdCQEHioWMWd4JhQFAE)  

### Tokamachi Snow Festival
![via Tokamachi Snow Fest website - http://www.snowfes.jp/](tokkamachi%20snow%20fest.jpg)
Last on our list but certainly not least is this snow fest in Niigata Prefecture, known for getting a bunch of the fluffy cold stuff every year. Known for being one of the best places for skiing in Japan, Tokamachi’s residents like to get into the spirit and sculpt characters out of ice and snow all over the town while the town gets into a carnival atmosphere with a kimono fashion show, concerts and culminating with fireworks. Even the stage itself is one big snow sculpture!   
**Web:** [http://www.snowfes.jp/](http://www.snowfes.jp/)
**Dates:** February 14 - 16, 2020
**Place:**  [ City of Tokamachi, Niigata](https://maps.apple.com/?address=%E3%80%92948-0079,%20%E6%96%B0%E6%BD%9F%E7%9C%8C%E5%8D%81%E6%97%A5%E7%94%BA%E5%B8%82,%20%E6%97%AD%E7%94%BA251-17%20%E5%8D%81%E6%97%A5%E7%94%BA%E5%B8%82%E7%B7%8F%E5%90%88%E8%A6%B3%E5%85%89%E6%A1%88%E5%86%85%E6%89%80%E5%86%85&auid=1932888441629971300&ll=37.134754,138.756557&lsp=9902&q=Tokamachi%20Snow%20Festival&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEN8BCgQIChAAEiYpZ7EUuq2QQkAxXQGSDQhYYUA55YY6FtSRQkBBrQSPXGRYYUBQBA%3D%3D)

Images via [Sapporo Snow Festival Official Website](https://www.snowfes.com/),  [Aomori City Tourism Office](https://www.city.aomori.aomori.jp/), [Urabandai Tourism Office](https://www.urabandai-inf.com), [Tokamachi Snow Fest website](http://www.snowfes.jp/)