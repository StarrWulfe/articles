# Romancing the rails at Odakyu's Romancecar Museum

It's an open secret that one of the reasons I enjoy living in Japan so much is the fact that public transportation is held in a high regard and promoted by the people as a matter of social, security, and cultural importance. Most people think of the Shinkansen "bullet train" network of high-speed rail routes, however it only serves as the backbone supporting a much larger system and there are an impressive amount of conventional long-distance, commuter and local lines. In the Greater Tokyo metropolis, they are virtually indistinguishable from most cities metros and subways. In fact most of our subway lines are through-routed onto commuter rail in the suburbs and it is a very real possibility to fall asleep on the subway in Shibuya and wake up out in the middle of the mountains, or a rice field! (Be careful!) One of the major operators of service here is Odakyu (Odawara Electric Railway), which starting from its main terminal in the Shinjuku Rail Station complex, operates service to Odawara and the Fujisawa/Enoshima areas in Kanagawa Prefecture, along with another branch to Karakida in the western Tokyo suburbs. There's a connection at Yoyogi-Uehara station to Tokyo Metro's Chiyoda Line and some trains have direct service to/from the subway and out the other end onto JR's Joban Line all the way into Chiba Prefecture on the other side of Tokyo. (Seriously, be careful not to sleep on the train!) There's a high degree of cooperation and coordination between train companies here, and its a real treat to see how each one has a slightly different approach to what essentially adds up to getting Tokyoites from point A to B each day. Odakyu being one of the largest and profitable transport companies in Tokyo, has a very unique history and story to tell and for that reason, they decided to create a museum to show it off; But wait, why is it called "Romancecar Museum?"


### The 'Lovebird Express'...?

The term *Romancecar* comes from Odakyu's service mark for its limited express lines and the specific trains that serve these routes. One can book a train from Shinjuku, all the way to Hakone, a popular resort destination that is the site of many ski resorts in the wintertime, the gateway to both Fuju-Q Highlands amusement park (known for having some of the wildest roller coasters in the world) and Mt Fuji and the Five Lakes nature reserve area during climbing season. Another route will take you to the Enoshima part of the Shonan beach zone.
Operating since 1957, the Romancecar's bright red livery, panoramic windows and the "loveseat" style 2x2 seating arrangement onboard has probably been a part of many a couple's getaway date, and hence the name. And these trains are known for one other thing: You may be lucky enough to score seats in the forward-most seats where you can have a driver's view of unfolding scenery ahead, since the driver is seated *on top* of the train here.
Just about everyone in Japan knows about these trains, and Odakyu wasn't wrong in making this the starting point of their historical storytelling in their new museum.


### The Museum

Located right next to Ebina station in a portion of Odakyu's Ebina Yards, the museum itself is a multimedia packed 21st century homage to rail transport. You will definitely find examples of historical rail cars, including every vintage permutation of the Romancecar, and since the actual museum is connected with tracks to the railway system outside, a rotating selection of antique rail cars are scheduled to be shown as well.
In addition, there's a historical films viewing area, where various milestones in Odakyu's history is shown, a very large diorama and model railroad setup, where the entire route of Odakyu's service area has been modeled, and a rooftop viewing area (remember, the museum is right between a working rail yard and one of their busiest train stations; the perfect rail fanning spot for pictures!)
Of course you'll also find the requisite museum shop (*Trains*) and on-site restaurant for snacks while train spotting (*The Clubhouse*). Best of all, the whole things is <span class="underline">FREE</span> to enter!!


### Getting There

⚠️ *Due to the ongoing COVID-19 situation, you must go online and <span class="underline">make a reservation first!</span>* 

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left"><b><a href="https://www.odakyu.jp/romancecarmuseum">Odakyu Romancecar Museum</a></b></th>
<th scope="col" class="org-left">&#xa0;</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">🧭 <b>Address:</b></td>
<td class="org-left"><a href="https://maps.apple.com/?address=%E3%80%92243-0438,%20%E7%A5%9E%E5%A5%88%E5%B7%9D%E7%9C%8C%E6%B5%B7%E8%80%81%E5%90%8D%E5%B8%82,%20%E3%82%81%E3%81%90%E3%81%BF%E7%94%BA1-3&amp;auid=16924231609302380188&amp;ll=35.453064,139.390280&amp;lsp=9902&amp;q=%E3%83%AD%E3%83%9E%E3%83%B3%E3%82%B9%E3%82%AB%E3%83%BC%E3%83%9F%E3%83%A5%E3%83%BC%E3%82%B8%E3%82%A2%E3%83%A0&amp;_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBAKBAgKEAASJil4oW+yXrlBQDERuJY+VWxhQDn2dpUOhbpBQEEd/3iVr2xhQFAE&amp;t=r%20">Megumi-machi 1-3, Ebina, Kanagawa</a></td>
</tr>


<tr>
<td class="org-left">🕙 <b>Hours:</b></td>
<td class="org-left">Everyday, 10am~6pm</td>
</tr>


<tr>
<td class="org-left">🎟 <b>Fees:</b></td>
<td class="org-left">Free</td>
</tr>


<tr>
<td class="org-left">🚉 <b>Access:</b></td>
<td class="org-left"><a href="https://maps.apple.com/?address=Megumicho,%20Ebina,%20Kanagawa,%20Japan&amp;auid=17204332276253265779&amp;ll=35.452773,139.390954&amp;lsp=9902&amp;q=Ebina%20Station&amp;_ext=CiUKBAgFEAMKBQgGEOEBCgQIChAOCgQIJRBkCgQIKhADCgQIOhABEiQpyJ/BBte5QUAxxrhFqXlsYUA5FGT85RG6QUBBnFanuotsYUA%3D&amp;t=r">Ebina Station OH-30</a></td>
</tr>


<tr>
<td class="org-left">📞 <b>Phone</b></td>
<td class="org-left">+81 462-33-0909</td>
</tr>


<tr>
<td class="org-left">💻 <b>Web:</b></td>
<td class="org-left"><a href="http://odakyu.jp/romancecarmuseum">http://odakyu.jp/romancecarmuseum</a></td>
</tr>
</tbody>
</table>

📸 *images via [Odakyu Romancecar website](http://odakyu.jp/romancecarmuseum) and [Jason L Gatewood](http://jlgatewood.com)*

