#  Tokyo Dessert Buffets

If you’re the type that is all about putting cake before casserole and satisfying your sweet tooth to a point that would make your dentist’s hair stand on end, then you’ll want to read on further as we present the best places in Tokyo to check out All-you-can-eat sweets, treats and other sugar infused eats.

### Cafe Ron Ron
Japan is the country that invented the conveyor belt sushi restaurant, so it’s completely plausible that a Tokyo enterprising restauranteur would use the same system to cart out confections to waiting guests. Cafe Ron Ron operates their “sweets-go-round” everyday without fail. Everything from crepes, shortcakes, and tarts to ice cream and seasonal treats float by on the belt and all you have to do is pick and eat whatever you like within 40 minutes. You’ll also get a free drink bottle (also filling with your favorite drink) to take home to commemorate your visit.  
[https://cafe-ronron.com](https://cafe-ronron.com)
Open Everyday, 11am - 6pm
[Jingumae 6-7-15, Shibuya, Tokyo](https://maps.apple.com/?address=7-15,%20Jingumae%206-Ch%C5%8Dme,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0001&ll=35.667244,139.705788&q=7-15,%20Jingumae%206-Ch%C5%8Dme&_ext=EiYpj49xEdXUQUAxL2dwhWh2YUA5DWWXbfvVQUBB2ZNQGsN2YUBQBA%3D%3D)
[Meiji Jingumae Station (C)(F)](https://maps.apple.com/?address=Jingumae,%20Shibuya-Ku,%20Tokyo,%20Japan%20150-0001&auid=16452232532047475413&ll=35.668367,139.705298&lsp=9902&q=Meiji-jingumae%20'Harajuku'%20Station&_ext=ChkKBAgEEAoKBAgFEAMKBQgGELwBCgQIChAAEiQpejmqn2/VQUAxGls9v4h2YUA5xv3kfqrVQUBBpH8U3Zp2YUA%3D), [Harajuku Station (JY)](https://maps.apple.com/?address=Jingumae,%20Shibuya-Ku,%20Tokyo,%20Japan&auid=10804100741301665850&ll=35.671365,139.702727&lsp=9902&q=Harajuku%20Station&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBgKBAgKEAASJCkY84zd0dVBQDGJcrKtc3ZhQDlkt8e8DNZBQEEhL7bLhXZhQA%3D%3D)
03-5468-8290

### Marble Lounge
Located in the Hilton Hotel’s massive west Shinjuku complex, this buffet regularly collects accolades on its regular food, including its breakfast and brunch selections. But the dessert section deserves its own mention due to its themes that change throughout the year. Right now for example you can enjoy their "Alice's Christmas Tea Party" complete with a miniature Alice hiding somewhere amongst the cakes and pastries on the bar. 
[Marble Lounge @ Hilton Tokyo](https://www3.hilton.com/en/hotels/japan/hilton-tokyo-TYOHITW/dining/marble-lounge.html "Marble Lounge Website")
Dessert bar available daily 2:30 p.m. - 5:30 p.m.; many of the dessert selections continue through the dinner time until 9pm. Reservations are taken through their website.
[Nishi-Shinjuku 6-6-2, Shinjuku, Tokyo](https://maps.apple.com/?address=6-2,%20Nishishinjuku%206-Ch%C5%8Dme,%20Shinjuku,%20Tokyo,%20Japan%20160-0023&auid=2594306682870434161&ll=35.692830,139.691237&lsp=9902&q=Marble%20Lounge&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBQKBAgKEAASJikBqQMAGNhBQDGc574b8XVhQDl/filcPtlBQEG2/Qa4S3ZhQFAE)
[Nishi Shinjuku Station (M)](https://maps.apple.com/?address=Nishishinjuku,%20Shinjuku-Ku,%20Tokyo,%20Japan%20160-0023&auid=2525564407440181813&ll=35.694344,139.692895&lsp=9902&q=Nishi-shinjuku%20Station&_ext=ChkKBAgEEAoKBAgFEAMKBQgGELwBCgQIChAAEiQpRDOy1cLYQUAxsrJ1IiN2YUA5kPfstP3YQUBBRHHPQTV2YUA%3D), [Tocho-mae Station (E)](https://maps.apple.com/?address=Nishishinjuku,%20Shinjuku-Ku,%20Tokyo,%20Japan%20160-0023&auid=3909977795419983436&ll=35.690591,139.692771&lsp=9902&q=Tochomae%20Station&_ext=ChkKBAgEEAoKBAgFEAMKBQgGELwBCgQIChAAEiQp1lMO10fYQUAxqqaFHiJ2YUA5IhhJtoLYQUBB1oCnPTR2YUA%3D)
03-3344-5111

### Sweets Paradise
Despite the name, you can find other dishes like pasta and salads at this buffet, but they are clearly designed to whet your palate for their main course: a 30+ item desert bar that changes throughout the year. Plus they have locations all over Greater Tokyo, and throughout Japan.
[Check their website for ￼￼￼￼current locations.](https://www.sweets-paradise.jp/shop/) 

---- 
Images: via [Cafe Ron Ron Instagram](https://instagram.com/caferonron?igshid=7y2awlzrof6g), [Sweets Paradise Instagram](https://instagram.com/sweetsparadise?igshid=oi9zv6tettf5)