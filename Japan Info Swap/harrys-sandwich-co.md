# Harrys Deli 🥪

Article Media: Harrys%20Deli%20%F0%9F%A5%AA%209c70cf918f284434accc7f55d6131f55/4CE7AC34-F575-4041-AA82-EF1C2851700F.jpeg, Harrys%20Deli%20%F0%9F%A5%AA%209c70cf918f284434accc7f55d6131f55/F8F55E96-2C19-46CE-BC34-FF0DF844CF54.jpeg, Harrys%20Deli%20%F0%9F%A5%AA%209c70cf918f284434accc7f55d6131f55/C3E5D700-06D3-468F-8110-9FFC7309941A.jpeg
Last Edited: Sep 22, 2020 9:48 PM
Status: In Progress
URL 1: https://www.yelp.com/biz/harrys-sandwich-co-渋谷区

# Harry's Sandwich Shop is the stuff dreams are made of

There are very few foods more simple and yet more complex than a sandwich. Literally whatever you can throw between two slices of bread, the varieties are infinite. Hard to really get wrong, but even more difficult to get 100% right, there are very few places to get a serious *back home* kinda grinder over here in Japan, but that was before a guy named Harry turned up to change all that.

### From the back of the kitchen to the back streets of Tokyo

The story of Harry’s Sandwich Company starts with Harry himself. Hailing from the San Francisco Bay Area, he came to Japan about 7 years ago, he became known for being “that guy that makes really good sandwiches” at every gathering/house party/infrequent get-togethers, due no less in part to having a restaurant chef father and spending an inordinate time around the kitchen. At some point, someone said “you ought to sell these, they’re way better than conbini food.” Hearing that along with wanting to exit the English teaching industry, he partnered with some other friends who were doing events all over Tokyo and began operating a pop-up sandwich stand. He perfected his skills and decided to see about getting a food truck. However as luck would have it, another friend of a friend who happened to own a spot in a 3 floor walk up in Harajuku let him know there was an immediate vacancy and thought his sandwiches would be a great addition to the Takeshita Alley landscape. Harry’s Sandwich Company At Cafe Roots was born.

### Chill Spot

The shop itself is a little hard to get to thanks to the typical Tokyo backstreet labyrinthine landscape; you’d better use your smartphone map app to find the building. Once you’re there though, the orange and black signs can take over and guide you off the street and up the narrow and cramped stairwell (seriously, mind your head if you’re tall!) The restaurant space is small and intimate and in the current times, certain areas and seating configurations are marked off due to social distancing. You’re welcome to call and confirm the conditions before making the trek to be sure you can be seated, since Harry is firm in keeping the place safe for his patrons. Warm and cozy boxes are ticked, free wifi, and pretty well known amongst the long-term expat crowd here, Harry's could easily be a nice spot to drop in on. Take out is also ok if you wanna bring the goods back home with you instead.

### Sammich heaven

The menu is a sandwich lover’s dream filled with all the meaty, cheesy, zesty goodness imaginable.  On my most recent visit with a friend, we ordered up both the Classic Italian — a collection of three meats and cheeses splashed with the dressing of the same name, and the Chopped Cheese — a take on the chopped meatball and cheese served in many a New York City bodega. Both came with potato chips but there's straight and curly fries on the menu too. For refreshment, try the homemade ginger ale for a perky pick up; or if you're not going back to the office, turn to the extensive cocktail list and let your mind wander; Harry can mix drinks just as good as he mixes bread and meats. By the way, if you're living the meatless life and think there's nothing here for you, think again; Harry's got you covered with plenty of vegetarian variations dotting the menu including a spin on the grilled cheese that will take you back to your childhood and simpler times indeed.

**[Harry's Sandwich Company](https://www.facebook.com/harryssandwichco/)**

**Address:** [1−16−7 Jingumae, Shibuya, Tokyo](https://maps.google.com/maps?ll=35.671031,139.702418&z=17&t=m&hl=en-US&gl=US&mapclient=embed&daddr=Harry%27s%20Sandwich%20Company%20MS%E3%83%93%E3%83%AB%203F%201%20Chome-16-7%20Jingumae%20Shibuya%20City%2C%20Tokyo%20150-0001@35.6710268,139.7046124) 

**Transport:** [JR Harajuku Station](https://maps.apple.com/?address=Jingumae,%20Shibuya-Ku,%20Tokyo,%20Japan&auid=8311635058217769247&ll=35.671365,139.702727&lsp=9902&q=Harajuku%20Station&_ext=CiUKBAgFEAMKBQgGEOEBCgQIChANCgQIJRBkCgQIKhABCgQIOhABEiQp2vdNBuzVQUAxfDn96nt2YUA5orIGlPLVQUBBLmhrjn12YUA%3D) (JY); [Tokyo Metro Meiji Jingu Mae Station](https://maps.apple.com/?address=Jingumae,%20Shibuya-Ku,%20Tokyo,%20Japan&auid=17787635158638419286&ll=35.668367,139.705298&lsp=9902&q=Meiji-jingumae%20Station&_ext=CiYKBAgFEAMKBQgGEOEBCgQIChACCgUIJRCQAwoECCoQAwoECDoQARIkKTw+a8iJ1UFAMQbWcfyQdmFAOQT5I1aQ1UFAQbgE4J+SdmFA) (C)(F)

**Hours:** Wednesday - Sunday, 12PM~10PM (11PM Fri & Sat)

*Images by [Jason L Gatewood](http://jlgatewood.com)*

[Directions](https://maps.google.com/maps?ll=35.671031,139.702418&z=17&t=m&hl=en-US&gl=US&mapclient=embed&daddr=Harry%27s%20Sandwich%20Company%20MS%E3%83%93%E3%83%AB%203F%201%20Chome-16-7%20Jingumae%20Shibuya%20City%2C%20Tokyo%20150-0001@35.6710268,139.7046124)

[https://www.google.com/maps/place/Harry's+Sandwich+Company/@35.6710311,139.7024184,17z/data=!3m2!4b1!5s0x60188cbb300f1a69:0xff3e8076fb9c8561!4m5!3m4!1s0x60188cbb3050dcdf:0x1eb2089b7f5722bd!8m2!3d35.6710268!4d139.7046124](https://www.google.com/maps/place/Harry's+Sandwich+Company/@35.6710311,139.7024184,17z/data=!3m2!4b1!5s0x60188cbb300f1a69:0xff3e8076fb9c8561!4m5!3m4!1s0x60188cbb3050dcdf:0x1eb2089b7f5722bd!8m2!3d35.6710268!4d139.7046124)

- Started as the "sandwich making dude" in his group of friends.
- Decided to quit English teaching and do pop-up events part-time to test his idea.
- 2 years ago took over a bar in Harajuku after some other folks abandoned their business there.
- Menu is filled with classics along with new creations
    - Chopped Cheese — NYC Bodega inspired
    - Italian — Classic mix of meats and dressing
    - Vegan sandwiches are on offer
    - Extensive cocktail list
    - Homemade Ginger Ale!
- Don't forget