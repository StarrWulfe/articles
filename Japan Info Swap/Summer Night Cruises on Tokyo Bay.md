# Summer Night Cruises on Tokyo Bay 
What's better than attending one of [Japan's summer festivals](https://morethanrelo.com/en/hanabi-fireworks-festivals-in-tokyo/)? Cruising Tokyo Bay while sipping cocktails in your [summer yukata](https://morethanrelo.com/en/buying-yukata-tokyo/)!
### So what exactly is it like to cruise Tokyo Bay anyway?
We [talked about ](https://morethanrelo.com/en/tokyos-romantic-valentine-spots/) how cruising Tokyo Bay aboard some of these ships makes for a romantic setting on Valentine's Day... Or any day with the one you love for that matter. But we didn't really cover why this is a cool thing to cross off your bucket list. The reason is deceptively simple: The view of Toyko from aboard a ship is one that can't be seen anywhere else in most cases, and is very rare! Plus it's a good way to beat the heat of summer. 
###What choices are there?
Turns out there are plenty of companies and different kinds of amenities aboard that will of course mean different price points, but the good news here is that there is something for everyone that will fit your budget and schedule.
###All Aboard!
Most ships leave from [Hinode Piers](https://maps.apple.com/?address=16-3,%20Kaigan%201-Ch%C5%8Dme,%20Minato-Ku,%20Tokyo,%20Japan%20105-0022&auid=1347325819187682191&ll=35.652347,139.762152&lsp=9902&q=Tokai%20Kisen&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEN8BCgQIChAAEiYpOycH7uzSQUAx7yZ6QzZ4YUA5ufwsShPUQUBBF/8G1JB4YUBQBA%3D%3D&t=r) near Hamamatsucho Station on the JR Yamanote Line unless otherwise stated. All will require a ticket to board that can either be purchased online, at a convenience store, or in some cases, at the terminal.

**Tokai Kisen**
The Salvia Maru will do summer matsuri style cruises of Tokyo Bay with a nomihodai menu and a Bon-Odori stage show. The ship itself is one big floating festival this time of year with food booths, vendors, dancing and more. Plus until the end of September, you'll get ¥1000 off your ticket if you are wearing a yukata! 
For more information, access [nouryousen.jp](https://www.nouryousen.jp/index.shtml)

**Samurai Cruises** 
They offer 3 different times during the day to sail around Tokyo Bay aboard the replica samurai warship Gozabune Atakemaru. Lunchtime and evening cruises include dining options as well. 
For more information, access [samuraiship.tokyo](https://www.samuraiship.tokyo)

**Yakatabune**
These are the low squat boats you find cruising the open waters and small canals of central Tokyo and Yokohama, from Minato Mirai to Odaiba all the way up to Asakusa. Back in the days of yore, the aristocracy would use then to host dinner parties while taking in the scenery of old Edo and have been a fixture in Japan for over 1000 years. These days they ply the waters around Tokyo as floating restaurants. You can spend anywhere from ¥6000 to ¥10000+ and eat like a king. These boats call at the [Asashio Pier in Harumi.](https://maps.apple.com/?address=6-1,%20Harumi%203-Ch%C5%8Dme,%20Chuo,%20Tokyo,%20Japan%20104-0053&auid=711298111244966809&ll=35.656791,139.779439&lsp=9902&q=Yakatabune%20Harumiya&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEN8BCgQIChAAEiYpPheyUHLTQUAxz25rOcN4YUA5vOzXrJjUQUBBKccmyx15YUBQBA%3D%3D&t=r)
For information, access [yakatabune-tokyo.com](http://yakatabune-tokyo.com)
 
 