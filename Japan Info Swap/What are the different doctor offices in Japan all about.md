# What are the different doctor offices in Japan all about?

If you live in Japan long enough, sooner or later, you will need to see a medical professional for either an ailment, dental cleaning, eye exam or to simply get a a health checkup. Japan has one of the best healthcare systems in the world in terms of accessibility and affordability. Most hospitals and clinics are non-profit or doctor owned and operated and there is universal health insurance that keeps costs in check. And with the country being consistently ranked one of the healthiest, long-living in the world, there are sometimes literally clinics on every corner. But sometimes the hardest part about going to the doctor’s office is figuring out which one to go to— and it’s not just a language barrier issue in many cases.

### One simply does not just go to the hospital...
The first thing you need to do is figure out just what type of doctor you need to go to. In most cases you’ll want to go to a general practitioner. These are found in almost every neighborhood in large cities, especially around train stations. Usually it will simply be the family name of the doctor that owns the practice; something like 田中クリニック; Tanaka Clinic. Start at these offices when you need medicine to treat a cold, or talk about any common problems you encounter. But you may be referred to a specialist for further opinion.

### Small clinics
The first stop many make when getting sick is to a general practitioner or local clinic. The type of condition you have will determine where you should go, as most physicians have a sub-specialty. Here, you simply go directly to the kind of specialist you require, so you’ll want to check the following information for the type of doctor you need to visit:
- **Ear, Nose, Throat (耳鼻科 jibika)** - For issues with your sinuses and allergies. 
- **Orthopedist (整形外科 seikeigeka)**- For sprained or twisted joints and broken bones, start here. Not to be confused with the next entry…
- **Chiropractor (整骨院 seikotsuin)**- If you need your back adjusted, posture corrected and neck popped. 
- **Dentist (歯科院 shikain)**- For teeth cleaning and general oral care.
- **Orthodontist (矯正歯科医 kyoseshika)**- If you need your teeth straightened or braces tightened, this is the place fo you.
- **Pediatricians (小児科 shounika)**- Where to take babies and toddlers; older children usually see the general practitioner.
- **Obstetrician/gynecologist (産婦人科 sanfushinka)**- for general women’s concerns and of course the doctor to see when pregnant.

### Types of hospitals and clinics
If you are needing more specific or longer term care for a condition, then one of the above doctors will usually refer you to a specialist in a hospital. Also these are usually trauma centers and where you go in the event of emergency or transport by ambulance.
- **Municipal Hospitals (市民病院 shiminbyouin), Public General Hospitials (公立総合病院 kouritsu sougou byouin)**- Hospitals that serve the needs of those living in a city of similar level district. May or may not have emergency services. 
- **University Hospitals (大学病院 daigaku byouin)**- Usually named after the university they are affiliated with, many training physicians are found here in addition to research facilities.
- **Other specific hospitals**- these include geriatric, pediatric, pre/post-natal, cancer research, and mental research hospitals. 

### One more thing…
In Japan, for-profit organizations are not permitted to run hospitals and clinics and all Japanese hospitals are non-profit organizations. All administrative decisions are made by physicians and operating committees made up of medical professionals. In addition, all small clinics must be owned and operated by physicians. The costs of most procedures and medicines are determined by a national committee that meets once every 2 years. In simple terms this means the cost for things like teeth cleaning, eye exams, and doctor visits are the same nationwide, from doctor to doctor. There is a high priority on preventive care so costs are usually cheaper for any preemptive treatments rather than their reactionary counterparts. In other words, don’t wait for a toothache to happen when you feel that first twinge of pain. 