# Light up the beach at the Enoshima Toro Lantern Festival
The area to the southwest of Tokyo proper located in Kanagawa Pefecture called Shonan, is known for being a longtime beach playground for anyone into the summer beach scene in Japan, and a local getaway spot for those of us living in Greater Tokyo whenever the stifling heat of summer forces us to search for some seaside bliss. But looking back through history, it has always been an important part of the country and many of Japan's important cultural asserts and events are located in the area. The Katase-Enoshima area of Kamakura City is home to Ryukoji Temple and its summer lantern light up festival, "Tatsunokuchi Take-toro" (竜の口竹灯籠) on the first weekend in August.

About 5000 cut bamboo stalks are turned into candle lanterns and scattered about the temple grounds and are lit just before sunset until around 9pm. The candles themselves represent how the lanterns used to be floated on the nearby river as part of O-bon "okuribi" ceremonies to see one's relatives who have passed away, make it safely to the Great Beyond. But because that also litters the stream and is bad for the environment, the switch was made to this current incarnation back in 2010.

In the past almost-decade, the display has sprawled outside the temple grounds and into the surrounding area and has been transformed into an ever-expanding show of dazzling designs and sparkling lights. The event makes for a great way to wind down a beach trip there and explore the old backstreets of Enoshima. Just try not to get flattened by the Eno-Den tram in the middle of the street there!

### Dates & Access

**_[Tatsunokuchi Take-Toro Festival](http://travelenoshima.jp/festival/taketoro.html)_**

**Address:** [Ryukoji Temple -- 3-13-37 Katase, Fujisawa-Shi, Kanagawa](https://maps.apple.com/?address=13-37,%20Katase%203-Ch%C5%8Dme,%20Fujisawa-Shi,%20Kanagawa,%20Japan%20251-0032&auid=12591139538328968116&ll=35.311290,139.489943&lsp=9902&q=Ryuko-ji%20Temple&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEM0BCgQIChAAEiYp3h7fKkWnQUAxpbz1hYBvYUA5XPQEh2uoQUBBAx1YtNpvYUBQBA%3D%3D)

**By Train:** Enoshima Station [EN06], Shonan-Enoshima Station [Shonan Monorail]

**Date and Time:** First weekend in August (August 3, 4 2019). 6pm~9pm.

**Fees:** Free!

