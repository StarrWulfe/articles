# Here's what has changed with Japan's new cycling laws

You'll be forgiven for thinking that many of the bicycling laws you may have
grown up with in your home country are heavily enforced in Japan as well, since
there is more bicycling traffic here than most other places in the world, but
the reality is starkly different. While there are laws in place that
thoretically prohibit actions such as riding on the wrong side of the street
against traffic or dipping around stopped cars waiting on traffic lights at
intersections (effectively running said stoplight themselves,) the enforcement
and penalties were lax. However this will chang after June 30, 2020 when new
reckless driving laws go into effect.

## What are the laws?
- Riding against traffic
- Menancing pedestrians and other traffic (ringing bell incessantly, harrassing, etc)
- Ignoring traffic signs and rules (not stopZZping at stoplights, not giving way
  to emergency vehicles, etc.)
- Failure to give way to pedestrians.
- Unnecessary braking
Generally speaking, any laws pretaining to road traffic also applies to
cyclist. This also means obeying speed limits, traffic flows, and driving -- er,
biking under the influence. 

## How to cycle safely in Japan
Just use common sense and Japan is a wonderful place to bike in most cases as
may of us use bikes as regular transport. If you find yourself commuting most
days by bike, it would behoove you to take out cycle insurance so in the event
of an accident, you're covered. Most convenenec stores sell policies for
cyclists, and certain cities and towns in Japan have insurance mandates; inquire
at your local city hall, police station or bicycle shop for details.
