#Tokyo's Five Teams of Summer: Baseball in Kanto

Japan is a modern-day mecca of sports having hosted several tournaments in the past and being the hosts of the upcoming World Rugby Cup this fall and the 2020 Summer Games. Answering the question, "what sports do you follow" with soccer or rugby will get you into a long conversation since they’ve become popular pastimes here, but nothing still gets an izakaya hopping like a which team is better in baseball topic. Every major city and region has a team, and they all have rowdy fanatics that make going to one of the games interesting. And like all things Tokyo, we've gone overboard because the Metropolis is home to five different teams. It is very much possible to take in 3 different games in a day by six different teams, thereby having seen a sizeable chunk of Japan Basesball! Let's find out who the teams are, where are they located, and what are their unique charateristics. Maybe one will be a good fit for you to rally behind!

### Tokyo Yomiuri Giants
Of course the 900lb juggernaut in all of Japan Baseball comes first. Hailing from the Tokyo Dome, they have pretty much dominated Japan Professional Baseball due to their marketing as "Japan's Team" outside the country; chances are you've definitely heard of them before the others on this list. 
**Home Field:** [Tokyo Dome](http://www.giants.jp/en/location/)
**Division:** Central
**Website:** http://www.giants.jp/en/

### Tokyo Yakult Swallows
Tokyo's "other" team isn't as well known, but makes up for it by being more approachable with respect to game days and having an "old school" feel to their field, Meiji Jingu Stadium. You'll also be able to see a lot of Japanese baseball traditions here as "chants" and fireworks tend to go off more than at the Dome a few clicks east of here. It is the second oldest baseball field in Japan after all; even Babe Ruth himself rounded the bases once.

**Home Field:** [Meiji jingu Stadium](http://www.yakult-swallows.co.jp/en/#stadiuminfo)
**Division:** Central
**Website:** http://www.yakult-swallows.co.jp/en/

### Yokohama DeNA BayStars
Cross over the Tama River into Kanagawa Prefecture and you'd better hide those Giants caps; this is BayStar country. Even though it's just 20 minutes south of central Tokyo by train, Japan's second city inhabitants are die-hard fans of their team,. Calling Yokohama Stadium home in the heart of downtown Yohohama's Kannai district, you'll be in for an international flair since Minato-Mirai and Chinatown are all a stone's throw away. 

**Home Field:** [Yohohama Stadium](https://www.yokohama-stadium.co.jp/)
**Division:** Central
**Website:** https://www.baystars.co.jp/

### Chiba Lotte Marines
Mickey Mouse and Duffy The Disney Bear aren't the only ones setting off fireworks on the east side of Tokyo Bay in the summertime. Heading over towards the Makuhari Messe exhibition complex, you'll also find the hard-hiting Lotte Marines, calling Zozo Marine Stadium home. This team is known for their cliff-hanging plays and has been recruiting and honing younger talent in the last few years, so they could be the ones to watch in the Pacific League this year.

**Home Field:** [Zozo Marine Stadium](https://en.wikipedia.org/wiki/Zozo_Marine_Stadium)
**Division:** Pacific
**Website:** http://www.marines.co.jp


### Saitama Seibu Lions
Lighting up the foothills between Tokorozawa and Sayama in Saitama are the Seibu Lions. Depending on the year, the Lions are the powerhouse driving the Pacific League, or somewhere just outside first place in the hunt. Their home field isn't a true dome, so much as a stadium with a roof over it, and homers can go right out the open back wall in center field; you'll always find a group of boys in the concourse on that side waiting to scoop a game ball or two. 

**Home Field:** [MetLife Dome](https://www.seibulions.jp/stadium/)
**Division:** Pacific
**Website:** https://www.seibulions.jp/