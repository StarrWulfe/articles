# Festival Tokyo 

![via festival-tokyo.jp](Festival%20Tokyo%20pen%20art.png)One of the benefits to living in the world’s biggest urban area is the simple fact that there’s always something going on in town for you, no matter what you’re into. Take the arts for example. There have been no less than 3 performing arts fairs around the Metropolis just in the past 8 months! If you’re still straining to see the city’s creative side, you still have time because Festival/Tokyo is on the calendar for pretty much the entire month of October and half of November.

### Ten Years Strong
Every year since 2009, Festival/Tokyo has been used as a vehicle for both artists and patrons to explore new social outlets for engaging the arts. Held mainly in venues around the Ikebukuro area of Tokyo, the festival features a program of theatre, dance, music, visual art, and film by cutting-edge artists from Japan and around the world. 

### Arts for Artists (and all of us are artists)
Not just content to showcase the arts passively, F/T is devoted to helping anyone creative to explore their talent and offers a chance for artists to share their insights to the next generation by holding talks, workshops, symposiums and more. Oh and don’t worry about the language barrier, [language guide is provided](https://www.festival-tokyo.jp/19/en/news/language-guide.html "language guide is provided") to help assauge any fears

### Access
**[Festival/Tokyo](https://www.festival-tokyo.jp/19/en.html)** 
- [5 October  - 10 November 2019](https://www.festival-tokyo.jp/19/en/schedule.html)
- [Various venues around Ikebukuro, Tokyo](https://www.festival-tokyo.jp/19/en/schedule.html)
- [Tickets and Pass information on festival website](https://www.festival-tokyo.jp/19/en/ticket.html)
	 