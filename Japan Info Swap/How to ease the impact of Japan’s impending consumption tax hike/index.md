# How to ease the impact of Japan’s impending consumption tax hike

![](Cashless%20Japan%20Poster.jpeg)

In order to ease the literal overnight transition into a higher tax bracket, the [Ministry of Economy, Trade and Industry](https://www.meti.go.jp/english/) has [begun a program](https://www.meti.go.jp/english/press/2019/0828_005.html) that will effectively refund the difference back to consumers. But there are a few catches of course; its only in effect for 9 months beginning 1 October 2019 though 30 June 2020. You’ll need to be paying for your item or services in something other than old-fashioned yen coins and bills— NOT using cash is key to the program. This is because METI is also using the transition period as a way to entice small businesses and outlying consumers to start using and accepting electronic forms of money like credit and debit cards, passes and e-wallets.
![](METI%20Minister%20and%20store%20owner%20posting%20new%20tax%20rebate%20notice.png)
### The devil in the details
* There is no enrollment necessary, simply pay at all participating stores with some form of e-money.
* You’ll either receive an instant rebate of 2 or 5% at the register or receive the equivalent in the form of points that are used on your particular payment instrument during the end-of-period calculation time. 
	* Credit cards will simply add cash back at the end of the month.
	* Debit Cards will do the same as the above, in the form of a deposit into your bank account it is linked to.
	* E-money apps and cards will refund in the form of points for that particular service; they are usually easily converted into cash-back rewards as well.
* As of this writing, over 570,000 shops have signed on to provide this benefit. This includes gas stations, small grocery stores and likely that one ramen stand that stays open past last train that gets your money at least once a month (no judgement here!) provided they have a way to take cashless payments.
* Only Japan issued cards work with the scheme, but there are ways to use an out-of-country card with the scheme. For example iPhone users can use Apple Pay Wallet to generate a Mobile Suica card on their device and register that virtual card with JR East’s JRE Point program and use the points as yen. The virtual Suica card can be recharged with any credit or debit card already registered to your Apple Pay Wallet, including international cards. Another way is to use one of the new QR Code payment apps like LINE Pay and PayPay. They can be recharged with either cash at a convenience store, through an ATM or connected to a charge card and local bank account.
![](https://www.meti.go.jp/press/2019/08/20190828018/20190828018h.png)
### I saw the sign and it opened up my eyes…
You can only receive the discount in the places where you see the Cashless Japan stickers and signs, so make sure you look about!
* Try to shop in small and medium stores and shopping arcades. You’re definitely paying full tax at LaLaPort and Aeon Mall!
* Check the register/point-of-sale area if you don’t immediately see the discount signs. Usually it will be posted next to whatever form of electronic payments they accept.
* If the store is only accepting cash, then expect to pay the full tax. Only electronic payment forms will be able to be reimbursed.
* There’s also [App Store](https://apps.apple.com/app/id1477479075 "iOS") and [Google Play](https://play.google.com/store/apps/details?id=jp.cashless.android "Android") apps you can download and use to find stores taking part in the program too.
![](https://www.meti.go.jp/press/2019/08/20190828018/20190828018d.png)\###Visitors are still welcome, right?
Of course tourists and their out-of-town issued debit cards and so forth are not able to get in on the program. If you’re keen on saving money on souveniers and gifts that you buy while you’re here, you can use the [Tax-Free Japan shopping scheme](https://tax-freeshop.jnto.go.jp/eng/shopping-guide.php) that you can take part in as long as you spend over a certain amount per store and don’t use certain consumables that you purchase while in Japan. 