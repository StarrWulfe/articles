# Let’s Tour: Jimbocho Books District
If you’ve been hanging around Tokyo (or following our _Let’s Tour_ series) long enough, you’ll have no doubt found there’s a pattern with a lot of neighborhoods in the central part of the city in that they sometimes specialize in some form of commerce or government. Harajuku and Omotesando is devoted to fashion, Kappabashi is famous for its cooking and kitchen supply houses, Tsukiji is home of some of the best sushi stands and restaurants; even if the wholesale market that started it all has moved 2kms east, and Tokyo wouldn’t be Tokyo without Akihabara and all things powered by electricity and Japanese pop-culture. Jimbocho Book Street keeps in line with a new rule I’ve dubbed “Japan Rule 84: There’s a neighborhood for everything if you search hard enough.”

### A Short Historical Record
Add history of Jinbocho here.

### My Partial Loss Is Your Gain
It was on a search for a photo book of old streetcars and trams in Japan that led me to spend a day in Jimbocho the first time years ago. I never did find that particular book, but I found a wealth of small first and secondhand booksellers that specialize in any and everything you can imagine. Of course the vast majority of publications are in Japanese, but you can be sure to find that needle-in-a-haystack out-of-print English language book if you comb through the shops enough as well. Not just books either, but almost anything that’s printed on paper or associated with literature can be found in the narrow streets between Yasukuni-dori and Kanda-Suzuran-dori. Also remember: there’s nothing like curling up with a good read over a hot cuppa. Jimbocho is also host to many coffeehouses and tea rooms because of this, so even if you don’t spend time searching for a page-turner, you can at least drop into a cozy spot with your own time waster in hand.

### Where to go
Here are a few shops I can recommend  in order to get you started:

#### Sanseido Books 
Although now one of the largest bookstore chains in Japan, this location is the one that started it all. 
[1-1, Kandajimbocho, Chiyoda, Tokyo](https://maps.apple.com/?address=1,%20Kandajimbocho%201-Ch%C5%8Dme,%20Chiyoda-Ku,%20Tokyo,%20Japan%20101-0051&auid=12771989826295796415&ll=35.695637,139.760550&lsp=9902&q=%E4%B8%89%E7%9C%81%E5%A0%82%E5%8F%A4%E6%9B%B8%E9%A4%A8&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEPYCCgQIChAAEiYpapfbU3nYQUAx1/ZaUCp4YUA56GwBsJ/ZQUBBCzaA7YR4YUBQBA%3D%3D&t=r)
[https://www.books-sanseido.co.jp](https://www.books-sanseido.co.jp)

#### Tokyodo
One of the original booksellers in this location, Tokyodo is known for it’s themed floors dealing with the past, present and future of humanity in terms of learning and thinking. The top floor has a small space where novelists, writers and authors come and discuss their manuscripts on a regular basis. It also happens to have one of the best coffeeshops in the area on the first floor in the form of Paperback Cafe. 
[1-17 Kandajimbocho, Chiyoda, Tokyo](https://maps.apple.com/?address=17,%20Kandajimbocho%201-Ch%C5%8Dme,%20Chiyoda-Ku,%20Tokyo,%20Japan%20101-0051&auid=3575855300008607956&ll=35.695205,139.759977&lsp=9902&q=Tokyodo&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEPYCCgQIChAAEiYpfmcgLGrYQUAxtLwY8iN4YUA5/DxGiJDZQUBBUosbj354YUBQBA%3D%3D&t=m)
[http://www.tokyodo-web.co.jp](http://www.tokyodo-web.co.jp)

### Access
The whole neighborhood is within a 5 minute walk of [Jimbocho station](https://maps.apple.com/?address=Kandajimbocho,%20Chiyoda-Ku,%20Tokyo,%20Japan%20101-0051&auid=8705341901325857199&ll=35.695983,139.758070&lsp=9902&q=Jimbocho%20Station&_ext=CiYKBAgFEAMKBQgGEOEBCgQIChANCgUIJRCQAwoECCoQAQoECDoQARIkKefnRIf42EFAMaIvSww5eGFAOTOsf2Yz2UFAQWhVvStLeGFA&t=r), served by the Hanzomon (H), Mita (I) and Shinjuku (S) lines.