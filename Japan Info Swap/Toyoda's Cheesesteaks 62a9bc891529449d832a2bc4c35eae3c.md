# Toyoda's Cheesesteaks

Deadline: Nov 26, 2020
Edition: Dec 2020
Last Edited: Dec 4, 2020 11:40 AM
Status: Published
URL 1: https://bento.com/rev/5189.html

# Toyoda's Cheesesteaks brings a taste of Philly from South Street to Koshu Kaido

Let me be up front— I am probably closer to being from [Philadelphia, Mississippi](https://en.wikipedia.org/wiki/Philadelphia,_Mississippi) than [Philadelphia, Pennsylvania](https://en.wikipedia.org/wiki/Philadelphia) when it comes to the innate knowledge and understanding of the quintessential [cheesesteak](https://en.wikipedia.org/wiki/Cheesesteak) sandwich. Philadelphians are rabidly protective of their hometown's symbols, from Rocky running up the [Rocky Steps at the Museum of Art](https://en.wikipedia.org/wiki/Rocky_Steps) or their [strange hockey mascot](https://en.wikipedia.org/wiki/Gritty_(mascot)#:~:text=Gritty%20is%20the%20official%20mascot,the%20Philadelphia%20Phillies%20baseball%20team.) and its [many memes](https://knowyourmeme.com/editorials/collections/the-best-gritty-memes).  Even the [two famed keepers of the Philly Cheesesteak bloodline keep the battlelines drawn](https://www.phillybite.com/index.php/travel/943-pat-s-vs-geno-s-philly-s-cheesesteak-debate) on the southside of that city in a perennial war of innovation versus tradition. I'll come out and say it off the top— nothing at Toyoda's is going to beat the "hometown heroes on a hogie." But being that we are halfway around the world where the only other option for something remotely resembling the authentic chopped steak sandwich would involve looking for locations of a fast food chain whose name is synonymous with underground rapid transit, Toyoda's is definitely worth the look.

Located in a nondescript late Showa-era storefront in the Hatagaya neighborhood roughly halfway between Shinjuku and Meiji University, about a 3 minute walk from the Keio train station underneath the same street, the inside of Toyoda's is a bare-bones homage to any American greasy spoon lunch spot. As soon as you walk in, the counter and cash register is on the left with the flattop griddle sitting just behind it and a poster board menu with photos of every possible sandwich that can be made. To the right is a small seating area for about 6 socially distanced customers; I'm sure more were able to be seated in the pre-pandemic days. Dead ahead is a genuine Weber grill and a bag of Kingsford charcoal as decoration, along with a self-serve soda fountain for dine-in customers, again turned off due to the... current conditions. Luckily Toyoda's does take-away and if you're living or working nearby, they have a listing on Uber Eats as well.

You can have a Philly with garlic, avocado, asparagus, teriyaki sauce, egg, garlic...the list goes on and on for traditionalists and adventurists alike. But Toyoda's menu offers even more variety in the way of the barbecue sandwich collection; you thought that aforementioned grill was just for show, didn't you? There's are juicy pulled pork and beef brisket sandwiches as well as pork and beef rib plates on offer as well. Combo it up with fries, mac 'n cheese or slaw for an extra few hundred yen to make your sandwich into a meal.  

With prices refreshingly consistent to what you'd expect from Pat's or Geno's in Philly and not the eye-gouging norm for most overseas tastes here in the Metropolis, its very easy to justify coming a little out the way from Shinjuku proper. You definitely won't find any crowds here on this side of town (which in 2020 is a good thing perhaps,) not to mention the aesthetics of the shop along with the taste of the food had this St. Louis native reminiscing about the corner BBQ shacks in my hometown.

〒151-0072 Tokyo, Shibuya City, Hatagaya, 1 Chome−4−1 Hana Bldg 1F

[https://goo.gl/maps/W8DjtMsj7v8R46ki9](https://goo.gl/maps/W8DjtMsj7v8R46ki9)

Transit: Keio New Line, Hatagaya Station
Hours: Everyday 11am~10:30pm; Closed Wednesdays.
Phone: 03-6276-6658

[東京でのトヨダチーズステーキ TOYODA'S CHEESE STEAKの宅配・出前・デリバリーを注文 ｜テイクアウトメニューと値段｜ウーバーイーツ](https://www.ubereats.com/jp/tokyo/food-delivery/トヨタチースステーキ-toyodas-cheese-steak/uB5_FJAmTtm0KfVXg-Q6Kw?utm_source=google&utm_medium=organic&utm_campaign=place-action-link)