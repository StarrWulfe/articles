# Tokyo Tower is 'lit' all summer long!
Tokyo Tower may have become Tokyo's second tallest tower in the last few years thanks to the SkyTree over in Sumida Ward stealing the title back in 2010, but the Eiffel Tower lookalike is not to be outdone anytime soon, thanks to it's more central location and constant stream of events like these.

### Milky Way Illumination
Of course the main attraction at Tokyo Tower is the view of Tokyo from 150 meters high. And while the sight of the urban expanse of Tokyo is jaw-dropping alone, we can always add a little more, right? The folks in charge of the observation deck have done just that and installed a state-of-the-art projection system along with and LED array that helps give the effect of a starry night sky, complete with the occasional shooting star, hovering just above the skyline outside the windows. It's [Tanabata](https://en.wikipedia.org/wiki/Tanabata) season after all right?

### City Light Fantasia
Don't let this year's elongated rainy season fool you; it's still summer and sooner or later you'll bear hearing the cacophonous shrills of cicadas and see splashes of color from [yukata](https://en.wikipedia.org/wiki/Yukata) and [jimbei](https://en.wikipedia.org/wiki/Jinbei) clad people on their way to summer festivals. Tokyo Tower is recreating this atmosphere with the same setup as above as part of is ongoing "City Light Fantasia" event. "Cool Summer Japan" shows what most Japanese associate summertime with. Sunflowers in bloom, fireworks, a lovely golden sunset, and more.  

###Times and Access
**Dates:** Running now until Sunday September 1st 2019.
**Times:** The Milky Way LED light up is on all day, 9am until 10:50pm. City Light Fantasia runs from 7pm until 10:50 pm; Tokyo Tower is open everyday.
**Admission Fee** Only [a ticket to the Main Observation Deck](https://www.tokyotower.co.jp/price/en.html) is needed and there's no separate fee. [Complete fee info is here.](https://www.tokyotower.co.jp/price/en.html)
[Tokyo Tower](https://www.tokyotower.co.jp/price/en.html)
[Shibakoen 4-2-8,
Minato, Tokyo](https://maps.apple.com/?address=2-8,%20Shibakoen%204-Ch%C5%8Dme,%20Minato,%20Tokyo,%20Japan%20105-0011&auid=11483186979552438639&ll=35.658558,139.745504&lsp=9902&q=Tokyo%20Tower&_ext=CiQKBAgEEAoKBAgFEAMKBAgGEHMKBAgKEA4KBAgQEAEKBAgvEEoSJikxWdxwutNBQDHJAoGNrXdhQDmvLgLN4NRBQEHx898fCHhhQFAE&t=r)
(03)3433-5111
**Nearest Stations:** Akeabane-bashi [E21], Kamiyacho [H05].

---
*Images: www.tokyotower.co.jp* 