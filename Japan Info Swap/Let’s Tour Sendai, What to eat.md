# Let’s Tour: Sendai, The City of Trees
## What to eat

No review of any part of Japan would be complete without talking about its contributions to the gastronomic scene, and Sendai has definitely done its part, and there are TONS of places to check out local delectables.

#### Beef Tounge
Ask any Japanese person anywhere what food Sendai is famous for, and you will definitely get *”gyu tan”* as an answer most of the time. The version served in Sendai is tender, thick and aged like a good steak and has the exact same flavors of a rich Delmonico cut. The usual serving is usually served with a side of ox tail soup and *mugimeshi*, rice cooked with barley. 

**~Date no Gyu Tan~**
I don’t think Date Masamune himself ate here, but he definitely would if he were around today. The Sendai based group has locations all over Sendai and  even a few in Tokyo, but I recommend checking out the main restaurant right on the grounds of Aoba Castle Park. 
[1 Kawauchi, Aoba, Sendai, Miyagi](https://goo.gl/maps/fycTQ6BCjmr)
www.dategyu.jp

#### Zunda Mochi
You may be used to eating *edamame* beans lightly covered with salt as a complement to beer, sake, or whiskey at many *izakaya* taverns around Nippon. However in Sendai, they also crush them up, combine them with sugar and use it as filling in soft rice *mochi* cakes and even *taiyaki* fish (and other) shaped cakes.

>>Need the name of the Taiyaki place Brian took me to before Sendai Station<<

#### Sasa Kamaboko
Made by taking ground whitefish with salt (*kamaboko*) and forming it into the shape of a bamboo leaf (*sasa*), the resulting grilled fish cake on a stick can be had in plain, cheese, beef tounge, and even smoked flavors. 

**~Abe Kamaboko~**
This store specializes in a lot of Sendai’s regional dishes (including zundamochi MILKSHAKES!) and will grill up their signature sasa kamaboko for you while you wait. 
[2-3-18 Chūō, Aoba, Sendai, Miyagi](Marked Location)

#### Oysters
Hiroshima is perhaps more well known outside Japan for these mussels but any shell-head here also knows Matsushima Bay on the northeastern coast of Sendai is also famous for their oyster habitat and harvesting operations. You can have your *kaki* raw, grilled or battered and fried, and if you’re in Sendai from mid-autumn until early spring, most every area restaurant will have something on the menu involving them. 

**~Sen Kichi~**
This izakaya is operated by the same company as the [Ciel Guesthouse hostel](link back to other article here) above it, and is a great place to wander over to if you can’t really make your mind up about lunch or dinner during your stay because they have all of Sendai’s “greatest hits” on offer, including local sake, beef tongue and of course oysters when in season. If you still can’t decide and you’re out with friends, just go for the course menu and get everything for one low price! 
 [3-1-15 Kokubuncho, Aoba, Sendai, Miyagi](https://goo.gl/maps/7gHcqXmVsJo)
izakaya-senkichi.owst.jp/en


- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
*Images: [WORKNAME]() by [PERSON](), [CC~~]()*