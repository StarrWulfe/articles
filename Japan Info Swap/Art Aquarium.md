# Art Aquarium retells the old Japanese tradition art and kingyo in a new way
Anyone with a penchant for Japanese history and a love of Japanese artistry will recognize the Japanese Goldfish, “Kingyo”, as a cultural motif instantly. Seen everywhere from anime and manga, to high fashion and the occasional tattoo, kingyo have been held in high regard ever since they were kept as rare novelties of the rich and wealthy of the Edo Period 500+ years ago. Starting in the Meiji era though, many people started keeping them as pets and even based a popular festival game, [*kingyo sukui* or “goldfish scooping”](https://i.ytimg.com/vi/2XdR7uQq6Ug/maxresdefault.jpg), on the premise that if you’re dexterous enough to scoop a fish out using a flimsy scoop, you get to keep your new pet! For those of us lacking the space and/or time to give to the golden swimmers in our homes, there’s the Art Aquarium exhibits.
### All that and a kettle of fish
Make that a TANK of fish. Self-proclaimed “aquartist” Hidetomo Kimura combines visual, aural and spatial art along with goldfish to create a traveling pop-up gallery like no other. The Tokyo exhibition is being held at Nihonbashi Mitsui Hall for the last time after a run of 8 years, the largest ever collection of 10,000 goldfish is on display along with a compilation of artwork to complement it. A combination of crystal, glass, paint, light, sound and water immerses all senses into a new world where the fish seem to dance in mid-air.
### The nighttime is the right time
During the day you can check out the exhibits at your leisure, but after 7pm most nights, the *Night Aquarium* comes alive and the venue takes on the style of an Old Edo Era saloon with a 21st century makeover. Enjoy Kyo-Mai traditional geisha and maiko performances one evening or a selection of the hottest club DJs the next. [Check the calendar](http://artaquarium.jp/en/nihonbashi2019/#nightaquarium) for details. 
### The Details…
**[ART AQUARIUM](http://artaquarium.jp/en/nihonbashi2019)**
**Venue:** 
Nihonbashi Mitsui Hall
5F, COREDO Muromachi 1
[Nihonbashi Muromachi 2-2-1, Chuo-ku, Tokyo](https://maps.google.com/maps?ll=35.686673,139.774382&z=16&t=m&hl=en-US&gl=JP&mapclient=embed&cid=5795482422667624146)
**By Train:** [Mitsukoshi-Mae station](https://goo.gl/maps/iavTmMDBofSFvEAL6) [G,Z]; [Shin-Nihonbashi station](https://goo.gl/maps/R8UZFBNFnVZNsrej7) [JO] 
**Dates & Times:** Now until 23 Sept, 2019 Everyday. 11am~10pm, Saturdays until 11pm. Special Event Days have a slightly different schedule; [check this page](http://artaquarium.jp/en/nihonbashi2019/#ticket) for info.
**Ticket Fees:**  ¥1000; 12yrs and younger, ¥600; 3yrs and below, Free. Please access [pre-order and discount ticket information here](http://artaquarium.jp/en/nihonbashi2019/#ticket).
