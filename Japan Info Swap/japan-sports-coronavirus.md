# Japan's Summertime Professional Sports Post-Coronavirus

Much like the rest of the world due to the COVID-19 pandemic, anything that would attract large numbers of
people gathering together for hours on end have been postponed or canceled in
Japan, including Major League sports. The situation remains fluid however as
some sports leagues are finding ways to deal with the situation despite it all.
Here's a brief rundown of what the different sports teams and organizations
around Japan are doing in the "new normal":

### Japan Major League Baseball (NPB)
The season has been shortened after play got underway three months later than
planned on June 19 and only in empty stadiums. The shortened season will go on
through November. There are tentative plans to welcome fans back into stadiums
sometime July 10 starting at a very low 5000 spectators per game, depending on
government guidelines by then. 

### Summer Sumo Tournament
Japan's national sport has already had to hold its March spring torney
sans-spectators, totally canceled the May meets and moved the summer bouts in July from Nagoya to Tokyo, also
with no plans for fans in the stands. It remains unclear what happens beyond
that point, but you can bet they will be extra cautious to keep both grapplers
and fans healthy; an apprentice wrestler died from COVID-19 related
complications May 13. 

### J-League Soccer
The season is set to open a month late to empty arenas on July 4, however only
the first two matches per team will be held this way; the plan is to gradually
let live spectators join in after that.  

### Formula One
F1's October 11th stop at Suzuka Circuit in Mie Prefecture for the Japan Gran Prix is
canceled for 2020 due to the uncertainty of travel restrictions getting into the
country at this time.

### 2020 Tokyo Olympics
Of course the major blow to this year's sporting calendar was the announcement
of the Tokyo Summer Games being postponed to July of 2021. 

For those sports that are still planned or ongoing, it is our recommendation
that if possible to watch on TV or internet as there is still an air of
uncertainty as to how virulent Coronavirus spreads. Most games/matches are
televised so even the biggest superfans will be able to see almost every angle
of play.
