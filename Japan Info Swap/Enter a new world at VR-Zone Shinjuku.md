# Enter a new world at VR-Zone Shinjuku
As the goggles slipped over my face and headphones popped over my ears, I was told to relax and move as naturally as possible. “Don’t worry, you will not bump into any walls or people because of the tracking system.” Suddenly the test screen disappeared and I could see a whole world around me along with other player’s avatars moving in real time. I had entered the world of Ghost In The Shell. But this wasn’t at some high-tech exhibition showing off new technology for the press; this was real, live, in-production virtual reality gaming at the first arcade dedicated to nothing but VR gaming. I had entered the VR-Zone.

Located right in the heart of the Kabukicho district north of Shinjuku Station, VR-Zone by Bandai-Nampo opened in 2017 to showcase how VR gaming can be made into an amusement experience just like going to a theme park for a day of activities. Just like such, you can expect the types of VR games and activities to change from time to time and remain fresh and up-to-date. Unlike going to a theme park, you don’t need to worry about the weather since everything is inside. 

There are three different kinds of VR experiences that can be had: 
* *VR Activity* Here, you can participate in physical challenges like flying a bike (yep, FLYING a bike), river rafting, learning Goku’s Kamehameha move from Dragon Ball, piloting an EVA unit from Evangelion, and driving a Mario Kart. You must wear VR goggles and the world is 100% simulated.
* *Field Activity* Group activities where you strap on a small computer backpack and wear motion tracking equipment because you’ll be walking around interacting and trying to out maneuver each other. The aforementioned Ghost In The Shell game is part of this group.
* *Activity* You don’t actually need any VR equipment here because the whole environment is set up to look, feel and react realistically. You can try climbing Niagara Falls, or break out of a panic room. 

### Concert time!
For those of you into 2D idol groups (or might just be curious as to what the future of live concerts may be) you can also attend an interactive VR concert. As of this writing, Idolm@sters Cinderella Girls: New Generations are on the bill and in true Japan idol live show tradition, not only do you get the concert, but also see each of the performers be interviewed as well.

### Access and Fees:
[1-29-1 Kabuki-cho, Shinjuku-ku, Tokyo](https://maps.google.com/maps?ll=35.695734,139.700655&z=16&t=m&hl=en-US&gl=JP&mapclient=embed&q=1%20Chome-29-1%20Kabukich%C5%8D%20Shinjuku-ku%2C%20T%C5%8Dky%C5%8D-to%20160-0021)
*By Train:*
* about 7 minutes' walk from JR
* Shinjuku Station's East exit
* about 2 minutes' walk from Seibu Shinjuku Station
* about 10 minutes' walk from Tokyo Metro Shinjuku-sanchome Station
* about 10 minutes' walk from Tokyo Metro Higashi-Shinjuku Station
*Hours*
Everyday, 10:00~22:00 (Final Entry 21:00)

Tickets and fees are variable; please visit their website for the latest information and to make reservations:
[VRzone-Shinjuku Website](https://vrzone-pic.com/shinjuku/en/)

### One more thing...
There is now a VR-Zone in Osaka in Hep Five near Osaka Station/Umeda. So now our Kansai friends can also check it out too!
https://vrzone-pic.com/osaka/
- - - -
—by [Jason L Gatewood](http://jlgatewood.com)

*Images:*

![](Enter%20a%20new%20world%20at%20VR-Zone%20Shinjuku/39548179182_80cd941d2f_z_d.jpg)
![](Enter%20a%20new%20world%20at%20VR-Zone%20Shinjuku/40173107314_0637af1dc3_z_d.jpg)
" [VR Zone](https://www.flickr.com/photos/crissyteena/40173107314/) " ( [CC BY-NC-ND 2.0](https://creativecommons.org/licenses/by-nc-nd/2.0/) ) by  [Crissy Teena](https://www.flickr.com/people/crissyteena/) 


