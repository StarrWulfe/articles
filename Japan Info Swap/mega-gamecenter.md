# Round One Spocha is the mother of all game centers

What place in Japan has a gazillion flashy lights, dazzling video screens, ear-busting loud noises and is teeming with people pressing their luck on games of chance and skill? Nope, not the pachinko parlor, but a more wholesome (maybe) way of sinking your hard-earned yen into oblivion: *Game Centers*. These are ginormous video arcades that you'll find all over the country, usually near train stations and inside shopping areas. Years ago they were limited to the types of games you'd find at a video arcade in your neck of the woods but slowly the advent of a type of game center has appeared to appeal to the masses' hunger for more types of amusements under one roof. Enter the *Mega* Game Center!

### From Archery to Volleyball, They Got It All!
Round One is the progenitor of these types of game centers where for one flat fee, you can play different sports all under one roof. Spocha (スポッチャ) is a Japan-coined portmanteau of "sports challenge" and is the brand that the biggest Round One gaming centers are using to advertise this offering. Only the biggest centers will have the space necessary. However there are many scattered around greater Tokyo with the [Odaiba location](https://goo.gl/maps/jVSaG2j5EZQfdEyeA) being the most convenient to those right in the middle of the Metropolis. The list of different sports and challenges are long: I'm a frequent user of the Kawasaki location because of the [bubble soccer](https://www.instagram.com/p/BkuhwpMAXub/?tagged=spocha) and [Segway racing](https://i.ytimg.com/vi/n3ayKMH3NeU/maxresdefault.jpg). 

### How do I use the facilities?
The Supocha set of amusements are different and separate from the usual set of video games and even bowling lanes that you'll find at other Round One centers, and is a time based activity. Of course the first thing to do is to pay at the front desk upon entering. You can purchase time in either 90 minute or 3 hour blocks, and for just a few hundred yen more, get a day-pass like "free time plan" to use until the place closes if you want. Once you get your pass (wristbands with a barcode on them), just head over to the Supocha part of the complex and keep your bands on in case the staff ask to check them. The different sports areas are on a first-come-first-serve basis and you may need to wait your turn by signing on a form next to the different areas at some locations. You can find [more information on this PDF](https://www.round1.co.jp/shop/pdf/howtoplay/howtoplay_spo.pdf). When you're finished, just return to the counter and turn your wristbands in. If there are any other charges (other amusements or food) you'll need to pay them at that time. 

### Access
Nationwide, all Round One STADIUM locations have the Spocha experience located inside them; Please [access this list](http://www.round1.co.jp/service/spo-cha/spo-cha_shoplist.html) to find one near you. 
Here are a few located around Greater Tokyo:
- [Central Tokyo/Odaiba](http://www.round1.co.jp/shop/facility/tokyo-divercity.html)
- [Central Tokyo/Itabashi](http://www.round1.co.jp/shop/facility/tokyo-itabashi.html)
- [Kanagawa/Kawasaki](http://www.round1.co.jp/shop/facility/kanagawa-kawasaki.html)
- [Saitama/Asaka](http://www.round1.co.jp/shop/facility/saitama-asaka.html)




