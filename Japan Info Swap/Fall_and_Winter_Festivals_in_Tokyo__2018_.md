# Fall and Winter Festivals in Tokyo
Just because the leaves are golden (or totally gone), and the temperature has fallen several notches, doesn’t mean the matsuri level goes down along with it. Just as always, you’ll be able to eat tasty Japanese yatai street food, purchase some cool trinkets, and enjoy being outside in the crowd… Just remember to bring your jacket!

Fall in Japan means it’s time for the “Tori-No-Ichi” festivals; literally meaning “market held on the day of the rooster” (the old Chinese calendar noted the day of the rooster comes every 12 days in November.) Here, all sorts of things are being sold to get ready for the busy time at the end of the year and bring luck for the coming one, including *kumade* rakes, which are blessed charms businesses buy in order to “rake in the cash” by hanging them in their establishments. Some of these ornately decorative charms are over US$1000, but many are priced within reason if you fancy picking one up. 

### Hanazono Shrine Tori no Ichi 
**31 Oct-1 Nov, 12-13 Nov, 25-26 Nov 2018**
If you have any business in the Shinjuku area over the next month, you can’t go wrong with wondering over to Hanazono shrine wedged literally in the middle of the ward’s government complex. This Tori no Ichi festival is over 400 years old and even though the neighboring Golden Gai and Kabukicho area is as modern as ever, there’s no sign of this old-school fall festival falling out of style.
**Address:** [5-17-3 Shinjuku, Shinjuku-ku, Tokyo](https://maps.apple.com/?address=17-3,%20Shinjuku%205-Ch%C5%8Dme,%20Shinjuku-Ku,%20Tokyo,%20Japan%20160-0022&ll=35.693508,139.705211&q=17-3,%20Shinjuku%205-Ch%C5%8Dme&_ext=EiQpWZgqrjHYQUAxi0A+yWN2YUA5121QCljZQUBBgbDAZb52YUA%3D&t=r) 
**Access:** Shinjuku San-chome station [M09][F13][S02]; Shinjuku Station [JB10][JC05][JA11][JY17][JS20][OH01][KO01]
**Web:** [http://www.hanazono-jinja.or.jp (Japanese)](http://www.hanazono-jinja.or.jp/mt/cms/webdir/index.html)

### Asakusa Tori-no-ichi
**1, 13, 25 Nov 2018**
Admittedly not exactly a hop and a skip away from the main action that happens around Senzō-ji to the south, the festival at Ōtori Jinja is still worth the extra walk because of the “shitamachi” factor. This is one of Tokyo’s oldest neighborhoods and you’ll be mingling with 
**Address:** [Ōtori Shrine, 3-18-7 Senzoku, Taitō-ku, Tōkyō-to](https://www.google.com/maps/place/Ohtori+Shrine/@35.7226487,139.7914962,19.54z/data=!4m13!1m7!3m6!1s0x0:0x0!2zMzXCsDQzJzIxLjIiTiAxMznCsDQ3JzMwLjgiRQ!3b1!8m2!3d35.722568!4d139.791886!3m4!1s0x60188e92ebab2383:0x5ada1c263a5ce60b!8m2!3d35.7225012!4d139.7919609?hl=en)
**Access:** Iriya Station [H18]
**Web:** [http://www.torinoichi.jp/](http://www.torinoichi.jp/english/index.htm)

### Okunitama Tori-no-Ichi
**1, 13, 25 Nov 2018**
The other major place to catch this same festival is in the city of  Fuchū, in Tokyo’s western suburbs. There are many stalls in place here and it is also customary to get a “financial fortune” read if you are a business owner or have a venture you wish to embark on in the coming year. For practical purposes, Okunitama is also only a short 5 minute walk from Fuchu station and there’s a huge mall complex in between so you can make a good day of it if you plan right. 
**Address** [3-1 Miya-machi, Fuchu-shi, Tokyo](https://www.google.com/maps/place/Okunitama+Jinja/@35.667357,139.478926,16z/data=!4m5!3m4!1s0x0:0x744e90e176833fcb!8m2!3d35.6673573!4d139.4789258?hl=en-US)
**Access** Fuchū Station [KO24]
**Web:** [https://www.ookunitamajinja.or.jp/](https://www.ookunitamajinja.or.jp/)

### Smart Illumination Yokohama 2018 
**31 Oct-31 Dec 2018**
Japan’s Second City gets lit with the latest in LED tech turning the bayshore next to “Elephant Trunk Park” into a digital nighttime art installation. The main festival happens from Oct 31 until Nov 4, but the lights will be on until the end of the year, helping out with winter holiday illumination duty. 
**Address:** [Zō no Hana Park, 1-1-1 Kaigandori, Naka-ku, Yokohama, Kanagawa](https://maps.apple.com/?address=1-1,%20Kaigandori%201-Ch%C5%8Dme,%20Naka-Ku,%20Yokohama,%20Kanagawa,%20Japan%20231-0002&auid=14959744650780003730&ll=35.449540,139.643140&lsp=9902&q=Zo%20no%20Hana%20Terrrace&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEN8BCgQIChAAEiQpFDlEzfK4QUAxuGnSRWp0YUA5kg5qKRm6QUBBNnHCm8R0YUA%3D&t=r)
**Access:** Nihon-Odori Station [MM05]
**Web:** [http://www.smart-illumination.jp](http://www.smart-illumination.jp)

### UNU Flea Market: Fall Sake Fest
**17, 18 Nov 2018**
Remember the [coffee festival we reported on](https://japaninfoswap.com/tokyo-java-why-are-indie-coffee-shops-opening-all-over-the-metropolis/) in the springtime on the grounds of the UN University’s flea market? This is the same thing, just with sake making the rounds. More than 30 makers of Japan’s signature adult beverage will have their wares out for tasting, so make sure you “pregame” accordingly! 
 **Address:** [UN University, 5-53-70 Jingumae, Shibuya, Tokyo](https://maps.apple.com/?address=53-70,%20Jingumae%205-Ch%C5%8Dme,%20Shibuya,%20Tokyo,%20Japan%20150-0001&auid=8341885731051049074&ll=35.662357,139.708262&lsp=9902&q=United%20Nations%20University&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBkKBAgKEAASJCkW+VDuNNRBQDHZ5mPMfHZhQDmUznZKW9VBQEETuNhf13ZhQA%3D%3D&t=r)
**Access:** Omotesando Station [C04][G02][Z02]
**Web:** [http://www.farmersmarkets.jp/](http://www.farmersmarkets.jp/)

### Shichi-Go-San Festival
**around 15 Nov**
You’ll likely notice a lot of well dressed parents toting along their three, five and seven year-old children clad in their best kimono around this time. They are heading to area shrines to pray for the children's future health and good fortune, and also take family photos that will become keepsakes for their generation. Not necessarily a festival per-se, but some local shrines have set up a food and games stall or three to entertain the families waiting; you can simply hang out and people watch for a spell if inclined.
[Shichi-go-san festival information](https://en.wikipedia.org/wiki/Shichi-Go-San)


Also, let’s not forget about [fall leaf viewing](https://japaninfoswap.com/fall-foliage-in-central-tokyo/) and [winter illumination](http://japaninfoswap.com/christmas-holiday-illumination-tokyo/) festivals that are popping up around town during this time as well! 


- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
*Images: Okunitama Tora-no-Ichi by [Jason L Gatewood](http://jlgatewood.com), [CC~~]()*

![](Fall%20and%20Winter%20Festivals%20in%20Tokyo/okunitama-tora-no-ichi-02.jpg)
![](Fall%20and%20Winter%20Festivals%20in%20Tokyo/okunitama-tora-no-ichi-01.jpg)
![](Fall%20and%20Winter%20Festivals%20in%20Tokyo/view%20from%20mt%20takao%20jlg.jpg)
