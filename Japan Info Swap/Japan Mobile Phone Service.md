<p style="text-align: left;"><strong><img class="size-thumbnail wp-image-3583 alignleft" src="http://japaninfoswap.com/wp-content/uploads/2012/04/Iphone-150x150.png" alt="iPhone 4 Photo" width="150" height="150" /></strong></p>
In recent years, the number of companies offering mobile phone service in Japan has increased substantially, along with the variety of plans available. While not a comprehensive list by any means, the information listed below should serve as a good introduction.

<strong>The Big Three</strong>

Docomo, SoftBank, and au are the three largest providers in Japan and the original incumbent carriers from the days of analog service 30 years ago. They have very similar plans and costs, and require a two-year contract. The biggest advantage they provide used to be the subsidized equipment cost (meaning the ability to divide the cost of the actual phone into monthly payments as well as deeply discounting the price of said phone) but thanks to sharp discounts on smartphones direct from electronics shops, online stores, MVNOs (more on them later) and the ability to <q>BYOD</q> (Bring Your Own Device), that’s not much of an issue anymore.
<table>
<thead>
<tr>
<th><strong>Provider</strong></th>
<th>Softbank</th>
<th>NTT Docomo</th>
<th>au</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>Monthly Cost</strong></td>
<td>Unlimited Talk ¥¥2,700+tax; S-Basic ¥300+tax; 5GB Data Plan ¥5,000+tax, Device Fee ¥3,678; Discount ¥3,220 – Total: ¥9,090 (tax inclusive)</td>
<td>Unlimited Talk ¥2,700+tax; SP Mode ¥300+tax; 5GB Data Plan ¥5,000+tax ;Device Fee ¥3,429; Discount ¥2,322 – Total: ¥9,747 (tax inclusive)</td>
<td>Unlimited Talk ¥2,700+tax; LTE Net ¥300+tax; 5GB Data Plan ¥5,000+tax; Device Fee ¥3,300; Discount ¥2,850 – Total: ¥9,090 (tax inclusive)</td>
</tr>
<tr>
<td><strong>Notes</strong></td>
<td>Softbank tends to have the best English support. English Customer Service: 0800-919-0157 (Toll Free), Press <q>8</q> for English support</td>
<td>Docomo has good family plans for those requiring 3+ devices. English Customer Service: 0120-005-250 (Toll Free)</td>
<td>English Customer Service: 0120-959-472 (Toll Free)</td>
</tr>
</tbody>
</table>
<ul>
 	<li>Each provider offers an Unlimited Talk Lite option for ¥1,700+tax/month, which allows unlimited calls until 5 minutes, and then charges for the talk time in excess of 5 minutes. Many people choose this option and then use VoIP services such as LINE or WhatsApp to avoid any calling charges.</li>
 	<li>Data cap is for high-speed data. If you go over the cap, in general your speed is just reduced to 128kbps until the next monthly cycle. In most cases you can visit your carrier’s web portal, app or store and purchase additional data buckets a-la-carte (I receive a text message reminding me of this fact on Docomo.) Some companies have the option to automatically charge for additional high-speed data, <u>so please check the company policy of your provider when you make a contract</u> .</li>
 	<li>Monthly cost will change depending on the device chosen, the data plan chosen, and the talking plan chosen. Every effort has been taken for accuracy; however, please contact the companies directly for confirmed/updated costs.</li>
 	<li>SIM Unlocking of your carrier-subsidized device is available after 100 days has passed on your contract for a fee of ¥3000 or free if done through your carrier’s web portal.</li>
</ul>
<strong>Y!mobile, UQ Mobile</strong>

Y!mobile is what Softbank decided to rebrand E-Mobile after their acquisition back in 2014. UQ Mobile is a sister company to AU since it’s a part of KDDI Communications. Both are predominately lower cost brands with a few brick-and-mortar stores offering lower prices than the Big Three. That being said, the way they keep costs low is by limiting support to online-only (very little of which is in English as of this writing), a smaller selection of devices (although there is BYOD available). One big advantage is the ability to still bundle fiber optic broadband with their wireless options, so if you also are needing internet at home, you may want to check their latest campaigns.
<ul>
 	<li>For SIM-only contracts, please check to make sure your device is unlocked and supported by Y!mobile in advance.</li>
</ul>
<strong>Low Cost SIM-only Options</strong>

In 2014, Japan established a <q>mobile virtual network operator</q> or MVNO market, to encourage competition. Since then, there is no dearth of <q>kyakuyasu 客安</q> low cost MVNO providers that have stepped in and provide service to all budgets, needs and tastes. These companies also offer data only SIMs as well, and in this day of being able to use VoIP services like FaceTime, LINE Call, Skype, and more, you really should consider doing away with buckets of minutes in favor of having higher data allocations for your communications needs. The details below relate to SIM offerings which include a calling function, but only on an as-needed basis.

<strong>Advantages</strong>
<ul>
 	<li>Low monthly service cost</li>
 	<li>Ability to use a phone from overseas provided it is unlocked and supported</li>
 	<li>Shorter contract term requirements (often 1-year)</li>
</ul>
<strong>Disadvantages</strong>
<ul>
 	<li>Somewhat lower speeds than the Big Three</li>
 	<li>Lack of English support</li>
 	<li>Lack of brick-and-mortar stores for service</li>
 	<li>No unlimited talk options</li>
</ul>
<table>
<thead>
<tr>
<th><strong>Provider</strong></th>
<th><strong>Monthly Cost</strong></th>
<th><strong>Options</strong></th>
<th><strong>Notes</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>IIJmio</strong></td>
<td>Data Only 3GB: ¥900; Data 3GB &amp; SMS: ¥900~1040; Data &amp; SMS &amp; Calling: ¥1600</td>
<td><strong>Unlimited domestic calls</strong> up to 3 mins for ¥600+tax/mo or 5 mins for ¥830+tax/mo using a special app in Japanese or entering <q>0037-691</q> in front of every number you dial.

<strong>Family Sharing</strong> is available by simply adding up to 3 add'l SIM cards to the plan at ¥450 each/mo.</td>
<td>IIJmio SIMs can be purchased and contracted at select Bic Camera, Yamada Denki and Yodobashi Camera stores and Aeon Shopping malls. Can be recharged using cash by buying cards at most convenience stores. Contracts are short at only 6 months.</td>
</tr>
<tr>
<td><strong>OCN</strong></td>
<td>Data Only 3GB: ¥900; Data 3GB &amp; SMS: ¥1020; Data &amp; SMS &amp; Calling: ¥1600</td>
<td><strong>Family-Share</strong>: SIMs can be added to use the same data for ¥1,100+tax per additional SIM.

<strong>English Customer Service</strong>: 0120-506-506 (press 8 once you hear the guidance voice)</td>
<td>SIMs can be purchased and contracted at select Yodobashi Camera,Yamada Denki and BIC Camera stores.</td>
</tr>
<tr>
<td><strong>Biglobe</strong></td>
<td>Data Only 3GB: ¥900; Data 3GB &amp; SMS: ¥1020; Data 1GB &amp; SMS &amp; Calling: ¥1020</td>
<td><strong>Entertainment Free Plan: </strong>Includes unlimited media app use.</td>
<td>Unused data rolls over into the next month on larger data plans.

Additional SIM cards to share plan features are ¥200~¥900 and very easy to add on.</td>
</tr>
<tr>
<td><strong>LINE Mobile</strong></td>
<td>Data Only 1GB ¥500; Data 1GB &amp; SMS ¥620; Data 1GB &amp; SMS &amp; Voice ¥1200

&nbsp;</td>
<td><strong>Communication Free Plan: </strong>Includes unlimited SNS app use.

<strong>10 minute free calls:</strong> All calls made under 10 minutes are not charged.

&nbsp;</td>
<td>Can use the LINE app on your phone to order service or see what stores in your area can help you get set up.

All account management is done through LINE app.

Can earn and use LINE points to pay for many other services; Can use LINE Cash and Pay to pay service.</td>
</tr>
</tbody>
</table>
<ul>
 	<li>Data cap is for high-speed data. If you go over the cap, in general your speed is just reduced to 200kbps until the next monthly cycle.</li>
 	<li>Every effort has been taken for accuracy; however, please contact the companies directly for confirmed/updated costs.</li>
 	<li>For SIM-only contracts, please check to make sure your device is unlocked and supported by the provider in advance.</li>
</ul>
<strong>Things to bring when making a mobile phone service contract:</strong>
<ul>
 	<li>Residence Card (the visa term generally must be longer than the contract term to pass screening)</li>
 	<li>Passport</li>
 	<li>Credit Card (Japanese bank cash cards can sometimes be presented instead, but often stores will ask for a credit card to register with the account – normally the method of payment can be changed to bank withdrawal later on)</li>
 	<li>Mobile Device, if using an existing device which is SIM-free/unlocked and supported by the Japanese carrier</li>
</ul>
<strong><em>Relo Japan does not endorse any companies/services listed. Please check terms with each company for accuracy.</em></strong>