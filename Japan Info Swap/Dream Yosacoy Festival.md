# Dream Yosacoy Festival

Yosakoi dancing started in Kochi in Shikoku around 1954 as a way to celebrate postwar reconstruction, building spirit and teamwork. Blending traditional Japanese dance and costume motif with modern music, thumping chants,  contemporary fashion, hairstyles, it’s known throughout Japan as one of the most energetic sights to behold at any given Japanese festival.

Whether you’ve never seen it before or you can’t get enough of it, you should check out the [Dream Yosacoy Festival](http://www.dreamyosacoy.jp/english/) in Odaiba the weekend of November 1st.  The year’s festival is poised to attract upwards of 800,000 spectators to watch around 6,000 dancers split into over 70 teams. Usually Yosakoi dancing is associated with summer festivals occurring in the warmer months, but this being a Being one of the largest festivals of its kind here, there are three venues around central Tokyo. In addition there is a sizable food court showcasing international foods as well as musical performances.

### Dates and Access:
**Times:** 
- November 1, Friday 17:00 to 21:00 （only music stage in front of [Daiba station](https://maps.apple.com/?address=Daiba,%20Minato-Ku,%20Tokyo,%20Japan&auid=14602167002978951369&ll=35.625925,139.771448&lsp=9902&q=Daiba%20Station&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBgKBAgKEAASJCkG/DPjANBBQDFiaXKmpnhhQDlSwG7CO9BBQEFW+tLBuHhhQA%3D%3D&t=r)）
- November 2, Saturday 11:00 to 21:00
- November 3, Sunday 10:00 to 21:00
- *each venue will have different operating times within these hours— please check below*
**Venues:**
- Tokyo Odaiba area／In front of [ Yurikamome Line Daiba station](https://maps.apple.com/?address=Daiba,%20Minato-Ku,%20Tokyo,%20Japan&auid=14602167002978951369&ll=35.625925,139.771448&lsp=9902&q=Daiba%20Station&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBgKBAgKEAASJCkG/DPjANBBQDFiaXKmpnhhQDlSwG7CO9BBQEFW+tLBuHhhQA%3D%3D&t=r)
- Tokyo Marunouchi area／[between Marunouchi Building and Shin Marunouchi Building in front of Tokyo station](https://maps.apple.com/?q=35.681601,139.765284&sll=35.681601,139.765284&sspn=0.003171,0.004870&t=r) November 3, 11:00 to 13:30 
- [Akihabara UDX building](https://maps.apple.com/?q=35.699111,139.772548&sll=35.699111,139.772548&sspn=0.001617,0.003132&t=r) (In front of Akihabara station); November 3 ,14:30 to 15:54 will held
- [Tokyo Tower (In front of entrance)](https://maps.apple.com/?address=2-8,%20Shibakoen%204-Ch%C5%8Dme,%20Minato,%20Tokyo,%20Japan%20105-0011&auid=11483186979552438639&ll=35.658558,139.745504&lsp=9902&q=Tokyo%20Tower&_ext=CiQKBAgEEAoKBAgFEAMKBAgGEHMKBAgKEA4KBAgQEAEKBAgvEEoSJikxWdxwutNBQDHJAoGNrXdhQDmvLgLN4NRBQEHx898fCHhhQFAE&t=r) November 2 ,11:30 to 12:00.