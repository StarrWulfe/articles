# Day Trips by Train in Kanto

Edition: Oct 2020
Last Edited: Oct 28, 2020 10:57 AM
Status: Published

When talking about trains in Japan, people either think of the Shinkansen high-speed rail lines linking Japan's major cities or the commuter lines in major cities, famous for their ease-of-use and convenience, if not for their sometimes crush load capacity. But there's also one more class of service that slots in-between these two train types; the Limited Express class trains that serve more destinations, not on the bullet train network, but are still way faster than the bus, and have many different amenities and services aboard. They make getting out of Central Tokyo for a day trip easy and economical too.

### South towards Mt Fuji, Hakone & Izu

![Day%20Trips%20by%20Train%20in%20Kanto%20a17633f78d6e4b1eb972931abae2b8cb/1024px-Saro_E233-3005.jpg](Day%20Trips%20by%20Train%20in%20Kanto%20a17633f78d6e4b1eb972931abae2b8cb/1024px-Saro_E233-3005.jpg)

If you're heading towards Izu, then your best bet is to ride the *[Green Cars](https://www.youtube.com/watch?v=uW8Fy6dbSdw)* on the JR Tokaido or Shonan-Shinjuku Lines south to Atami. These are the double-decker cars found in the middle of the normal trains and have a reserved seating surcharge, but the benefit of having a seat and snack service aboard a usually very crowded train. Once at Atami, change to the Ito line and head to Ito city. If going further down the peninsula, then hop onto the [Izukyu Railway](http://www.izukyu.jp/foreign_language/en/04.html) and head towards Shimoda or wherever your final destination may take you. 

Going towards Mt Fuji and Hakone gives a few options, but the most picturesque is via [Odakyu Railway's Romance Car](https://www.odakyu.jp/english/romancecar/) limited express trains leaving from [Shinjuku station](https://goo.gl/maps/vhRUGzbE2m22Z29L7). If you're really lucky, you can book seats in the front or rearmost car and have the same view usually reserved for just the operator in a normal train, thanks to the vista view windows at the end.

![Day%20Trips%20by%20Train%20in%20Kanto%20a17633f78d6e4b1eb972931abae2b8cb/odakyu_romance_car.jpg](Day%20Trips%20by%20Train%20in%20Kanto%20a17633f78d6e4b1eb972931abae2b8cb/odakyu_romance_car.jpg)

### West towards Chichibu & Nagano

![Day%20Trips%20by%20Train%20in%20Kanto%20a17633f78d6e4b1eb972931abae2b8cb/Seibu_001_series.jpg](Day%20Trips%20by%20Train%20in%20Kanto%20a17633f78d6e4b1eb972931abae2b8cb/Seibu_001_series.jpg)

Seibu operates limited express service to Chichibu from its Ikebukuro terminal using ["LaView" trains](https://www.seiburailway.jp/railways/laview-blueribbon/?utm_source=official&utm_medium=banner) that look like they came straight from the year 3000. Big ginormous windows from floor to ceiling to take in the view and a shiny stainless steel body with no hint of a paint job ensures you'll know exactly which one to catch when it's time to depart.

If heading to Nagano, then [JR's Azusa service](https://en.wikipedia.org/wiki/Azusa_(train)) from [Tokyo](https://en.wikipedia.org/wiki/Tokyo_Station) and [Shinjuku](https://en.wikipedia.org/wiki/Shinjuku_Station) stations to [Matsumoto](https://en.wikipedia.org/wiki/Matsumoto_Station) is a cheaper alternative to the Shinkansen. It's more picturesque too since unlike the many tunnels of the bullet train service, you'll be treated to many mountainous views of Japan's [Central Alps](https://en.wikipedia.org/wiki/Japanese_Alps). 

### From Subway to Getaway

You don't have to even travel far to a separate station in some cases to enjoy riding the rails to a far-flung daytrip because some of the routes actually begin their journeys in Tokyo's subway system. The [S-Train](https://en.wikipedia.org/wiki/S-Train_%28Seibu%29) on weekends cruises along the Fukutoshin subway line on the weekends and offers the fastest service to Yokohama's Minato-Mirai and Chinatown areas in the south or the Chichibu nature area in the western mountains. You can also pick up a few of the Odakyu Railway's "Romancecar" trains on the Chiyoda Line subway and head towards Gotemba, Hakone, or Enoshima.

![Day%20Trips%20by%20Train%20in%20Kanto%20a17633f78d6e4b1eb972931abae2b8cb/Keio_Liner_JLG.jpg](Day%20Trips%20by%20Train%20in%20Kanto%20a17633f78d6e4b1eb972931abae2b8cb/Keio_Liner_JLG.jpg)

Images:

Keio Liner at Shinjuku Station by [Jason L Gatewood](http://jlgatewood.com), own work

E233 via Wikipedia, [Toshinori Baba](https://commons.wikimedia.org/wiki/User:Toshinori_baba) , [CC BY SA 3.0](https://creativecommons.org/licenses/by-sa/3.0)

Romance Car via Wikipedia, [Cfktj1596](https://commons.wikimedia.org/wiki/User:Cfktj1596), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)

江戸村のとくぞう, [Seibu 001 series 20190703a3](https://commons.wikimedia.org/wiki/File:Seibu_001_series_20190703a3.jpg), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)