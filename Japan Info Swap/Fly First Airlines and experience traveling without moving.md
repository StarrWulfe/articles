# Fly First Airlines and experience traveling without moving
 The are no limits to the amount of themed cafes that are available to you if you want a unique dining experience in the Metropolis. Wanna hang out with owls, sure. How about ninjas? Ditto. Vampire maids, yeah we got that. Full blown psychedelic anime-themed robots piloted by buxom ladies? We wrote the book on that one. But what about an airline-themed restaurant? Well we got one of those now too. Welcome to [First Airlines](http://firstairlines.jp), “flying” nonstop between Ikebukuro and 5 world cities...and even back in time everyday! 

If you’re reading this article then you certainly know what it’s like to fly internationally already, and unless you’re part of the “Gold Mileage Club” then your experience is one of having to sit for hours on end in an increasingly not-so-comfortable seat until you figure out a way to get to sleep or the in-flight movies or pre-selected Netflix cue/video games/work that still needs to be completed distracts enough. First Airlines aims to get rid of that part of the experience and concentrate on what makes us board the plane in the first place: the destination. Using a combination of cosplay, set design, authentic cuisine and virtual reality, First Airlines is able to take you from their Ikebukuro based terminal to either Rome, Paris, New York City, Helsinki or Hawaii. 

Just like a real airline, you must book a flight...err....make a reservation... Unlike a real airline, you don’t need to go through security or be there 2 hours beforehand; 20 minutes is enough. Boarding is just like being at Narita or Haneda minus the long walks, lugging around bags, and showing your passport every 4 seconds. The cabin crew will show you to your seat, and yes everything they do is done in earnest; most of the staff are either training to become or are former cabin attendants. 

Everything from “takeoff” to “landing” is done mostly in VR, but the in-flight meal is the real deal and is based on the “destination” being visited. There’s no “chicken or beef” here either, the [head chef is from a Michelin starred restaurant.](firstairlines.jp/food.html)  Each meal is themed for the destination and is the highlight of the experience. Expect 4 courses in all, with the “return flight” being the dessert course.

Once at the destination, the VR headsets stay on where you are taken on a virtual tour of the destination by a tour guide and even “talk” to locals through a level of interactivity in the system. 

On the “return” you are given the opportunity to do some in-flight shopping, with items selected from the itinerary as well. 

Book your flight now at www.firstairlines.jp. It’s also the world’s first delay-free airline as well since there are no virtual storms programmed in the experience…yet?

### Access 
[First Airlines](http://firstairlines.jp)
Ikebukuro Parkheim West, 8F
Nishiikebukuro 3 Chome-31-5 , Toshima-ku, Tōkyō-to 171-0021
[Map](https://maps.google.com/maps?ll=35.730954,139.705602&z=16&t=m&hl=en-US&gl=JP&mapclient=embed&q=%E6%B1%A0%E8%A2%8B%E3%83%91%E3%83%BC%E3%82%AF%E3%83%8F%E3%82%A4%E3%83%A0%E3%82%A6%E3%82%A8%E3%82%B9%E3%83%88%20%E3%80%92171-0021%20T%C5%8Dky%C5%8D-to%2C%20Toshima-ku%2C%20Nishiikebukuro%2C%203%20Chome%E2%88%9231%E2%88%925)
5 minutes from Ikebukuro Station (JR, Tokyo Metro, Tobu, Seibu lines)

Weekdays: 17: 30 ~ 23: 00
Saturdays, Sundays, holidays: 12: 30-23: 00
By appointment only! 
info@firstairlines.jp
