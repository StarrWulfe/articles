#The Unko Museum is anything but crappy

We knew it was bound to happen eventually; Japan is home to the first restroom-themed restaurant, our toilet technology is talked about and adored far and wide, and we even invented the poop 💩 emoji. So of course Japan should be the first to have a museum dedicated to all things doo-doo. If you're hankering to know all about Mr Hanky, then head over to the [Unko Museum](https://ale-box.com/unkomuseum/) in Yokohama and brace yourself!

Located next to Yokohama's East exit in ASOBUILD, a new entertainment complex the was used to be part of the large post office building, the Unko Museum kicks off what will be a long line of interactive exhibitions in the space. As you can probably guess, *unko* means "poop" in Japanese, but don't worry because you'll never forget it after leaving here!

### Un-teractive Un-telligence
The museum offers plenty of hands on exhibits and spaces to learn about and, yes, even play with digital poo. Learn about how humanity has dealt with cleaning up after its #2s since the beginnings of time. There's also an area where you can learn about unko in modern times -- poop pop culture --  and see everything from books to toys that are inspired by or deal with dookey as an art form. 

### Un-stagrammable
A large part of the venue is dedicated to creating spaces where you can "get it on the 'gram" and show your friends you've spent a totally crappy day at the Unko Museum. From floating pastel swirly poops to interactive floaties in a giant toilet, you can impress/gross out your followers with your own take on the exhibit.

###Smooth Move
The temporary interactive excrement exhibition is only around until July 15 2019, but the venue at ASOBUILD plans on hosting many more pop-up (poop-out?) attractions to come. Currently the complex is also playing host to Japan’s largest handmade-experience providing crafting workshops for pottery, leather accessories and more and also a VR exhibit of Akihiro Nishino’s popular illustrated storybook Poupelle of Chimney Town. 

###Access
[Takashima 2-14-9, Nishi-ku, Yokohama, Kanagawa ](https://maps.apple.com/?daddr=(35.464434,%20139.624705)&dirflg=d)
[Yohohama Station](https://maps.apple.com/?daddr=(35.464434,%20139.624705)&dirflg=d)
Open Weekdays 11am-8pm, Weekends 11am-8pm (last entry 30 minutes before close)
Tickets: ¥1,600, Kids 6-12 ¥900, 5 and younger are free.