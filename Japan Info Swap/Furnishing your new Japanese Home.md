# Furnishing your new Japanese Home
Though the chances of finding your new home in Japan thoroughly barren of even light fixtures and nothing short of [four walls, floor and ceiling](https://resources.realestate.co.jp/wp-content/uploads/2017/05/Japanese-Apartment-360-Virtual-Walkthrough-Tour-Video-1020x500.png) are slim these days, most standard apartments and other rental spaces don’t come with standard appliances like stoves, refrigerators and HVAC units, things that even the most spartan of apartment rentals in places like the US always have and leave most foreigners wondering how to find most of this stuff when they first move in. Luckily there are services like [H&R Group](http://japaninfoswap.com/who-is-hr-group-japan/) that take the hassle out of finding furnished apartments so you don’t have to deal with that aspect of it, but what about those other “only in Japan” items that you’ll have to deal with sooner or later in your home? 

![](Furnishing%20your%20new%20Japanese%20Home//japaninfoswap.com/wp-content/uploads/2018/07/drain-and-foil-balls-300x274.jpg)
### Everything IN the kitchen sink

Instead of an array of steel gears and teeth ready to chew up whatever entered its maw, most kitchen sinks have a [small metal or plastic wastebasket](https://www.youtube.com/watch?v=GrKD6f3aqbY) wedged into the drain. Of course you’d better empty its contents whenever you wash dishes or otherwise it will create the moldiest gag-inducing scene when you take it apart once it clogs up. But we can do better than this.  

1. Use [a few small pieces of aluminum foil balled up](http://www.tokyofamilies.net/2015/06/japanese-life-hack-the-most-time-efficient-way-of-cleaning-your-kitchen-drain/) and toss into the strainer to prevent mold and break down food you can’t stop. The aluminum generates ions that kill organics and keep slime and mold from developing.
2. Use the small stocking nets over the drain basket. That way you can simply pull the whole thing out and toss it and not worry about cleaning the assembly every time.
3. Get a separate basket that sits ~in~ the sink and let that catch the bits of food off plates and utensils.
All these items are found at your local ¥100 shop

### One Coin Outfitting

Speaking of the ¥100 shops, most things you need in the kitchen and dining area of your new digs can be had there. I’m a big fan of IKEA and Nittori, but a [plate](https://www.nitori-net.jp/store/SearchDisplay?categoryId=&amp;storeId=10001&amp;catalogId=10001&amp;langId=-10&amp;sType=SimpleSearch&amp;resultCatEntryType=2&amp;showResultsPage=true&amp;searchSource=Q&amp;pageView=&amp;beginIndex=0&amp;pageSize=12&amp;freeword=true&amp;catgroup_id=&amp;searchTerm=お皿) is a [plate](https://www.ikea.com/us/en/catalog/categories/departments/eating/18861/) is a [plate](http://howtominute.com/wp-content/uploads/2015/08/Screen-Shot-2015-08-30-at-9.09.32-AM.png), right? Also remember Japanese kitchens are sometimes literally just a nook in-between the front door and the main room; no need to go all out on that [Muji designer dish set](https://www.muji.net/store/cmdty/detail/4945247193947?searchno=6) if there’s no place to store the other 4 pieces! 

![](Furnishing%20your%20new%20Japanese%20Home//japaninfoswap.com/wp-content/uploads/2018/07/hanging-laundry-300x225.jpg)
### Just Hanging Out

Another part of domestic life in Japan that surprises many: for such a technological innovator in many sectors including home appliances, most Japanese don’t have many of the conveniences they produce, like clothes dryers. Check any residential neighborhood on a dry day and there’ll be a parade of the family’s wardrobe drying in the sun off many a balcony. A closer look shows a variety of fiberglass bars, racks, clothespin hangers and other assorted gear that the average person needs to get their threads dry after a spin in the washing machine. Again, most of these things can be found at a ¥100 store, but those big racks and bars call for a trip to the nearest DIY home center or a trip online to Rakuten or Amazon to find them. In the past, I have been lucky by simply asking for some of this stuff from the landlord at move in— don’t hesitate to ask!

### Tis the season

You’re bound to hear the phrase, “Japan enjoys four distinct seasons” sooner or later, sometimes as if it’s the only location on Earth with that pattern. Nonetheless, there are many incidentals you may want to have on hand as time passes through the year.

1. **Ground Sheet.** You’ll need this whenever doing any kind of outdoor activity, from spring hanami, summer beach trips, all the way to early fall fireworks.
2. **Mosquito Coils.** Many a person’s Japan summer memory banks are triggered by the smell of “ [katori senko (蚊取り線香)](https://en.wikipedia.org/wiki/Mosquito_coil) ”. These green spiral shaped incense packs have been around since the late 19th century since they were invented here. Typically they are burned outside on balconies, verandas, and terraces near where doors and windows are used. Whatever you do, don’t use it indoors… Not because of the fire risk, but really because the smoke and smell are horrible and will make your room impossible to be in for 2 days, not that I would have any experience with that… Instead, get one of the battery powered “bug-b-gawn” devices.
3. **Electric Carpet/Blanket/Kotatsu.** Unless you’re way north in Tohoku or Hokkaido, you’re going to have a drafty and chilly time during the winter. Better head over to [Don Quixote](http://donki.jp) and get [one (or all) of these](https://en.wikipedia.org/wiki/Kotatsu) to stay as toasty as possible!
- - - -

  — By [Jason L. Gatewood](http://jlgatewood.com) *Images:* “ [moving](https://www.flickr.com/photos/miss_pupik/106699669/)” ( [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) ) by [miss pupik](https://www.flickr.com/people/miss_pupik/) ["drain and foll ball"](http://tokyofamilies.net/wp-content/uploads/2015/06/IMGP0835-1024x934.jpg) by [Tokyo Families](http://www.tokyofamilies.net) is licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0) [“Hanging Laundry”](https://www.freeimages.com/photo/laundry-1483899) by [kyran yeomans](https://www.freeimages.com/photographer/pihstekcor-34516), [Getty Free Images](https://www.freeimages.com) is licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0)

