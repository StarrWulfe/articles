# How to go vegan in Tokyo
![](How%20to%20go%20vegan%20in%20Tokyo/Kawasaki%20farming.jpg)
The choices for dining out in Tokyo are limitless. You can literally eat your way around the world and dine on almost anything that humans consider edible (even the most questionable) here in Japan’s capital. One type of cuisine that has seen a recent boom is that of vegetarian and vegan restaurants. Although many establishments offer/meatless/ options, they may still include things made from egg and milk in the ingredients which may not agree with some diets. However it’s just a matter of becoming informed; you can even find many vegan choices in just about any convenience store if you know what to look for. 
![](How%20to%20go%20vegan%20in%20Tokyo/IMG_0484.PNG)

### There’s an app for that
Chances are you’re already used to tapping through your smartphone to figure out dining options in your home country. Many of the apps you’re already using likely will have Tokyo area restaurants listed. Specialized apps like [Happy Cow](https://itunes.apple.com/us/app/happycow-vegan-vegetarian/id435871950?mt=8&at=1010l9pz) have you covered. My main weapon when showing my veggie munching friends around town though is simply [Google Maps](http://maps.google.com) . Because of crowd sourcing, you can find many places that don’t even advertise much in Japanese, let alone English. If you’re using an iPhone, [Apple Maps](http://maps.apple.com) sources its locations and reviews from Japanese restaurant listing apps like [Tabelog](www.tabelog.jp) and [Hot Pepper](www.hotpepper.jp) in addition to others. 

### Remember the Basics
Historical gastronomists know Japan’s diet was mainly macrobiotic and vegetable based in the times before modern conveniences like refrigeration, mass fish farms and open sea trade. Even meat from fish, boar and other game was considered a rare treat for the average Japanese due to both being restricted religiously (Bhuddists) and simply being poor.  The diet mainly consisted of rice, seaweed, radishes, squash, and other savory greens. Before refrigeration, salting and pickling were the main methods used to keep food fresh. Fast forward to the modern era, and many of these same foods are still eaten as a part of Japanese cuisine. Tsukemono, various pickled vegetables that are eaten with a bowl of rice can be eaten on-the-go as onigiri, commonly found in in a triangular form wrapped in seaweed. Meats such as tuna and salmon are usually found in the middle but pickled plum (umeboshi), fermented soybean (natto), and _________. Another staple in the Japanese diet: soy. Believe it or not, Starbucks had the soy latte option here in Tokyo before you could find it in many places in the States! Soy milk (tonyu), ice cream and donuts are a few common foods found in most stores.

### Dining out
Thanks to increasing inbound tourists to Japan with varied dietary needs along with the Japanese themselves wanting new dining choices, there are lots of places to pig out, sans pork. beef…or any other meat. Here are some of the ones I’ve tried myself over the years:

#### T’s Restaurant /T’s Tantan
The first time one of my friends visiting Tokyo hit me with the “by the way, I gave up eating meat, so can you help me eat out in Japan” conundrum, I found out about this place.  The main shop in Jiyugaoka is well known for its spin on tan-tan men, curry and gyoza dumplings, swapping out soy and veggies for the usual meat that goes in their preparation. The location includes a store where you can pickup some of their dishes in a heat-and-eat version to-go.
 ![](How%20to%20go%20vegan%20in%20Tokyo/main_tantan.png)
T’s Tantan locations also serves a small subset of their menu, focusing on their tasty spin on noodle soups. You can find them in both Tokyo and Ueno JR stations. 
*Address:* [2-9-6 Jiyugaoka, Meguro, Tokyo 152-0035](https://goo.gl/maps/GVKgiZiUqCH2)
*Access:* Jiyugaoka Station, Tokyu Toyoko, Oimachi Lines [TY07] [OM10]
*Hours:* 	Everyday 11am~10pm
*Website:* [ts-restaurant.jp](http://ts-restaurant.jp)
*Phone:* 03-3717-0831

#### Vegan Cafe
This very appropriately named cafe located in the Hiroō district does exactly what it says on the tin: serve some of the best vegan spins on Japanese and other world cuisine. I was actually headed to the McDonald’s nearby when I spotted their signboard advertising curry, and decided to give it a try. I was not disappointed! There’s a loco-moco dish and a vegan take on sushi that will create confusion between tongue and brain as well. 
*Address:* [4-5-65 Minamiazabu, Minato-ku, Tōkyō-to 106-0047](https://goo.gl/maps/qrmyy4RYnNS2)
*Access:* Hiroō Station, Tokyo Metro Hibiya line [H03]
*Hours:* Everyday 11:30am~9pm
*Website:* [vegan-cafe.jp](http://www.vegan-cafe.jp/)
*Phone:* 03-6450-3020

#### AIN SOPH.journey/Heavenly Pancake
My day job as a university coordinator and teacher means I come into contact with a lot of young people, and they all usually have part-time jobs somewhere in town; one of them works (worked?) here. Somehow I was able to remember that the next time I had a vegetarian in the party and needed to find a spot close to Shinjuku station. This place serves some great sandwich wraps and other usual vegan fare, but the PANCAKES…I can’t believe there’s no dairy involved. Even though I’m not a vegetarian, I kinda made it my breakfast spot more than a few times.
*Address:* [3-8-9 Shinjuku, Shinjuku-ku, Tokyo 160-0022](https://www.google.com/maps/place/Ain+Soph.+Journey+Shinjuku/@35.6906917,139.7048202,17z/data=!3m1!4b1!4m8!1m2!2m1!1svegan+restaurants!3m4!1s0x0:0xbfb9290fe024dc7d!8m2!3d35.6906877!4d139.706674)
*Access:* Shinjuku 3-Chome Station, Tokyo Metro Fukutoshin, Marunouchi Lines, Toei Shinjuku Line. [F13][M09][S02] 
*Website:* [ain-sophjourney.business.site](https://ain-sophjourney.business.site/)
*Phone:* 03-5925-8908

- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
/Images:/
# How to go vegan in Tokyo

![](How%20to%20go%20vegan%20in%20Tokyo/Kawasaki%2520farming.jpg)
The choices for dining out in Tokyo are limitless. You can literally eat your way around the world and dine on almost anything that humans consider edible (even the most questionable) here in Japan’s capital. One type of cuisine that has seen a recent boom is that of vegetarian and vegan restaurants. Although many establishments offer/meatless/ options, they may still include things made from egg and milk in the ingredients which may not agree with some diets. However it’s just a matter of becoming informed; you can even find many vegan choices in just about any convenience store if you know what to look for.

![](How%20to%20go%20vegan%20in%20Tokyo/IMG_0484.PNG)
### There’s an app for that

Chances are you’re already used to tapping through your smartphone to figure out dining options in your home country. Many of the apps you’re already using likely will have Tokyo area restaurants listed. Specialized apps like [Happy Cow](https://itunes.apple.com/us/app/happycow-vegan-vegetarian/id435871950?mt=8&at=1010l9pz) have you covered. My main weapon when showing my veggie munching friends around town though is simply [Google Maps](http://maps.google.com) . Because of crowd sourcing, you can find many places that don’t even advertise much in Japanese, let alone English. If you’re using an iPhone, [Apple Maps](http://maps.apple.com) sources its locations and reviews from Japanese restaurant listing apps like [Tabelog](www.tabelog.jp) and [Hot Pepper](www.hotpepper.jp) in addition to others. 

### Remember the Basics

Historical gastronomists know Japan’s diet was mainly macrobiotic and vegetable based in the times before modern conveniences like refrigeration, mass fish farms and open sea trade. Even meat from fish, boar and other game was considered a rare treat for the average Japanese due to both being restricted religiously (Bhuddists) and simply being poor. The diet mainly consisted of rice, seaweed, radishes, squash, and other savory greens. Before refrigeration, salting and pickling were the main methods used to keep food fresh. Fast forward to the modern era, and many of these same foods are still eaten as a part of Japanese cuisine. Tsukemono, various pickled vegetables that are eaten with a bowl of rice can be eaten on-the-go as onigiri, commonly found in in a triangular form wrapped in seaweed. Meats such as tuna and salmon are usually found in the middle but pickled plum (umeboshi), fermented soybean (natto), and _. Another staple in the Japanese diet: soy. Believe it or not, Starbucks had the soy latte option here in Tokyo before you could find it in many places in the States! Soy milk (tonyu), ice cream and donuts are a few common foods found in most stores.

### Dining out

Thanks to increasing inbound tourists to Japan with varied dietary needs along with the Japanese themselves wanting new dining choices, there are lots of places to pig out, sans pork. beef…or any other meat. Here are some of the ones I’ve tried myself over the years:

#### T’s Restaurant /T’s Tantan

The first time one of my friends visiting Tokyo hit me with the “by the way, I gave up eating meat, so can you help me eat out in Japan” conundrum, I found out about this place. The main shop in Jiyugaoka is well known for its spin on tan-tan men, curry and gyoza dumplings, swapping out soy and veggies for the usual meat that goes in their preparation. The location includes a store where you can pickup some of their dishes in a heat-and-eat version to-go.

![](How%20to%20go%20vegan%20in%20Tokyo/main_tantan.png)
T’s Tantan locations also serves a small subset of their menu, focusing on their tasty spin on noodle soups. You can find them in both Tokyo and Ueno JR stations.

*Address:* [2-9-6 Jiyugaoka, Meguro, Tokyo 152-0035](https://goo.gl/maps/GVKgiZiUqCH2)

*Access:* Jiyugaoka Station, Tokyu Toyoko, Oimachi Lines [TY07] [OM10]

*Hours:* Everyday 11am~10pm

*Website:* [ts-restaurant.jp](http://ts-restaurant.jp)

*Phone:* 03-3717-0831

#### Vegan Cafe

This very appropriately named cafe located in the Hiroō district does exactly what it says on the tin: serve some of the best vegan spins on Japanese and other world cuisine. I was actually headed to the McDonald’s nearby when I spotted their signboard advertising curry, and decided to give it a try. I was not disappointed! There’s a loco-moco dish and a vegan take on sushi that will create confusion between tongue and brain as well.

*Address:* [4-5-65 Minamiazabu, Minato-ku, Tōkyō-to 106-0047](https://goo.gl/maps/qrmyy4RYnNS2)

*Access:* Hiroō Station, Tokyo Metro Hibiya line [H03]

*Hours:* Everyday 11:30am~9pm

*Website:* [vegan-cafe.jp](http://www.vegan-cafe.jp/)

*Phone:* 03-6450-3020

#### AIN SOPH.journey/Heavenly Pancake

My day job as a university coordinator and teacher means I come into contact with a lot of young people, and they all usually have part-time jobs somewhere in town; one of them works (worked?) here. Somehow I was able to remember that the next time I had a vegetarian in the party and needed to find a spot close to Shinjuku station. This place serves some great sandwich wraps and other usual vegan fare, but the PANCAKES…I can’t believe there’s no dairy involved. Even though I’m not a vegetarian, I kinda made it my breakfast spot more than a few times.

*Address:* [3-8-9 Shinjuku, Shinjuku-ku, Tokyo 160-0022](https://www.google.com/maps/place/Ain+Soph.+Journey+Shinjuku/@35.6906917,139.7048202,17z/data=!3m1!4b1!4m8!1m2!2m1!1svegan+restaurants!3m4!1s0x0:0xbfb9290fe024dc7d!8m2!3d35.6906877!4d139.706674)

*Access:* Shinjuku 3-Chome Station, Tokyo Metro Fukutoshin, Marunouchi Lines, Toei Shinjuku Line. [F13][M09][S02] 

*Website:* [ain-sophjourney.business.site](https://ain-sophjourney.business.site/)

*Phone:* 03-5925-8908

- - - -

— By [Jason L. Gatewood](http://jlgatewood.com)

*Images:*

[Kawasaki Farm](https://photos.google.com/share/AF1QipNUyz6OtMCb09aQFcTq4lNR5GtPIapXaiELodE8Xe1heDoo9Tb7EQ88BSnxK__Xhg/photo/AF1QipOe7mhJTZ0fg-ft9RX-AqPqh757W2QM_zXg2ik_?key=WG9yM1k0bnAtS2JreE9FNFhqRFp3Z0xVMk5jMUFn) by [Jason L. Gatewood](http://jlgatewood.com)

[Apple Maps Screengrab](http://maps.apple.com) 

[T’s Restaurant Website](http://ts-restaurant.jp)
