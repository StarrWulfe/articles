# Best Picnic Spots in Tokyo
With the weather becoming a lot nicer, many people are ready to get out and enjoy the warm weather, and having a picnic is a nice cheap way to get out into the city and enjoy it. I know a few places from small squares to riverside glens that are good to have an afternoon picnic. 

### Large Parks
It goes without saying that Tokyo’s large mega parks are perfect spots for getting your outdoor on-nom-nom on.  

**Yoyogi Park**
[Yoyogikamizonocho, Shibuya-Ku, Tokyo](https://maps.apple.com/?address=Yoyogikamizonocho,%20Shibuya-Ku,%20Tokyo,%20Japan%20151-0052&auid=14712655071368276843&ll=35.671510,139.694870&lsp=9902&q=Yoyogi%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAISJilOWU3IadVBQDFeo8fO9XVhQDnMLnMkkNZBQEFOVPlkUHZhQFAE&t=r)

**Ueno Park**
[Uenokoen, Taito, Tokyo](https://maps.apple.com/?address=Uenokoen,%20Taito,%20Tokyo,%20Japan%20110-0007&auid=15689944535114600587&ll=35.715333,139.773409&lsp=9902&q=Ueno%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAISJikMJx9i1NpBQDGtLhjEmHhhQDmK/ES++ttBQEEFf5hm83hhQFAE&t=r)

**Shinjuku Gyoen**
[Naitocho 11, Shinjuku, Tokyo](https://maps.apple.com/?address=11,%20Naitocho,%20Shinjuku,%20Tokyo,%20Japan%20160-0014&auid=9687342960014594349&ll=35.684751,139.710002&lsp=9902&q=Shinjuku%20Gyoen&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAISJim4zERdYddBQDEU7j1rkHZhQDk2omq5h9hBQEE2GOcF63ZhQFAE&t=r)

### Riverbanks
Tokyo also has a nice abundance of small and big rivers that almost always have a green belt esplanade doubling as a park providing a nice respite close to home.

**Sumida Park**
 [Taito-Ku, Tokyo](https://maps.apple.com/?address=Sumida%20Park,%20Taito-Ku,%20Tokyo,%20Japan%20111-0032&auid=9551504958124187707&ll=35.715559,139.802935&lsp=9902&q=Sumida%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJCmTWK2YB9tBQDGVdyjnfnlhQDkRLtP0LdxBQEHlRR2K2XlhQA%3D%3D)
 
 **Tsukuda Park**
[Tsukuda 1-1-18, Chuo-Ku, Tokyo](https://maps.apple.com/?address=1-18,%20Tsukuda%201Chome,%20Chuo-Ku,%20Tokyo,%20Japan%20104-0051&auid=1031336123744046262&ll=35.668913,139.782523&lsp=9902&q=Tsukuda%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAASJik5Zo/3+9RBQDHrDAht3HhhQDm3O7VTItZBQEFNgEACN3lhQFAE)

**Hyogojima Park**
[Tamagawa 3-2-1, Setagaya, Tokyo](https://maps.apple.com/?address=2-1,%20Tamagawa%203-Ch%C5%8Dme,%20Setagaya,%20Tokyo,%20Japan%20158-0094&auid=7962252354010205504&ll=35.611685,139.623881&lsp=9902&q=Hyogojima%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJilmq9xGr81BQDGUv1EUzXNhQDnkgAKj1c5BQEHuRv+YJ3RhQFAE&t=r)

 **Sotobori Park** 
[Fujimi 2-9,Chiyoda-Ku, Tokyo](https://maps.apple.com/?address=9,%20Fujimi%202Chome,%20Chiyoda-Ku,%20Tokyo,%20Japan%20102-0071&auid=17795986102580696360&ll=35.696866,139.741226&lsp=9902&q=Sotobori%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAASJilzUeOFxdhBQDFSW/0vkHdhQDnxJgni69lBQEGGxM/N6ndhQFAE) 

### Odaiba
Of course, this artificial island on the edge of Tokyo Bay has plenty of green spaces and shoreline to warrant a category of its own. 

**Odaiba Seaside Park**
[Daiba 1, Minato-Ku, Tokyo](Https://maps.apple.com/?address=Daiba%201-Ch%C5%8Dme,%20Minato-Ku,%20Tokyo,%20Japan%20135-0091&auid=17052584374012580352&ll=35.630737,139.776045&lsp=9902&q=Odaiba%20Kaihin%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJCmaUd7qMMVBQDE+iYHvSHVhQDkpH0LCQ9xBQEHGJeLHYXxhQA%3D%3D)

**Shiokaze Park**
[Higashiyashio 1, Shinagawa-Ku, Tokyo](https://maps.apple.com/?address=Daiba%20Park,%201,%20Higashiyashio,%20Shinagawa-Ku,%20Tokyo,%20Japan%20135-0092&auid=13440397664781917889&ll=35.623832,139.768935&lsp=9902&q=Shiokaze%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJike8jvjAs9BQDFYGUzNbHhhQDmcx2E/KdBBQEGOYvpUx3hhQFAE)

### Far Far Away
If you feel the need to "rough it" for a bit, these locations are 30 minutes to an hour's train ride away from central Tokyo.

**Inokashira Park**
[Goten-yama 1-18-31 Musashino-Shi, Tokyo](https://maps.apple.com/?address=18-31,%20Goten-yama%201-Ch%C5%8Dme,%20Musashino-Shi,%20Tokyo,%20Japan%20180-0005&auid=11689870936145658003&ll=35.699773,139.573252&lsp=9902&q=Inokashira%20Onshi%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJCk3bozp/9hBQDGs2BEtLHJhQDm1Q7JFJtpBQEHU92jLhnJhQA%3D%3D)

**Yamashita Park**
[Yamashitacho 279, Naka-Ku, Yokohama, Kanagawa](https://maps.apple.com/?address=279,%20Yamashitacho,%20Naka-Ku,%20Yokohama,%20Kanagawa,%20Japan%20231-0023&auid=16930410746915608086&ll=35.445900,139.649593&lsp=9902&q=Yamashita%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJikl5lk6gbhBQDE5uLPLxXRhQDmju3+Wp7lBQEEVy6QgIHVhQFAE&t=h)

**Showa Kinen Koen**
[Midoricho 3173,Tachikawa-Shi, Tokyo](https://maps.apple.com/?address=Showa%20Kinen%20Park,%203173,%20Midoricho,%20Tachikawa-Shi,%20Tokyo,%20Japan%20190-0014&auid=18161581280520586529&ll=35.711294,139.393755&lsp=9902&q=Showa%20Kinen%20Park&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAISJimKcf4XatlBQDFAwFkk9WxhQDkIRyR0kNpBQEH8PKLDT21hQFAE&t=r)
