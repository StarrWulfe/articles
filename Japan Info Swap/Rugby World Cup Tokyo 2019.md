# Rugby World Cup Tokyo 2019
#draft #JIS# #articles #jan #yr2019 

While most people around the world know the Summer Olympic Games will be here in Tokyo in 2020, we actually have very large sporting engagement to host a year before that during early autumn 2019, just over 8 months from when this will hit the interwebs. The Rugby World Cup is actually the world’s third largest sports tournament behind the Summer Games and Soccer World Cup. But hey, Japan has hosted both of those other sports before, so this should be like old hat to us (we hope) and give us a chance to test out a lot of facilities that will be also playing double duty in 2020. Not to mention giving a chance for Japan’s burgeoning rugby scene to be front and center during the tourney. Yes, that’s right, Japan has some great rugby teams and a storied past here.

### A bit about Japan’s rugby history
There are some 3000 officially sanctioned clubs in Nippon and the sport has been played since the latter half of the 19th century when a team organized itself called the Yokohama Foot Ball Club. Later on due to the influence of foreign professors, Keio University along with a host of other prominent colleges started teams and began playing each other. Even the brother of the Showa Emperor, Prince Chichibu, came to love and promote the sport, becoming the president of the Japanese Rugby Football Union. Nowadays you can catch a match between any of the 16 corporate owned teams in the [Top League](http://www.top-league.jp) who vie for the Lixil Cup every year. 

From September 20 until November 2 of 2019, twenty of the worlds best teams will challenge each other in 40 matches along with the quarters, semis, and finals matches. There will be [12 different venues around the country](https://www.rugbyworldcup.com/venues) from Hokkaido to Kyushu, with [Tokyo Stadium](https://www.rugbyworldcup.com/venues/tokyo-stadium) in Chofu and [Yokohama International Stadium](http://www.rugbyworldcup.com/venues/international-stadium-yokohama) hosting many games as well as the playoff and final matches around the Kanto Capital Region.  The [schedule is already posted](https://www.rugbyworldcup.com/match-schedule), so do give the table a look and check out where your favorite team is playing at the venue of your choice. 

### Getting Tickets
Tickets for the matches can be found only in one location: https://tickets.rugbyworldcup.com/. There are no other official means to purchase tickets at the time of this writing however in the past as with the 2002 FIFA World Cup, there were ticket agents with permission to sell seats as well as day-of tickets available at the venues themselves; it isn’t known yet whether this will be the case for the Rugby World Cup so stay tuned. 


![](Rugby%20World%20Cup%20Tokyo%202019/800px-2019_Rugby_World_Cup_(logo).svg.png)
Fair Use 

![](Rugby%20World%20Cup%20Tokyo%202019/Japan_v_Australia_A_IRB_Pac_Nations_2008_June_8.jpg)

[“Japan vs Australia IRB 2008”](https://commons.wikimedia.org/wiki/File:Japan_v_Australia_A_IRB_Pac_Nations_2008_June_8.JPG) by [Historian](https://commons.wikimedia.org/wiki/User:Historian) is licensed under [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0)
