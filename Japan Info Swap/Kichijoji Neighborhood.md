# Kichijoji Neighborhood
It’s been awhile since I last introduced a part of Tokyo you may not be so familiar with, so this time we’ll head westward from Shinjuku to visit the Kichijoji (吉祥寺)  district. This area is centered around the junction of the JR Chuo Line and Keio Inokashira Line at its namesake station, and is mostly in the city of Musashino, with a little bit bleeding over into Mitaka city as well. Long an area popular with those wishing to have access into central Tokyo (express trains on both lines whisk you to Shinjuku or Shibuya in only 20 minutes), recently it has become a boon to young adults and foreigners alike because of the nearby universities and access to affordable apartments in the area. 

### History
It all goes back to the old Edo Period practice of making the best of a bad situation… Especially if you’re rich and powerful. The old Kichijoji temple was actually located in what is now Suidobashi, the area where the Tokyo Dome stands today. There was [a big fire](https://en.wikipedia.org/wiki/Great_fire_of_Meireki), and the people living there were burned out of their homes. Instead of rebuilding them where they stood, the Shogunate government built homes intended for daimyo; the class of people that ruled over other lands outside of Edo but had to have their families live under the shogun’s nose to keep them from betrayal. The disheveled residents were working under Kichijoji land grants, so when two samurai in the clan were able to open up lands in what is now eastern Musashino city, the former residents relocated. The [Tamagawa aqueduct](https://en.wikipedia.org/wiki/Tamagawa_Aqueduct) turned the swampy grasslands into a farmer’s paradise in just a few years. Since they had an affinity for their old area, they dubbed the new lands “Kichijoji” 

### Shopping & Entertainment
Immediately to the north side of the station complex lies “Sun Road and “Harmonica”, a large collection of covered shopping streets packed with almost every chain store, restaurant, and cafe that Japan offers.  There are larger anchor stores as well, such as electronics giants Yodobashi Camera and Yamada Labi.  Don Quixote and Tokyu have branches here as well. The train station itself is one huge interconnected mall as well. There are a fare number of izakaya, cabarets, and small concert “live” houses as well, attracting many to come here to find a more intimate setting for drinks and conversation instead of Shinjuku or Shibuya.

### Parks and Museums
With [Inokashira Park](https://en.wikipedia.org/wiki/Inokashira_Park) located immediately south, Kichijoji is a fun place to enjoy the spring and summer months. The park has a huge explosion of pink sakura blossoms during Hanami season, and the lake sports paddle boats and canoes. There’s even a petting zoo and a few small cafes ion the grounds as well.  Also of note, the [Ghibli Museum](https://en.wikipedia.org/wiki/Ghibli_Museum) is a 15 minute walk from the park, and on a beautiful day, is easier to trek to than taking the shuttle bus from Mitaka station. 

Access
JR Chuo [JC] and Keio Inokashira [IN] lines to Kichijoji Station. 
[Map link](https://maps.apple.com/?address=Kichijojiminamicho,%20Musashino,%20Tokyo,%20Japan%20180-0003&auid=7450301512337552087&ll=35.703136,139.579816&lsp=9902&q=Kichij%C5%8Dji%20Station&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBgKBAgKEAASJCmMLi3v4tlBQDF0nIPJhHJhQDnY8mfOHdpBQEEGUWDplnJhQA%3D%3D&t=r)
- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
    Images: [Jason L Gatewood](h)
