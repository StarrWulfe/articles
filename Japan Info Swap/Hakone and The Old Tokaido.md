# Hakone and The Old Tokaido

Hakone is usually more synonymous with skiing or being a jumping off point for tours to Mt. Fuji but it’s actually a great place to check out almost anytime of the year since it’s easily accessible by way of the Odakyu Line or many direct express buses. Of the many things to get into, we’re going to set our sights on the history and natural beauty of this area for this particular day trip.

**Touring the Old Tokaido Road**  There’s a bullet train, regular train, and two expressways named “Tokaido” but the original was just a foot and horse trail. Only 11km of the original path actually remains and you can find it here.  [Using this map](https://goo.gl/TUczN3), we’re going to walk along the same trail many a samurai, merchant and laborer walked in the old days of Edo and check out a little history along the way.

- **[Hakone-Yumoto Station](https://goo.gl/maps/qw8agQKuKem)** We’ll start here since this also means we will be walking downhill the entire time. The station area itself is lively with tourists and shops selling sundries and souvenirs from the area. There are also plenty of restaurants and even convenience stores in case you arrived on an empty stomach. Here’s a good chance to stock up on some water for the walk ahead too.
- **[Hakone History Museum](https://www.town.hakone.kanagawa.jp/index.cfm/6,420,14,99,html)** Since its near the trail, head here to learn more about the journey you’re embarking on and all the hot springs you’ll be walking past shortly. You’ll also learn why Hakone was a very important stop along the Tokaido.
- **[Hakone Daitengu Shrine](http://hakonedaitengu.org/)** This shrine located at the halfway point may look fairly new, but it's just because it was rebuilt in 2004. But it's been around since the late 1800’s and is a very interesting spot to check out, especially if you love the foxes that are symbolic of every *inari* shrine.
- **[Amazake-chaya](http://www.amasake-chaya.jp/)** A 350 year old teahouse. Wait, *what?* This teahouse has been serving *amazake*, a rich non-alcoholic drink which when fermented becomes the beginnings of sake since the 1740s. Of course you'll be taking that with their other specialty, *Mochi* rice cakes fired over coals. 
- **Hot springs and Minshuku**  The big draw to this area when the Tokaido had travelers shuffling through the area was the natural warm waters emanating from the earth. It made for great place to wash the grime of the road off, eat a hot meal and get some rest; and indeed not much has changed owing to the number of hotels, B&Bs and bathhouses in the hills around the trail here. Feel free to stop off at any of them and inquire into doing any of the above. 


- - - -
— By [Jason L. Gatewood](http://jlgatewood.com)
