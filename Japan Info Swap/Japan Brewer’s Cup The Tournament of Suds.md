# Japan Brewer’s Cup: The Tournament of Suds
#draft #JIS# #articles #jan #yr2019 

Please read the following in the voice of one of those monster truck TV spots from back in the day:
*Thirty-five of the best craft brewers from all over Japan, plus one each from Taiwan and the Czech Republic, and 6 craft beer importers thrown in for good measure. They’ll duke it out on the third weekend in Yokohama Osambashi Pier to show who has the best IPA of the day and whose Pale Ale raises the most hell! It’s the Battle of the Brews at Yokohama Cruise...Center. The annual Japan Brewer’s Cup is upon us!!*
Ok, that sounded admittedly hokey, but also kind of cool too right? We have talked enough about Japan and it’s history with beer to know at this point that it’s probably the best place in Asia to hunt down and guzzle your favorite beer in almost every corner of the country. So of course it only seems fitting for the masters of microbrew in Nixon gather together once a year and crown the most superior indie suds. For just ¥500 to get in and around  ¥500 per drink, you can also be sure this is one of the cheaper options in the beer festival circuit in Japan too. 

If you want to see the judging, be sure to come early when the venue opens up; otherwise you may miss the art of [beer judging.](https://www.bjcp.org/) Don’t fret if you do though because that amber gold will still find its way into a mug or pint until the sun as long set behind Landmark Tower just across Yokohama Bay. 

### For More Information:
**Osambashi Pier**
[1-4, Kaigandori 1-ChōmeNaka-Ku, Yokohama, KanagawaJapan 231-0002](￼[1-4, Kaigandori 1-ChōmeNaka-Ku, Yokohama, KanagawaJapan 231-0002](https://maps.apple.com/?address=1-4,%20Kaigandori%201-Ch%C5%8Dme,%20Naka-Ku,%20Yokohama,%20Kanagawa,%20Japan%20231-0002&auid=5600627948993908583&ll=35.449515,139.646436&lsp=9902&q=Osambashi%20Terminal&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEHQKBAgKEAASJClOWfIO97hBQDHsoQpTgnRhQDnMLhhrHbpBQEHMNwSp3HRhQA%3D%3D&t=r)