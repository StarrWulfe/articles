# Let's Tour: Yokocho Drinking Arcades

Japan is very adept at making sure its past is preserved while building the future in terms of architecture at the city level. It is very common to see a new building complex go up right next door to a 200+ year old temple complex in many cities, even in ever-changing Tokyo. However due to major earthquakes and a very large-scale war taking place in the 20th century, it's difficult to find examples of early to mid modern buildings and neighborhoods. However you need only wander into a *yokochō (横丁)* to see how things used to be only a few generations ago. 

### What is a Yokocho?
Loosely translated as "area next to a major destination", these are a collection of small stalls and restaurants that cropped up at first next to major temples, shrines and way stations on traveler's routes like the Tokaidō and Nakasendō trails between cities. In those days you'd find *yatai* stands serving grilled fish and yakitori, small teahouses and *izakaya* sake bars. You'd also find gambling dens and brothels too in those days. With the Meiji Restoration and the railroad's spread into all corners of Japan, they started cropping up next to rail stations and became the core part of a neighborhood's modern day *shotengai* shopping street radiating away from the terminal. It is here where you will find the last remaining yokocho in the 21st century.

###How's the atmosphere?
Expect small and cramped conditions; just the right atmosphere for literally rubbing elbows with the locals and using the adult beverage of your choice as social lubricant. While you shouldn't expect perfect English being used if at all, many fellow customers and staff will likely be chatty and use their level of English to find out about you -- this is not the space to go to if you wanted to be an introvert! Indeed, I can attribute my copious use of the Osaka dialect in my Japanese to my very frequent visits of the yokocho alley around Osaka's Tennoji and Nanba stations when I was a student here. Many of the establishments will have been around for generations, perhaps with the same family running them, so expect to learn a thing or two as well if you happen to get a chatty proprietor. 

### Where are they located?
Usually they can be found next to older established train stations. Many will have a sign that has the name of the area and the characters for yokocho, 横丁 as well. Unfortunately the older, more quaint examples seem to be disappearing at a rapid clip due to building code changes and redevelopment. They were't constructed of the best materials, are cramped and in many cases you'll need to access a central public restroom in the neighborhood; back in the day there were no code requirements to have a toilet! As an illustration, one yokocho I was a frequent patron of [next to Shimo-Kitazawa station](https://morethanrelo.com/en/lets-tour-the-metropolis-shimo-kitazawa/) was torn down in 2015 as the train station it was located next to was undergrounded and the space is now part of a new outdoor plaza. Fear not though, for there are plenty of them still around for now that you can check out:

####Shibuya Nonbei Yokocho
Located just northeast of Shibuya station, "Drunkard's Alley" has been bustling since the early postwar days of the 1950s as millions of people flooded into Greater Tokyo from the countryside. 

[Shibuya 1-25, Shibuya, Tokyo](https://goo.gl/maps/QrFQsisfgPZwMCyB6)
[Shibuya Station](https://goo.gl/maps/dwv49dqohqjj37FQ9) [F, G, Z, IN, JY, JS, JA, DT, TY trains]
Hours: 6pm~Late

####Shinjuku Golden Gai
Located in the heart of Kabukicho is the [Golden Gai](http://goldengai.jp/) (ゴルデン街 literally meaning "Golden Alley"). You'll find an impressive almost 300 small bars and shops around here. Although the neighborhood was incredibly sketchy back in the day, it has been reborn as a tourist area and is frequently crowded through out the evening hours.

[Kabukichō 1-1-6, Shinjuku, Tōkyō](https://goo.gl/maps/RwTMYbdCxzDqMqsm7)
[Shinjuku San-Chome](https://goo.gl/maps/UdSFc1qdyZ9UuLLH8) [F, M, S] and [Shinjuku](https://goo.gl/maps/e7eLF1V3PJ4atWpY7) [JA, JB, JC, JS, JY, KO, OH, E, M, S trains]
Hours: 6pm~Late

#### Corridor-gai
Located underneath and to the sides of the JR tracks and expressway between Yurakucho and Shimbashi stations, this long stretch of restaurants and bars is nicknamed "salaryman heaven" thanks to its prime location near many large skyscrapers and corporate headquarters. Any given weekday evening you can see groups of employees out for a drink together, and recently the weekends are bustling thanks to the tourism boom.

[Ginza 6-2, Chuo, Tōkyō](https://goo.gl/maps/WQefMeqrWHBfcsr98)
[Yurakucho station](https://goo.gl/maps/YAhHBYkDTLZM7Qy3A) [JY, JK, Y trains]; [Ginza station](https://www.google.com/maps/place/Ginza+Station/@35.6722906,139.7626887,17.91z/data=!4m12!1m6!3m5!1s0x60188be5454f4cd5:0xb887d7273136016a!2sYurakucho+Station!8m2!3d35.6749187!4d139.7628199!3m4!1s0x60188be5c2a360a1:0xa6dcab0cb7a45f2e!8m2!3d35.6717276!4d139.7644427) [H, G, M trains]; [Shimbashi station](https://www.google.com/maps/place/Shimbashi+Station/@35.6669099,139.7584024,17.9z/data=!4m12!1m6!3m5!1s0x60188be5454f4cd5:0xb887d7273136016a!2sYurakucho+Station!8m2!3d35.6749187!4d139.7628199!3m4!1s0x60188be9d1ae11e1:0x5851f1d35efc45b9!8m2!3d35.6663792!4d139.75834) [JJ, JY, JK, JJ, JO, JT, A, G, U trains]
Hours: 5pm~last train



