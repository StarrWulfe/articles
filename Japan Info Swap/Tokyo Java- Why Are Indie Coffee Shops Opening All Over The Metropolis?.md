##Tokyo Java: Why are indie coffee shops opening all over the Metropolis?

We'll forgive you for thinking Japan was all about the green tea if you've never been here before. Of course we pride ourselves on having over a hundred varieties of "o-cha" you can drink, and the [image of a kimono-clad stern performer of tea ceremony ](https://en.wikipedia.org/wiki/Japanese_tea_ceremony#/media/File:Toshihana_tea_ceremony.jpg)is forever burned into our memory banks of what iconography can be associated with Nippon. But when it comes to what is consumed in offices, cafes, and yanked out of many vending machines and convenience stores nationwide, collectively coffee is the drink of choice of many Japanese. Put it more succinctly, the first Starbucks store in Japan opened in 1996; there are now over 1,200 locations with some literally across the street from one another. But we're not here to talk about them, we're here to talk about the independent coffee shops that have started dotting the landscape, especially around Tokyo.

Niche neighborhoods like Naka Meguro, Daikanyama, and Shimo-Kitazawa are home to at least a dozen each of little coffeehouses, some unable to seat more than 6 customers at a time. But their customers are fiercely loyal, and many offer something you can't find anywhere else be it a rare strain of bean, a twist in the roasting and steeping process, or just a touch of whimsy and an eclectic atmosphere. In a quest to find out some of these mysteries, I ventured to the [Tokyo Coffee Festival](https://tokyocoffeefestival.co/), held twice a year at the United Nations University campus in the Omotesando/Aoyama area. There I asked a few of the local shopkeepers why they got into the coffee game and what they offer to their customers.

 "We like to provide a nice quiet space for people to come and relax...to get away from the daily grind and just spend some time thinking. Our cafe is designed around that thinking and our coffee blend helps too."
[Ki Cafe](http://ki-cafe.com)
[Tōkyō-to, Setagaya-ku, Daita 5−9−5](http://maps.google.co.jp/maps?q=東京都世田谷代田5-9-5&z=15)

"I had been in the business for some time but only on the sale of the beans themselves. Then I decided to provide customers their own custom roasts but I needed a cafe to showcase the idea. We want to give our customers a unique taste just for them."
[Coffee-Ya](http://coffeeya.co.jp/)
[Tōkyō-to, Nakano-ku, Honchō 4-31-10, #104](http://maps.google.co.jp/maps?q=東京都中野区本町4-31-10-104&z=15)

"I noticed 'people seem really happy when they are chatting over coffee.' I don't have a lot of resources, but its easy to have a stand; I can make a sidewalk cafe wherever I can!"
[Kakuya Coffee Stand](https://kakuyacoffeestand.wordpress.com/)
Usually found around [Chiba-ken, Nishi Funabashi, Nishi-funa 4-24-8](http://maps.google.com/maps?q=千葉県船橋市西船4-24-8&z=15)

There are a lot of different places in town to enjoy a cup of joe, so instead of heading to the familar green and white mermaid embossed store, search for a local cafe and mix things up a little. You might just find a new brew and some friends to go along with it. 

If you want to recommend your favorite java joint to the IJS crew, please drop a comment below! 


---
-- By [Jason L. Gatewood](http://jlgatewood.com)

*Images: Scenes from Spring 2018 Tokyo Coffee Festival; Jason L Gatewood*