# Things to do in Tokyo during Rainy Season
We’ve braved yet another cold winter, sat out under the pink trees during *hanami* season, and seen it warm up steadily with our minds starting to look towards the barbecues, beach trips and *bon ordori* festivals of summer. But we have to get through three or so weeks of dreary, sopping wet *tsuyu* rainy season in the first half of June first. So in a bid to save you from cabin fever after being outside during our spectacular springtime, let’s check out what can be done around the Tokyo Metropolis even when the weather isn’t being very cooperative.

### A museum visit is never a bad thing
Like any world-class city worth its salt, Tokyo is awash in cultural institutions. Museums and galleries are all over the place waiting for you to check out the hidden treasures inside.  Here are a few we’ve covered to get you started:
[Educational Museums in Tokyo That Your Kids (and You) Will Love](http://japaninfoswap.com/museums-tokyo-kids/)
[Taito Shitamachi Museum](http://japaninfoswap.com/learn-about-tokyos-yesteryear-at-taito-shitamachi-museum/)
[Museum of Yebisu Beer, Tokyo](http://japaninfoswap.com/museum-of-yebisu-beer-tokyo/)
[Visit the JR East Railway Museum to see Japan’s history move on wheels of steel](https://japaninfoswap.com/?p=35648&preview=true)

### Ready, Set, Charge it! Let’s go shopping
There’s no shortage of places to be separated from your hard-earned yen here, and since it’s pouring outside you may as well go ahead and use that as an excuse for a mini shopping spree.
[Tokyo Midtown Hibiya: Upscale shopping at the Emperor’s front door](https://japaninfoswap.com/tokyo-midtown-hibiya/)
[Lalaport](https://www.google.co.jp/search?newwindow=1&q=Lalaport&npsic=0&rflfq=1&rlha=0&rllag=35601695,139778228,21295&tbm=lcl&ved=0ahUKEwiJwLO9qqjbAhVGwLwKHbGhAJsQjGoIbQ&tbs=lrf:!2m4!1e17!4m2!17m1!1e2!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2&rldoc=1#rlfi=hd:;si:;mv:!1m3!1d325997.8451402943!2d139.76236062382816!3d35.60899486776202!3m2!1i871!2i603!4f13.1), for when you need a “back home style” shopping mall experience.
[Mitsui Outlet Parks](https://www.google.co.jp/search?newwindow=1&tbm=lcl&ei=4-wLW9aYFcSA8gX_jKWgCg&q=Mitsui+outlet+park&oq=Mitsui+outlet+park&gs_l=psy-ab.3..0i67k1l3j0l2j0i203k1j0l2j0i203k1j0i20i263k1.26331.30361.0.30667.18.10.0.8.8.0.115.925.3j6.9.0....0...1c.1.64.psy-ab..1.17.965...35i39k1.0.8XUiWBgXQps#rlfi=hd:;si:;mv:!1m3!1d334418.6572414669!2d139.73944015706206!3d35.53435784494344!3m2!1i886!2i618!4f13.1) offer discount prices but are a little more involved to get to. Some offer bus service from nearby train stations or even Shinjuku and Tokyo stations. 
Get your nerd on in [Akihabara](https://japaninfoswap.com/under-the-tracks-in-akihabara/) or [Nakano](https://japaninfoswap.com/nakano-broadway-tokyos-other-akihabara/). If you like to geek out over cameras, trains, comics, games or anything else, just go to one of these places and your day is made.

### Bum out at the Bijoux
Personally, this is what I wind up doing by default. Most summer blockbusters come over to Japan around this time if they haven’t arrived already, and there’s usually a few good Japanese flicks out as well if you can speak the lingo. 
[Toho Cinemas](https://www.google.co.jp/search?newwindow=1&tbm=lcl&ei=cu8LW6-aBYLL0gS-5I2wAQ&q=toho+cinemas&oq=toho+cinemas&gs_l=psy-ab.3..35i39k1j0j0i67k1j0l2j0i67k1j0l4.34033.34720.0.35435.4.4.0.0.0.0.189.413.0j3.3.0....0...1c.1.64.psy-ab..1.3.411...0i7i30k1j0i13k1.0.vpAikpv7Z8o#rlfi=hd:;si:;mv:!1m3!1d183285.37369984947!2d139.74976317943992!3d35.60334001396106!3m2!1i886!2i678!4f13.1)
[Aeon Cinemas](https://www.google.co.jp/search?newwindow=1&tbm=lcl&ei=Tu8LW8CuJIG50ATg7LvIAw&q=aeon+cinemas&oq=Aeon+&gs_l=psy-ab.3.0.0i67k1l2j0i20i263k1j0i203k1j0j0i203k1j0l4.30406.33265.0.34446.11.7.3.1.1.0.112.714.2j5.7.0....0...1c.1.64.psy-ab..0.11.732...35i39k1j0i10k1j0i10i203k1.0.V7uphUU4LkA#rlfi=hd:;si:;mv:!1m3!1d735106.2199843476!2d139.65937655778725!3d35.38833264113291!3m2!1i886!2i678!4f13.1)
[109 Cinemas](https://www.google.co.jp/search?newwindow=1&tbm=lcl&ei=A-0LW-evDYer8QXkq4aAAw&q=109+cinemas&oq=109+&gs_l=psy-ab.3.1.0l2j0i20i263k1j0l7.583110.584435.0.586279.4.4.0.0.0.0.205.495.2j1j1.4.0....0...1c.1.64.psy-ab..0.4.494...35i39k1j0i67k1.0.w_vBW8O_4lI#rlfi=hd:;si:;mv:!1m3!1d72452.24743810622!2d139.68828325!3d35.599845450000004!2m3!1f0!2f0!3f0!3m2!1i378!2i268!4f13.1;tbs:lrf:!2m4!1e17!4m2!17m1!1e2!3sIAE,lf:1,lf_ui:4)

### Stay home and chill
We have some of the fastest internet speeds on Planet Earth, and we can watch [Netflix](http://netflix.com), [Hulu](https://hulu.com) and even get programming from back home by using a [VPN](https://en.wikipedia.org/wiki/Virtual_private_network), so why not kick back and veg out in front of the TV? You did get a [Chromecast](https://store.google.com/product/chromecast_2015) or [Apple TV](https://www.apple.com/tv/) right?  Remember, you don’t even need to worry about what to eat; there’s an app for that too! 
[Tokyo Food Apps Bring the Restaurant to Your Door](https://japaninfoswap.com/tokyo-food-apps-bring-the-restaurant-to-your-door/)
[Grocery Delivery Services In Tokyo](https://japaninfoswap.com/grocery-delivery-services-in-tokyo/)

### Get your traveling shoes and go somewhere else
If you have some way to take time off, you can always just go somewhere else; it isn’t rainy season everywhere right? Okinawa just came out of their rainy season and Hokkaido won’t get wet until July or so. And if you wanna leave Nippon totally there’s always Hawaii, Guam, Taiwan and more just a Haneda or Narita airport away.

### Don’t panic, keep calm and stay dry 
But if you do stay in town, remember to be careful and mind these tips for dealing with the impending drenching that is rainy season in Japan:
1. Buy an umbrella and take it EVERYWHERE. Do not try to tell yourself that it’s sunny and doesn’t look like rain. It will rain and you will be the only one without an umbrella. Yes, they’re a pain to carry with you and yes, you will leave them somewhere (second hint: buy a cheap plastic one from Daiso or konbini) and need to buy another, but you will very grateful that you have it.
2. Invest in a good pair of rain boots. Yes, I’m talking the rubber type that people in other areas of the world refer to as galoshes, gumboots, wellies, rain shoes etc. Make sure they fit well and are comfortable. You will be wearing them a lot. Most shoe stores, department stores and even some grocery stores have what you need to keep your tootsies dry.
3. Take an extra pair of socks or stockings with you when you go out. Even with rain boots you will find that the rain is extra sneaky and manages to slip inside and drench everything in sight. There is nothing worse than having to sit in wet socks all day, especially when the air conditioning is cranked up and makes you cold.
4. If you’re like me and don’t mind being wet outside but want to get dry in a hurry indoors, then just wear some sandals and shorts. I grew up in a very flash flood prone part of America and became used to wet feet outside, dry feet inside. Remember to bring a towel!
5. Have a back-up plan if you plan on having a BBQ or want to do any sort of outdoor activity. In downtown Hiroshima you can escape the rain by using the handy underground Shareo mall and the undercover protection of Hondori.
6. Breathe, try to stay cool, calm and collected (yes, even in the ridiculously humid weather) and don’t yell at everyone that annoys you. Tempers are easily tested during this season, but remember, it’s just a season and it won’t last forever.
  
- - - -
—- by [Jason L Gatewood](http://jlgatewood.com)
