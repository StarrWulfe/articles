# Meguro Sanma Festival: Thanks for all the (free) fish!

It’s no secret that Japan is a seafood lover’s paradise. Everything that swims, crawls or otherwise calls any body of water home can be found on a dinner table here. Of course the national dish, sushi, is served raw but grilling fish over coals is also part of traditional Japanese cuisine, to the point where every home has a fish grill built right into their cooktop ranges. Just as everything else here in Nippon, there’s a season for certain fish, and September marks the beginning of *sanma* (Pacific Saury, a part of the sardine family) season, and one of the tastiest fish you’ll find. One place in Tokyo marks this time with a festival dedicated to the tantalizing fish.

### Meguro City Festival
Meguro Ward being one of the 23 wards of central Tokyo has their festival around the beginning of September every year and part of its popularity centers on their tradition of giving out free grilled sanma. Over 5000 of the fish will be handed out, hot off the coals fresh from Meguro’s sister city Kesenuma in Miyagi Prefecture, about 250km north. As such with all things popular in Tokyo and free in Japan, expect some of the longest lines you’ve ever seen, so you might want to get an early start. However don't fret because there’s still an area showcasing other foods from around Japan, and entertainment options such as taiko drumming and Japanese comedic storytelling called [*rakugo*](https://en.wikipedia.org/wiki/Rakugo).

### Access and Times
**Date:**  September 15, 2019
**Time:**  10:00am – 3:30pm
**Fees:** Free, but bring cash for other food and recreation items.
**Place:** [1-25-8 Meguro, Meguro, Tokyo](https://goo.gl/maps/6amd1szRQBLiHU157)
**Closest station(s):** Meguro Station (JY22, I01, N01, MG01) 
**Web:** [http://www.owarai.to/megur...](http://www.owarai.to/meguro/ "Meguro Sanma Festival")