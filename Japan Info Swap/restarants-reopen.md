# Dining out in Tokyo Post Coronavirus 
Starting with the State of Emergency that went into effect for Tokyo and
surrounding areas at the beginning of April, many restaurants went into a
"takeout and delivery only" mode along with drastic cuts in hours and menus. Now
that Japan has started reopening, many are in a hurry to head back to their
favorite dining spots especially in the Metropolis, known for having one of the
highest concentration of restarants in the world. Eating out is OK again, but
you are going to be in for a world of changes upon entering your favorite
eatery.

### D i s t a n c e d 

The biggest noticable change will be all the new procedures in the
establishments themselves to prevent any transmission of coronavirus.
Social distancing measures are in full effect, and that means you may not
even be able to get into your gnosh-spot without first making
a reservation. My little hole-in-the-wall ramen joint in my corner of
Tokyo has cut their seating area capacity in half; so that means the
already tiny dining area that could seat 20 is slashed to 10 people only.
There's a QR code on the door that lets you access their reservations app
that will place you into a queue without standing in a line
outside...which would create the same conditions they're trying to avoid
indoors. Win-win?

### Sectioned off
You will also likely find plastic and acrylic barriers in addition to many seats
left empty between dining parties in order to cut down on the probability of
airborne transmissions. 

### Takeout Only

Some places may not even be open for indoor seating partially or fully.
Still others have gotten creative and made outdoor seating areas wherever
possible. If nothing else, the coronavirus has encouraged many bistros to
consider outdoor dining as a thing, something from my hometown that 


### New Menu

In some cases, its impossible to keep the same items on the menu due to
supply chain issues stemming from the pandemic, reduced business hours,
and/or staffing shortages. In any case, be prepared for ingredient
substitutions to dish omissions at your eatery of choice.

### Hour-by-Hour

As stated above, some places have had to adjust their opening hours as
well. My favorite Indian cafe in my area is now only open three days
a week because of staff being caught outside the country when the pandemic
hit. Make sure to check those hours of operation before going.


It's the "new normal" for us here in Tokyo and beyond as we do battle with
COVID-19 in the intrim before a vaccine is created, but with a lot of
preparation and a little luck, the hope is that we can stay safe and still
socialize. 
