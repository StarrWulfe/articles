##### Nakano Broadway: Akihabara-in-a-Box

No matter how you slice it, Tokyo's modern image is the stuff of cyberpunk dreams. We are the city of [giant robots, iron gollums](http://muza-chan.net/aj/poze-weblog/top-6-giant-robots-japanese-tourist-attractions.jpg), and [Godzilla](https://c2.staticflickr.com/8/7621/26583882153_4657e54ed4_b.jpg). So of course most people's image of "Cool Japan" is found in Akihabara, our city's major electronics district and hub of all things anime and video game related. Of course you gotta head over there and check out "Akiba"; its truly one-of-a-kind... Or is it? 

Situated just west of Shinjuku, Nakano Station is at the center of the namesake borough of Tokyo which became home to the actual studios that output the majority of Japan's animation and comic books to the world. Because of that, it's become a sort of mecca to those who are so steeped in "otaku" culture that those in the know would tell you "Only tourists bother with Akiba; if you want the real deal, check out Nakano". Specifically [Nakano Broadway](XXX), which is located north of the station at the end of the [Sun Mall](XXX) shopping arcade. 

Opened in 1966, Nakano Broadway's first 4 floors are a hodepodge of many different shops, services and restaurants. The basement has both a Seiyu supermarket and a Daiso 100 yen shop. There's a great ice cream place and a bunch of ramen and udon shops as well. If you're into palm reading and fortune telling, there's a whole hallway in the basement dedicated to just that. But what "The Broadway" is known for best is it's huge amounts of toy, game, hobby and comic shops in the upper levels.

Up on the 3rd floor, [Mandarake](http://earth.mandarake.co.jp/shop/index.html) serves as de facto anchor for all the character related stores. Started in 1987 right here in Nakano Broadway as a simple secondhand comics shop, it has grown into a large chain of stores dealing in literally anything having to do with anime characters. While you can find a branch of Mandarake in every large city in Japan (and at least 6 other branches in Tokyo alone), this store is pretty special for being the first.  If you are looking for vintage Japanese comic and animation goods, start here.

The smaller shops on the upper floors are gold mines as well; there's even two stores dedicated to those who collect original animation cells! Also good news for Japanese toy fanatics: There are at least 4 different shops selling everything from Takara Tomy cars and Pla-Rail to vintage Voltron (oops, I mean Go-Lion) playsets. I've even managed to find a mint condition 1980's era Transformers Megatron toy here... You don't wanna know the price though, trust me.

Audio/video creation geeks aren't left out either thanks to the [Fujiya Avic](http://www.fujiya.avic.jp) store on the 2nd level. If you need DJ turntables, a Moog analog synthesizer, and a Blackmagic camera rig to shoot your next feature film, you'll drool over it here. And if you're in to timepieces, [Jackroad](http://www.jackroad.co.jp/shop/pages/access_en.aspx) will make sure your wrist walks out with something unique; they've been here for 30 years! 

There's something for everyone and then some at Nakano Broadway, whatever you're into. The smaller size and cozy atmosphere makes it more inviting than its eastside rival Akiba across town according to some; but don't take their word for it... Check it out yourself!

##### Access
- Nakano Station (Chuo, Tozai lines), North Exit 
- [Map](https://www.google.co.jp/maps/place/Nakano+Broadway/@35.7091428,139.6655408,15z/data=!4m2!3m1!1s0x0:0xe1b67e4ee0eff0be?sa=X&ved=0ahUKEwj_mLrPpJvYAhUBQZQKHfKCC_cQ_BIIiAEwDg) 
- [Website](http://www.nbw.jp/)


 