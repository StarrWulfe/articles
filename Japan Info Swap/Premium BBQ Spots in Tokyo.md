# Premium BBQ Spots in Tokyo 
As long as we have known fire, we’ve also known barbecue. Most of humanity would agree that there’s nothing beating a few pieces of meat and/or veg on the grill, with the smoke from the coals helping to give it a flavor that’s unmistakably summertime. We Tokyoites have it rough however: even if we are lucky enough to have enough space in our home to store a bonafide Weber or Coleman grill, in many cases we have no space on the premises to actually cook out. Of course there are a few parks and greenbelts that will allow us to do so, but that also means having to drag the grill, charcoal and food there and back… And most of us only have a Suica pass and a bicycle as far as transport. Luckily some enterprising people have found a way to capitalize on life in the Japanese capital…
![](Premium%20BBQ%20Spots%20in%20Tokyo//japaninfoswap.com/wp-content/uploads/2018/07/kichijoji-BBQ-1-300x225.jpg)
### Enter the “do-it-with-help” barbecue spot
The concept of “full-service BBQ areas”, where there are chairs tables, grills, charcoal all provided for a fee. Here, you are in charge of the grill, but the staff prepares the area for you and provides a place to toss your rubbish once finished. These spots are strategically located on rooftops of buildings, in parks or the shores of rivers, lakes and the ocean. Open from early spring until late fall, many also provide beverages (including all-you-can-drink adult drink packages) and also have deals with nearby markets where you can purchase food to be grilled on-site.
![](Premium%20BBQ%20Spots%20in%20Tokyo//japaninfoswap.com/wp-content/uploads/2018/07/kichijoji-BBQ-3-300x225.jpg)
### Let’s BBQ!
Start by heading to one of the following websites and picking out a location along with a date and time plus the number of friends you’ll bring along. You may need to enlist the help of a Japanese reading friend to help, but Google Translate is also good enough now to navigate their homepages as well.

* [Dejique (デジキュー)](http://digiq.jp) has the most locations, not just in Tokyo, but nationwide. Here they are found in places such as the rooftop of Keio Department Store in Shinjuku, Copicce in Kichijoji, and the shores of Odaiba Seaside Park.
* Not to be outdone is [Hero Field](https://www.herofield.com/), another chain of full-service BBQ areas in locations like the rooftop of both JR Meguro Atre and JR Tachikawa Lumine.

Prices in most cases start around ¥1,000 per person before add-ons like drink nomihodai and extra food services. Once you’ve penned in your reservation, make sure to have your food and drinks ready for the cookout. Also remember to dress for the occasion; take cold packs with you if the heat will be an issue and wear sunscreen. In many cases there are supermarkets nearby (my local spot in Kichijoji had a grocery store in the same building) so you may only have to come a little early and get the shopping done beforehand without needing to lug around bags on the train.

- - - -

— By [Jason L. Gatewood](http://jlgatewood.com) *images:* “Rooftop BBQ in Kichijoji Tokyo” by [Jason L Gatewood](http://jlgatewood.com) is licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0)
