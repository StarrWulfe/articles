href="https://morethanrelo.com/en/flying-high-in-minato-mirai/">Flying High in Minato-Mirai 
The concept of a gondola in an urban setting isn’t new; in fact they serve as high speed transport options in places like Bogota, Colombia and Rio de Janerio, Brazil. There was even a plan to build one to connect the main Olympic venues in Odaiba with Tokyo station and another connecting Tokyo Olympic Stadium to Station floated at one point; however it morphed into a plan for BRT (Bus Rapid Transit) instead. What those plans did spark however, was an idea to utilize gondolas as a way to provide yet another tourist attraction in Yokohama’s Minato-Mirai amusement area, and provide a direct way to zip across the collection of inlets and water features separating Sakuragicho station and the World Porters shopping center area.

### ![](<https://morethanrelo.com/wp-content/uploads/2021/04/yokohama-air-cabin-01-300x150.jpg> =300x150) What’s Old Is New Again 

If you’ve been around the area for the last 30-odd years, you might have a sense of deja-vu; During the [Yokohama Exotic Showcase (YES’ 89)](<https://ja.wikipedia.org/wiki/%E6%A8%AA%E6%B5%9C%E5%8D%9A%E8%A6%A7%E4%BC%9A>) , there was a temporary one set up in roughly the same area for visitors to the expo but it was quickly dismantled after the expo ended. The [Yokohama Air Cabin](<http://yokohama-air-cabin>) however is a very permanent addition to the scene. The new gondola travels 630 meters and 40 meters above the landscape at its highest point, and just like the one built for the *YES* expo three decades ago, this one is meant more as an amusement attraction since it’s operated by the same company that runs the nearby *[Cosmoworld](<http://cosmoworld.jp>)* theme park. In fact, you are able to buy a combined ticket for both the new gondola and the famous [“CosmoClock21”](<http://cosmoworld.jp/attraction/wonder/cosmoclock21/>) Ferris wheel.

### ![](<https://morethanrelo.com/wp-content/uploads/2021/04/yokohama-cabin-route-map-300x161.png> =300x161) From on high… 

Each car can carry a total of 8 passengers (and expect that number to be tailored down and limited to the number of people in your party due to the ongoing COVID-19 pandemic.) The trip across takes about 5 minutes and costs ¥1,000 one-way, but there are discounts for kids and combo tickets available.





| [Yokohama Air Cabin](<https://yokohama-air-cabin.jp/>) ||
| --------------------------|---------------------------- |
| 🗺 Address: |2-1-2 Shinko, Naka-ku, Yokohama City 231-0001 |
| 🕙 Hours:                                              | Everyday 10am~8pm (Varies due to weather and other conditions) |
| 🎟 Tickets:                                            | One-way Adult ¥1,000; Kids 3-11yo ¥500 |
|| Round-trip Adult ¥1,800; Kids ¥900                     |
|| Groups 20pax+ 10% off; Disabled persons 50% off        |
|| See website for other ticket deals                     |
| 🚇 Access:                                             | Sakuragicho Station 🅹🅺, Ⓑ |

* 📸 <span style="font-size: 10pt;"> images via </span>

*[<span style="font-size: 10pt;"><em>Yokohama Air Cabin website</em></span>

](<https://yokohama-air-cabin.jp/>)

