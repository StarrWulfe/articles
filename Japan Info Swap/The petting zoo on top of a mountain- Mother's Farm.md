# The petting zoo on top of a mountain: Mother's Farm

Ok, that may have been a bit of an over-simplification. Mother Farm (マザー牧場) is an expansive working "entertainment" farm that is located in the Chiba's central Boso Peninsula  countryside sitting 300m atop Mt. Kanozan. All of these qualities comebine to create the perfect outing for those wanting to getaway from the Metropolis's traffic-choked, building lined streets, and are looking for some open space with the kids in tow.

Not your typical theme park, the farm was the idea of Hisakichi Maeda, the founder of the Sankei Shinbun newspaper and then went on to develop Tokyo Tower. Growing up in prewar rural Osaka, his mother often said "if only we had one more cow we could prosper." That must have stuck to him because in 1962, he opened the farm, the name of which is to honor his own mother.

Within it's 250 hectare expanse, you will find horses, pigs, sheep, alpacas, goats, and cows. This also means the kids can enjoy pony rides, cow milking, butter churning, piggy racing, duck parading, sheep shearing, and general oogling at the other animals in the livestock sections. 

Mother Farm is also known for its fields of wildflowers so if you visit at the right time, the surrounding grounds will be an explosion of yellows, oranges, and lavenders as petunias, daffodils, wintersweets, and rape blossoms bloom. Speaking of plants, strawberries, blueberries and blackberries can be picked here as well but only in limited quantities in-season to avoid over-harvesting. 

If you happen to have your own four-legged family member looking to get away, check out probably the largest collection of dog runs in the Greater Tokyo area; your canine pal will go absolutely bonkers with happiness in this environment based on my own dog's reaction. It's also one of the few places that are both family and dog friendly. 

At some point the older kids (and maybe you as well) will want a little bit more excitement. For that there's Waku-Waku Land (わくわくランド). The carnival-style setup places rides and attractions area in the middle of the farm, complete with the requisite Ferris wheel, go-karts, bungee-jumping and a zip-line (my personal favorite.) 

As for the dining options... well it *is* a farm right? There's 3 main restaurants and numerous small stands in the park. One of the restaurants is actually a barbecue area where you can eat "Genghis Khan style", a Japanese euphemism for barbecued lamb, beef, pork and stir fried veggies cooked on a grill at your table. Another restaurant is more traditional in nature, but offers splendid views of the farm and valley below. One other must-get while we're talking food: the fresh ice cream! Those cows aren't just for show; the ice cream is as fresh as it gets, complete with fresh blueberries and strawberries. 

##Info:
[Mother Farm (マザー牧場)](http://www.motherfarm.co.jp/en/)
[940－3, Tagura, Futtsu City, Chiba Prefecture 299-1601](https://www.google.co.jp/maps/place/940-3+Tagura,+Futtsu-shi,+Chiba-ken+299-1731/@35.2454348,139.9318962,17z/data=!3m1!4b1!4m5!3m4!1s0x60180f5262f65c2f:0x4c841cb564ff113f!8m2!3d35.2454348!4d139.9340849?dcr=0)
Tel: +81 (43)937-3211

###Opening Hours:
Feb to Nov 09：00 – 17:00 (Weekdays 09:30 – 16:30)
Dec to Jan 09:30 – 16:00 (Weekdays 10:00 – 16:00)
Hours may be extended depending on the season, especially during the holiday season. Check their website for the latest schedules and the specific dates when they are closed.

###Admission Fee:
Individual – Adults ¥1500, Child ¥800
Group (20 & above) – Adults ¥1100, Child ¥600
Kids – 3 years old & below enter free.

###Access:
##### Train 
JR Uchibo Line (some trains on Yokosuka/Sobu line thru-route): 
Head to Kimitsu Station 「君津駅」
#####Bus
Aqua-Line Nonstop Bus is available from the following stations to Kimitsu Station:
• Tokyo Station, Yaesu Bus Terminal
• Shinjuku Station, Shinjuku Bus Terminal
• Shinagawa Station, East Exit
• Haneda Airport, Bus stop #12
• Kawasaki Station, East Exit, Bus Stop #34
• Yokohama Station, East Exit, Bus Stop #18

From Kimitsu, a shuttle bus is available to the farm, outside the south exit, bus stop #1.

Other options are available just by consulting the mapping app of your choice.

#####Car
[940－3, Tagura, Futtsu City, Chiba Prefecture 299-1601](https://www.google.co.jp/maps/place/940-3+Tagura,+Futtsu-shi,+Chiba-ken+299-1731/@35.2454348,139.9318962,17z/data=!3m1!4b1!4m5!3m4!1s0x60180f5262f65c2f:0x4c841cb564ff113f!8m2!3d35.2454348!4d139.9340849?dcr=0)

Tateyama Expwy [E14], Kimitsu exit.
Prefectural Route 163 south to 93 west. 

---
*By [Jason L. Gatewood](http://linkedin.com/in/jlgatewood)*

Images: <a href='https://www.flickr.com/photos/inucara/17748609856/' target='_blank'>マザー牧場</a>&quot;&nbsp;(<a rel='license' href='https://creativecommons.org/licenses/by/2.0/' target='_blank'>CC BY 2.0</a>)&nbsp;by&nbsp;<a xmlns:cc='http://creativecommons.org/ns#' rel='cc:attributionURL' property='cc:attributionName' href='https://www.flickr.com/people/inucara/' target='_blank'>Kentaro Ohno</a>