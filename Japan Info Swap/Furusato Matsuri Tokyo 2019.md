# Furusato Matsuri Tokyo 2019
#draft #JIS# #articles #jan #yr2019 


Like food? Silly question right, of course you like food?! If you are looking for the opportunity to sample dishes from all over Japan, but don’t really have the time on your hands to be bouncing through all 47 prefectures and regions to seek out each individual dish (Or like me, you already have “been there, done that” and want to try that dish again from a long time ago) the “Furusato Matsuri” should definitely be on your list of things to do in the new year.
Furusato Matsuri Tokyo is held annually at the Tokyo Dome in early January. Roughly 400,000 people cycle through the venue, enjoying not only local food from throughout the country but small slices of regional culture as well, in the form of traditional performances and ceremony showcases. 

This year there will be a special venues for [ramen](https://www.tokyo-dome.co.jp/furusato/special/ikemen/), [sweets](https://www.tokyo-dome.co.jp/furusato/special/sweets/) as well as craft beer, sake and more to discover as some 300 companies, organizations and bureaus set up shop under the dome. If you’re missing the summer matsuri fun of Japan (or you happen to only be here over the winter months) then this is the perfect time to get your *”washoi”* on as we ring in the new year. Oh and don’t worry about the language barrier because this year there is a multilingual smartphone app that will help you out, so keep that battery charger handy!

### Access
**Tokyo Dome**
- 1 Chome-3-61 Kōraku, Bunkyō-ku, Tōkyō-to 112-0004, Japan ( [map link](https://goo.gl/maps/m2twCt48vxF2) )
- [www.tokyo-dome.co.jp](https://www.tokyo-dome.co.jp/e/ ) 
- 03-5800-9999
- 2-minutes walk from Tokyo Metro Korakuen Station [M][N] or 10 minute walk from  Suidobashi Station [I][JS]
- January 11–20, 2019, 10:00–19:00
- Tickets 1,700 yen / Half-Day 1,400 yen / Evening 1,300 yen
- [www.tokyo-dome.co.jp/furusato](http://www.tokyo-dome.co.jp/furusato/) 
![](Furusato%20Matsuri%20Tokyo%202019/photo01.jpg)
[“via Tokyo Dome/Furusato Matsuri website”](http://tokyo-dome.co.jp/furusato) / [Public Domain](https://wiki.creativecommons.org/Public_domain)

--[Jason L Gatewood](http://jlgatewood.com)