# Let's Tour: Odawara

Getting there is a breeze since not only does the town's namesake railway, Odakyu Railroad, runs between here and Shinjuku station in Tokyo, but JR East's Tokaido commuter line along with JR Central's Tokaido Shinkansen makes a stop here as well. Odawara is also the gateway to the Hakone ski and nature reserve area as well as a good way to head to Mt. Fuji. Even if you're headed to these destinations, Odawara should definitely bookend your journey, if only for its convenience.
The station itself is currently being overhauled and has a decent sized shopping mall grafted onto its southern ocean-facing exit into the main downtown area called [Minaka Odawara](https://www.minaka-odawara.jp/), and includes an up-to-date rendition of an old Taisho/Showa era collection of *michi-no-eki* at the street level. It's just a facade, and inside is very much appropriate for 2021.
A [tourist information center](https://livejapan.com/en/in-tokyo/in-pref-kanagawa/in-hakone_odawara/spot-lj0000045/) is also located on the main gate level of the station and should be your first stop to pick up maps and various guidebooks in the language of your choice; there are also discount coupons and tickets handed out here at various times of the year as well!


## On the way to the castle

Heading out of the East exit of Odawara Station, head south on the street immediately in front of there, passing by the new *Minika* shops along the way. Eventually you'll reach a junction with an elementary school in front of you; turn left here and as you walk along, there will be historical markers every so often. These mark the former outer walls and gates of the Odawara Castle complex. You can see some of the original stone "boulders" used as masonry along with a very well manicured stream that was once part of the castle's moat. Sections of it are now used as planting beds for different varieties of flowers. Also note that many of the walkways and streets that form the perimeter of the castle grounds are lined with Japanese cherry trees and the area is usually a prime spot for *hanami* cherry blossom parties during the early spring blooming season.(however due to the ongoing COVID-19 situation, many events are still cancelled&#x2013; always check the Tourist Information Center for updated information.)



## Castle Grounds

Rounding the bend and crossing over the moat over wooden bridges and into the main gate, you start to notice the serious undertaking that happens to create all these old structures. Though a large part of Odawara Castle is a recreation of what stood there, many of the construction techniques used in the recreated outer walls, and the castle itself, were done using materials and tools from that time period.
Also for seemingly no related reason, there is a sizeable enclosure housing a family of Japanese Macaque monkeys sitting prominently for all to crowd around and watch them "monkey around" (sorry-not-sorry). If you're really lucky, you can spot their wild cousins living in the mountainsides all over Japan and sometimes can be seen bathing in hot springs.



## In the neighborhood;

Unfortunately, the pandemic caused the tours to the actual castle itself to be cancelled, but according to the guide, the caretakers are using the downtime to rehabilitate the interior so you now have something to look forward to when the time comes! 😉 But there's plenty of attractions that are still open for business in the area around the castle:

-   **[Kodomo Yuenchi](https://maps.apple.com/?address=Odawara%20Joshi%20Park,%205,%20Jonai,%20Odawara-Shi,%20Kanagawa,%20Japan%20250-0014&auid=10478161586079874297&ll=35.251368,139.152941&lsp=9902&q=Kodomo%20Yuenchi&_ext=ChgKBAgEEAoKBAgFEAMKBAgGEBIKBAgKEAESJinFBsGKpJ9BQDEpeqnWt2RhQDlD3ObmyqBBQEHtUhD0EWVhQFAE&t=r)** -  a children's  playground on the verge of being a small amusement park, complete with a rideable model railroad!
-   **[Hotoku Ninomiya Shrine](https://maps.apple.com/?address=Odawara%20Joshi%20Park,%205-30,%20Jonai,%20Odawara-Shi,%20Kanagawa,%20Japan%20250-0014&auid=1159245368174770703&ll=35.250025,139.152751&lsp=9902&q=Hotokuninomiya%20Shrine&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEMwBCgQIChAAEiYp57KHnXqfQUAxn6J+m7dkYUA5ZYit+aCgQUBBs0WIuBFlYUBQBA%3D%3D&t=r)** fulfills the requisite "shrine next to Japanese historical site" duty, and is a very picturesque place find a respite after all the walking you've done so far. But you'll also notice a lot of schoolkids and their parents coming to pray here for reasons that are explained in the next point&#x2026;
-   **[[https://www.hotoku.or.jp/][Hotoku Museum]]** is the place to go to find out about Ninomuya Kinjirou, who is not only the namesake of the aforementioned shrine, but is also [a very famous philosopher and self-taught farmer](https://en.m.wikipedia.org/wiki/Ninomiya_Sontoku) whose land management techniques staved off a famine in the early 19th Century. Because he was quite frequently seen walking with a book in his hand, he is sometimes called the unofficial "God of Studying."


## Down the Shore

Since Odawara sits on the coast of Sagami Bay and the sea can easily be spotted from the plateau the Castle sits on, my wife and I decided to see if there was some kind of local beach area in Odawara. Japan being an island archipelago, doesn't offer as much in the way of "resort level sandy beach areas" as you'd think; much of the shoreline is rocky. But the Shonan area in Kanagawa is known for its beachfront area, and Odawara's public beach area is up to the task. **[[][Miyuki-no-hama Beach]]** is a short 15 minute walk due east from both station and castle. We were only able to see it on a warmish early spring afternoon in March, but I'm told it's not a bad place to picnic and take a dip in the sea during the summer months either.


## Street Food

*[Kamaboko Dori](https://g.co/kgs/DfciGw) loosely translates to *"street where one can chow down"* and has been that way since Odawara was a stop on the Tokaido road between Osaka and old Edo (now Tokyo). Only a few shops remain, but they are all family owned and offer a glimpse not only into the past, but present day-to-day "mom & pop store" Japan. Most of the eateries deal in seafood (we *are* on the shore after all) and we chose to scarf down some fish croquette pita sandwiches and wash it all down with some locally produced plum wine spritzers. Just the thing to help us gather the strength needed to walk back to the station.



## Getting There

Odawara is served by JR East's Tokaido and Shonan-Shinjuku Lines [JS] [JT], Odakyu's Odawara Line [OH47], and JR Central's Tokaido Shinkansen bullet train all at [Odawara Station](https://maps.apple.com/?address=Shiroyama,%20Odawara,%20Kanagawa,%20Japan&auid=7031964786266131702&ll=35.256424,139.155429&lsp=9902&q=Odawara%20Station&_ext=CiUKBAgFEAMKBQgGEOEBCgQIChACCgQIJRBlCgQIKhADCgQIOhABEiQpmJAoFLWgQUAxzpBfQ/BkYUA55FRj8++gQUBBYsWFSQJlYUA%3D&t=r). This is also the gateway for Hakone and the Fuji Five Lakes area via the Hakone Tozan Railway as well as bus service to Mt. Fuji itself.

