# Toyota MEGAWEB

Tokyo can be a tough place to find something cool for the whole family to
get into without either needing to travel a ways into the countryside or
dropping copious amounts of yen on the adventure. However here's something
that is both close to the center of the Metropolis and yet relatively
inexpensive for a day's outing. 

Located on Odaiba isle, MEGAWEB is Toyota Motors' "car theme park to
'Look', 'Ride' and 'Feel' automotive tech". Those three themes are housed
inside three separate areas around their facility. 

### Look 

At the Toyota City Showcase building, you can check out over 60 models of
Toyota's automobiles and even test drive some of them on the private track
in the parking area connected to the facility (provided you are properly
licenced of course!) Some vehicles showcased are rarely seen 										 models like
the new "JPN Taxicab" that is currently being phased in by many livery
companies to replace the aging Crown Comforts that define the industry
here in Japan. You can also chech out futuristic concept cars, Gran Prix
and Rallycar racers, and more. There are also video simulators, technology
exhibits and other games in this area as well.

### Feel

Head over to the History Garage area to check out not only how Toyota
Motors got its start in racing in the 1960s. Everything here is decked out
to look as if you're standing in Postwar "Showa" Era Japan. Here you'll
learn about how mechanics and technology have changed over the years, and
don't miss the classic cars from the age either. Toyota has a bigger
collection in it's main museum near Nagoya, in case you're up for
continuing your journey down memory lane sometime. 

### Ride

The Ride Studio is where the kiddos will likely be happiest, since this is
the part where indoor and outdoor karts can be driven. Even preschoolers
get their own course where they can toot around and have some fun. There's
another longer indoor course for the older kids to learn traffic rules and
enjoy driving, and an outdoor e-kart track for kids and adults alike to
mix it up a bit; just be careful about "swappin' paint", mm'kay?  

### Is it pricy? How long are the waits?

Pretty much everything except for the rides in the Ride Studio is free;
Most rides are ¥300 per session or less. Also there's a reserve timeslot
ticket system in place, so the earlier you can arrive, the better. In
other words, if you can come on a weekday morning instead of a Sunday
afternoon, you'd be golden. You'd be even wiser if you're checking [the
official website](https://www.megaweb.gr.jp/) to make sure there's no
hiccups in operation the day you want to visit. 

### Access & Hours 

[Aomi 1-3-12, Koto-ku, Tokyo, Japan 135-0064](https://maps.apple.com/?address=3-12,%20Aomi%201-Ch%C5%8Dme,%20Koto-Ku,%20Tokyo,%20Japan%20135-0064&auid=15055782854942844873&ll=35.625892,139.781503&lsp=9902&q=Mega%20Web&_ext=ChkKBAgEEAoKBAgFEAMKBQgGEN8BCgQIChAAEiYpQBtv0oXPQUAxAPMrD9V4YUA5vvCULqzQQUBBkroCmC95YUBQBA%3D%3D&t=h)

By transit: Toyko Teleport Station (R04) on the Rinkai Line or Aomi Station (U10) on the Yurikamome Line.

MEGA WEB is generally open everyday but closes once a month for
maintenance; that date floats so please check [the
calendar](https://www.megaweb.gr.jp/service/) for details. Main attraction
is open from 11:00-20:00, however some sections close admittance earlier;
again check to see which ones.


